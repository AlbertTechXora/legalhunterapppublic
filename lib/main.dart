import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:hexcolor/hexcolor.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:firebase_core/firebase_core.dart';

import './views/splashScreen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  final savedThemeMode = await AdaptiveTheme.getThemeMode();
  runApp(MyApp(savedThemeMode: savedThemeMode));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final AdaptiveThemeMode savedThemeMode;

  const MyApp({Key key, this.savedThemeMode}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      // systemNavigationBarColor: Colors.transparent,
    ));
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return AdaptiveTheme(
      light: ThemeData(
        brightness: Brightness.light,
        primaryColor: HexColor("#2B3F63"),
        primaryColorLight: HexColor("#919191"),
        accentColor: HexColor("#2B3F63"),
        highlightColor: HexColor("#1779C0"),
        // colorScheme: ColorScheme.light(primary: HexColor("#2B3F63")),
        fontFamily: 'Stolzl',
        textTheme: TextTheme(
          headline1: TextStyle(
            color: Colors.black,
            fontSize: 16.0,
            height: 1.4,
            fontWeight: FontWeight.w500,
            fontFamily: "assets/fonts/Stolzl-Bold.ttf",
          ),
          headline2: TextStyle(
            fontSize: 20.0,
            height: 1.4,
            color: HexColor("#2B3F63"),
            fontWeight: FontWeight.bold,
            fontFamily: "assets/fonts/Stolzl-Bold.ttf",
          ),
          headline3: TextStyle(
            color: HexColor("#2B3F63"),
            fontSize: 16.0,
            height: 1.4,
            fontWeight: FontWeight.w500,
            fontFamily: "assets/fonts/Stolzl-Bold.ttf",
          ),
          headline4: TextStyle(
            color: Colors.white,
            fontSize: 13.0,
            height: 1.4,
            fontWeight: FontWeight.w500,
            fontFamily: "assets/fonts/Stolzl-Bold.ttf",
          ),
          headline5: TextStyle(
            fontSize: 20.0,
            height: 1.4,
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontFamily: "assets/fonts/Stolzl-Bold.ttf",
          ),
          headline6: TextStyle(
            fontSize: 18.0,
            height: 1.4,
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontFamily: "assets/fonts/Stolzl-Bold.ttf",
          ),
          bodyText1: TextStyle(
            color: Colors.black,
            fontSize: 14.0,
            height: 1.4,
            fontWeight: FontWeight.normal,
            fontFamily: "assets/fonts/Stolzl-Regular.ttf",
          ),
          bodyText2: TextStyle(
            color: Colors.white,
            fontSize: 14.0,
            height: 1.4,
            fontFamily: "assets/fonts/Stolzl-Regular.ttf",
          ),
          subtitle1: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
            height: 1.4,
            fontWeight: FontWeight.w600,
            fontFamily: "assets/fonts/Stolzl-Medium.ttf",
          ),
          subtitle2: TextStyle(
            color: HexColor("#2B3F63"),
            fontSize: 14.0,
            height: 1.4,
            fontWeight: FontWeight.w500,
            fontFamily: "assets/fonts/Stolzl-Medium.ttf",
          ),
        ),
        buttonTheme: ButtonThemeData(
          buttonColor: HexColor("#2B3F63"),
          minWidth: 150,
          height: 38,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
          ),
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(
              HexColor("#2B3F63"),
            ),
            shape: MaterialStateProperty.all<OutlinedBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
            minimumSize: MaterialStateProperty.all<Size>(Size(150.0, 38.0)),
          ),
        ),
        appBarTheme: AppBarTheme(
          elevation: 0,
          // actionsIconTheme: IconThemeData(
          //   color: Colors.white,
          // ),
        ),
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          selectedItemColor: HexColor("#1779C0"),
          unselectedItemColor: Colors.grey,
        ),
      ),
      dark: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.black,
        primaryColorLight: HexColor("#919191"),
        accentColor: HexColor("#2B3F63"),
        highlightColor: HexColor("#1779C0"),
        scaffoldBackgroundColor: Colors.black,
        // colorScheme: ColorScheme.light(primary: HexColor("#2B3F63")),
        fontFamily: 'Stolzl',
        textTheme: TextTheme(
          headline1: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
            height: 1.4,
            fontWeight: FontWeight.w500,
            fontFamily: "assets/fonts/Stolzl-Bold.ttf",
          ),
          headline2: TextStyle(
            fontSize: 20.0,
            height: 1.4,
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: "assets/fonts/Stolzl-Bold.ttf",
          ),
          headline3: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
            height: 1.4,
            fontWeight: FontWeight.w500,
            fontFamily: "assets/fonts/Stolzl-Bold.ttf",
          ),
          headline4: TextStyle(
            color: Colors.white,
            fontSize: 13.0,
            height: 1.4,
            fontWeight: FontWeight.w500,
            fontFamily: "assets/fonts/Stolzl-Bold.ttf",
          ),
          headline5: TextStyle(
            fontSize: 20.0,
            height: 1.4,
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: "assets/fonts/Stolzl-Bold.ttf",
          ),
          headline6: TextStyle(
            fontSize: 18.0,
            height: 1.4,
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: "assets/fonts/Stolzl-Bold.ttf",
          ),
          bodyText1: TextStyle(
            color: Colors.grey,
            fontSize: 14.0,
            height: 1.4,
            fontWeight: FontWeight.normal,
            fontFamily: "assets/fonts/Stolzl-Regular.ttf",
          ),
          bodyText2: TextStyle(
            color: Colors.white,
            fontSize: 14.0,
            height: 1.4,
            fontWeight: FontWeight.normal,
            fontFamily: "assets/fonts/Stolzl-Regular.ttf",
          ),
          subtitle1: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
            height: 1.4,
            fontWeight: FontWeight.w600,
            fontFamily: "assets/fonts/Stolzl-Medium.ttf",
          ),
          subtitle2: TextStyle(
            color: Colors.white,
            fontSize: 14.0,
            height: 1.4,
            fontWeight: FontWeight.w500,
            fontFamily: "assets/fonts/Stolzl-Medium.ttf",
          ),
        ),
        buttonTheme: ButtonThemeData(
          buttonColor: Colors.grey[800],
          minWidth: 150,
          height: 38,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
          ),
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(
              Colors.blueGrey[900],
            ),
            shape: MaterialStateProperty.all<OutlinedBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
            minimumSize: MaterialStateProperty.all<Size>(Size(150.0, 38.0)),
          ),
        ),
        appBarTheme: AppBarTheme(
          elevation: 0,
          // actionsIconTheme: IconThemeData(
          //   color: Colors.white,
          // ),
        ),
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.grey,
        ),
      ),
      initial: savedThemeMode ?? AdaptiveThemeMode.light,
      builder: (theme, darkTheme) => MaterialApp(
        title: 'Legal Hunter',
        debugShowCheckedModeBanner: false,
        theme: theme,
        darkTheme: darkTheme,
        home: SplashScreen(),
      ),
    );
  }
}
