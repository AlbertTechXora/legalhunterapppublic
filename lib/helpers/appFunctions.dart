import 'dart:io';

import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
// import 'package:flutter_twitter/flutter_twitter.dart';

class AppFuntions {
  static connectionCheck() async {
    var connectionStatus;
    try {
      final result = await InternetAddress.lookup('google.com');
      print("result: $result");
      if (result.isEmpty && result[0].rawAddress.isEmpty) {
        connectionStatus = "No Internet Connection.";
      } else {
        connectionStatus = null;
      }
    } on SocketException catch (_) {
      connectionStatus = "You are not Connected to the Internet.";
    }
    return connectionStatus;
  }

  static logoutFromApp() async {
    print("logoutFromApp...");
    final preferences = await StreamingSharedPreferences.instance;
    Preference<String> loginType =
        preferences.getString('LoginType', defaultValue: "");
    var loginTypeVal = loginType.getValue();
    // preferences.clear();

    Preference<Set<String>> prefKeys = preferences.getKeys();
    var sharedPrefKeys = prefKeys.getValue();
    print("sharedPrefKeys: $sharedPrefKeys");
    for (var keys in sharedPrefKeys) {
      print("keys:$keys");
      if (keys != "isDark" && keys != "adaptive_theme_preferences") {
        preferences.remove(keys);
        print("Removed");
      } else {
        print("Not Removed");
      }
    }
    if (loginTypeVal != "Normal") {
      socialLogout(loginType);
    }
  }

  static socialLogout(socialApp) async {
    print("socialLogout...");
    if (socialApp == "Google") {
      final googleSignIn = GoogleSignIn();
      await googleSignIn.disconnect();
    } else if (socialApp == "Facebook") {
      final FacebookLogin facebookSignIn = FacebookLogin();
      await facebookSignIn.logOut();
    } else {
      // var twitterLogin = TwitterLogin(
      //   consumerKey: 'hcuPnXNZddkHxr7yw6MuPYx3k',
      //   consumerSecret: '9LmTRLcmNgnz4J78dxVp99SQXtQ6p9nfFfihNgpqMjqYfcQ7CY',
      // );
      // await twitterLogin.logOut();
    }
    FirebaseAuth.instance.signOut();
  }
}
