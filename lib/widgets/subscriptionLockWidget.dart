import 'package:flutter/material.dart';

class SubscriptionLockWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 50.0,
        width: 50.0,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(20.0)),
        child: Row(
          children: [
            Text(
              "PRO",
              style: TextStyle(
                color: Colors.black,
                fontSize: 12.0,
                height: 1.4,
                fontWeight: FontWeight.w500,
                fontFamily: "assets/fonts/Stolzl-Bold.ttf",
              ),
            ),
            Icon(
              Icons.lock,
              size: 12.0,
            ),
          ],
        ));
  }
}
