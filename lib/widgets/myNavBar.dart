import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:another_flushbar/flushbar.dart';
import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as Path;

import '../helpers/appFunctions.dart';
import '../services/connections.dart' as Constants;

import '../views/subscriptionScreen.dart';
import '../views/usersProfilePage.dart';
import '../views/feedsOrNotificationsScreen.dart';
import '../views/editProfileScreen.dart';
import '../views/loginScreen.dart';
import '../views/faqScreen.dart';

class MyNavDrawer extends StatefulWidget {
  @override
  _MyNavDrawerState createState() => _MyNavDrawerState();
}

class _MyNavDrawerState extends State<MyNavDrawer> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth > 1200) {
          return DesktopMyNavDrawer();
        } else if (constraints.maxWidth > 800 && constraints.maxWidth < 1200) {
          return MobileMyNavDrawer();
        } else {
          return MobileMyNavDrawer();
        }
      },
    );
  }
}

class MobileMyNavDrawer extends StatefulWidget {
  @override
  _MobileMyNavDrawerState createState() => _MobileMyNavDrawerState();
}

class _MobileMyNavDrawerState extends State<MobileMyNavDrawer> {
  var apiRequestStatus;
  var apiErrMessage;
  var connectionChk;
  var userName;
  var userEmail;
  var profileImage;
  var appLang;
  var currLang;
  bool isDarkMode = false;
  bool isPageLoading = false;
  var providerKey;
  List userAttemptedExamList = [];
  List userExamCategoryList = [];

  List imgPickerOptions = [
    {"iconData": Icons.camera, "optionsLabel": "Camera"},
    {"iconData": Icons.photo, "optionsLabel": "Gallery"}
  ];

  getUserProfile() async {
    setState(() {
      isPageLoading = true;
    });
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      //multipart/form-data POST Method
      Map<String, String> requestBody = <String, String>{
        "ProviderKey": providerKey,
      };
      // Map<String, String> headers = <String, String>{
      //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
      // };
      var uri = Uri.parse(Constants.getUserProfile);
      var request = http.MultipartRequest('POST', uri)
        // ..headers.addAll(
        //     headers) //if u have headers, basic auth, token bearer... Else remove line
        ..fields.addAll(requestBody);
      var response = await request.send();
      final respStr = await response.stream.bytesToString();
      //

      print("response.request:${response.request}");
      if (response.statusCode == 200) {
        var data = json.decode(respStr);
        // print("data: $data");
        if (data != null) {
          apiRequestStatus = data['Status'];
          print("apiRequestStatus: $apiRequestStatus");
          if (apiRequestStatus == "Success") {
            // print(
            //     "data['UserAttemptedExamList']: ${data['UserAttemptedExamList']}");
            setState(() {
              userAttemptedExamList = data['UserAttemptedExamList'];
              userExamCategoryList = data['UserExamCategoryList'];
            });
            final preferences = await StreamingSharedPreferences.instance;
            preferences.setString('ProviderKey', data['ProviderKey']);
            preferences.setString('UserName', data['Name']);
            preferences.setString('ProfileImage', data['ProfileImage']);
            preferences.setString('Email', data['Email']);
            preferences.setString('DateofBirth', data['DateofBirthFormatted']);
            preferences.setInt('UserCode', data['UserCode']);
            preferences.setString('PhoneNumber', data['PhoneNumber']);
            preferences.setInt('SubscriptionID', data['SubscriptionID']);
            preferences.setString('SubscriptionName', data['SubscriptionName']);
            preferences.setString(
                'SubscriptionValidFrom', data['SubscriptionValidFrom']);
            preferences.setString(
                'SubscriptionExpiryOn', data['SubscriptionExpiryOn']);
            preferences.setDouble('SubsciptionCost', data['SubsciptionCost']);

            preferences.setString('Gender', data['Gender']);
            preferences.setString('Occupation', data['Occupation']);
            preferences.setString('District', data['District']);
            preferences.setString(
                'EducationalQualification', data['EducationalQualification']);
            preferences.setString('Subject', data['Subject']);
            preferences.setString(
                'LastInstitutionStudied', data['LastInstitutionStudied']);
            preferences.setString('Course', data['Course']);
            preferences.setString('EntranceType', data['EntranceType']);
            preferences.setInt(
                'NumberOfDaysPassed', data['NumberOfDaysPassed']);
            preferences.setInt(
                'NumberOfExamsAttended', data['NumberOfExamsAttended']);
            preferences.setInt(
                'HighestScoreAchieved', data['HighestScoreAchieved']);
          } else {
            apiErrMessage = data['Errors'][0];
            Flushbar(
              message: apiErrMessage,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          apiRequestStatus = false;
          apiErrMessage = "No data found.";
          Flushbar(
            message: apiErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        apiRequestStatus = false;
        apiErrMessage = "Error: ${response.statusCode}.";
        Flushbar(
          message: apiErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      apiRequestStatus = false;
      apiErrMessage = connectionChk;
      Flushbar(
        message: apiErrMessage,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
    getSharedDataPref();
  }

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<String> username =
        preferences.getString('UserName', defaultValue: "");
    Preference<String> useremail =
        preferences.getString('Email', defaultValue: "");
    Preference<String> profileImg =
        preferences.getString('ProfileImage', defaultValue: "");
    Preference<String> proKey =
        preferences.getString('ProviderKey', defaultValue: "");
    Preference<bool> isDark =
        preferences.getBool('isDark', defaultValue: false);

    setState(() {
      username.listen((value) {
        userName = value;
      });
      useremail.listen((value) {
        userEmail = value;
      });
      profileImg.listen((value) {
        profileImage = value;
      });
      proKey.listen((value) {
        providerKey = value;
      });
      isDark.listen((value) {
        isDarkMode = value;
      });
    });
    // print("userName: $userName");
    // // print("profileImage: $profileImage");
    // print("isDarkMode: $isDarkMode");
  }

  void changeDarkOrLightMode() async {
    final preferences = await StreamingSharedPreferences.instance;
    if (isDarkMode == true) {
      AdaptiveTheme.of(context).setLight();
      preferences.setBool('isDark', false);
    } else {
      AdaptiveTheme.of(context).setDark();
      preferences.setBool('isDark', true);
    }
  }

  //Image Picking
  File imageFile;
  PickedFile _img;
  final ImagePicker _picker = ImagePicker();
  getProfileImg(src) async {
    final preferences = await StreamingSharedPreferences.instance;
    setState(() {
      isPageLoading = true;
    });
    final image = await _picker.getImage(source: src);
    setState(() {
      _img = image;
    });
    imageFile = File(_img.path);
    print("_img:=> $_img");
    print("imageFile:=> $imageFile");

    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      // open a byteStream
      var stream = new http.ByteStream(imageFile.openRead());
      stream.cast();
      // get file length
      var length = await imageFile.length();

      // string to uri
      var uri = Uri.parse(Constants.updateUserProfileImage);

      // create multipart request
      var request = new http.MultipartRequest("POST", uri);

      // if you need more parameters to parse, add those like this. i added "user_id". here this "ProviderKey" is a key of the API request
      request.fields["ProviderKey"] = providerKey;

      // multipart that takes file.. here this "ProfileImage" is a key of the API request
      var multipartFile = new http.MultipartFile(
          'UserProfileImage', stream, length,
          filename: Path.basename(imageFile.path));

      // add file to multipart
      request.files.add(multipartFile);

      // send request to upload image
      await request.send().then((response) async {
        // listen for response
        response.stream.transform(utf8.decoder).listen((value) async {
          // print("ResponseVal: $value");
          if (response.statusCode == 200) {
            var imgUploadData = json.decode(value);
            print("imgUploadData:$imgUploadData");
            if (imgUploadData != null) {
              var imgUploadDataStatus = imgUploadData['Status'];
              if (imgUploadDataStatus == "Success") {
                var profileImage = imgUploadData['ProfileImage'];
                preferences.setString('ProfileImage', profileImage);
                Flushbar(
                  message: "Profile Image Updated Successfully.",
                  duration: Duration(seconds: 2),
                  backgroundColor: Colors.green,
                ).show(context);
                getSharedDataPref();
              } else {
                Flushbar(
                  message: "Profile Image Update Failed.",
                  duration: Duration(seconds: 2),
                  backgroundColor: Colors.red,
                ).show(context);
              }
            } else {
              Flushbar(
                message: "Empty UploadData.",
                duration: Duration(seconds: 2),
                backgroundColor: Colors.red,
              ).show(context);
            }
          } else {
            Flushbar(
              message: "Error: ${response.statusCode}",
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        });
      }).catchError((e) {
        print(e);
      });
    } else {
      Flushbar(
        message: connectionChk,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
  }
  //Image picking End

  showImgPickerOptionsBottomSheet() {
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25.0),
          topRight: Radius.circular(25.0),
        ),
      ),
      context: context,
      builder: (BuildContext _) {
        return Container(
          // height: MediaQuery.of(context).size.height * 0.05,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25.0),
              topRight: Radius.circular(25.0),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView.separated(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              primary: true,
              itemCount: imgPickerOptions.length,
              // itemCount: 1,
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  leading: Icon(imgPickerOptions[index]['iconData']),
                  title: Text(imgPickerOptions[index]['optionsLabel']),
                  onTap: () {
                    if (imgPickerOptions[index]['optionsLabel'] == "Camera") {
                      getProfileImg(ImageSource.camera);
                    } else {
                      getProfileImg(ImageSource.gallery);
                    }
                    Navigator.pop(context);
                  },
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            ),
          ),
        );
      },
      isScrollControlled: true,
    );
  }

  @override
  void initState() {
    super.initState();
    getSharedDataPref();
    getUserProfile();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: isDarkMode ? Colors.black12 : Theme.of(context).primaryColor,
        child: SafeArea(
          child: Stack(
            children: [
              isPageLoading
                  ? Center(
                      child: new ClipRect(
                        child: new BackdropFilter(
                          filter:
                              new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                          child: new Container(
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height,
                            decoration: new BoxDecoration(
                              color: Colors.grey[400].withOpacity(1.0),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15.0)),
                            ),
                            child: new Center(
                              child: CircularProgressIndicator(),
                            ),
                          ),
                        ),
                      ),
                    )
                  : Container(),
              ListView(
                padding: EdgeInsets.all(2.0),
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            color: isDarkMode ? Colors.black12 : Colors.white,
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 15.0,
                                ),
                                Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    GestureDetector(
                                      onTap: showImgPickerOptionsBottomSheet,
                                      child: Container(
                                        child: ClipOval(
                                          child: Align(
                                            heightFactor: 1,
                                            widthFactor: 1,
                                            child: profileImage != null
                                                ? Image.network(
                                                    profileImage,
                                                    height: 100.0,
                                                    width: 100.0,
                                                    fit: BoxFit.cover,
                                                  )
                                                : Image.asset(
                                                    "assets/icons/ic10.png",
                                                    height: 100.0,
                                                    width: 100.0,
                                                    fit: BoxFit.cover,
                                                  ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Positioned.fill(
                                      left: 70.0,
                                      child: Align(
                                        alignment: Alignment.bottomRight,
                                        child: IconButton(
                                          onPressed:
                                              showImgPickerOptionsBottomSheet,
                                          icon: Icon(Icons
                                              .add_photo_alternate_rounded),
                                          color: Theme.of(context).primaryColor,
                                          iconSize: 30.0,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 12.0,
                                ),
                                userName != null
                                    ? Text(
                                        '$userName',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline6,
                                      )
                                    : Container(),
                                SizedBox(
                                  height: 5.0,
                                ),
                                Text(
                                  '$userEmail',
                                  style: Theme.of(context).textTheme.bodyText1,
                                ),
                                SizedBox(
                                  height: 15.0,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  ListTile(
                    leading: Image.asset(
                      "assets/icons/ic5.png",
                      height: 25.0,
                      width: 25.0,
                    ),
                    title: Text(
                      "View Profile",
                      style: TextStyle(
                        color: Colors.white,
                        // fontSize: 15,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => UsersProfilePage(
                            userAttemptedExamDetails: userAttemptedExamList,
                            userExamCategoryListDetails: userExamCategoryList,
                          ),
                        ),
                      );
                    },
                  ),
                  ListTile(
                    leading: Image.asset(
                      "assets/icons/ic4.png",
                      height: 25.0,
                      width: 25.0,
                    ),
                    title: Text(
                      "Subscription",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => SubscriptionScreen(),
                        ),
                      );
                    },
                  ),
                  // ListTile(
                  //   leading: Image.asset(
                  //     "assets/icons/ic9.png",
                  //     height: 25.0,
                  //     width: 25.0,
                  //   ),
                  //   title: Text(
                  //     "My Exams",
                  //     style: TextStyle(
                  //       color: Colors.white,
                  //       fontWeight: FontWeight.w600,
                  //       fontStyle: FontStyle.normal,
                  //     ),
                  //   ),
                  //   onTap: () {
                  //     Navigator.of(context).push(
                  //       MaterialPageRoute(
                  //         builder: (context) => UsersProfilePage(),
                  //       ),
                  //     );
                  //   },
                  // ),
                  ListTile(
                    leading: Image.asset(
                      "assets/icons/ic1.png",
                      height: 25.0,
                      width: 25.0,
                    ),
                    title: Text(
                      "Feeds",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => FeedsOrNotificationsScreen(
                            pageTitle: "Feeds",
                          ),
                        ),
                      );
                    },
                  ),
                  ListTile(
                    leading: Image.asset(
                      "assets/icons/ic6.png",
                      height: 25.0,
                      width: 25.0,
                    ),
                    title: Text(
                      "Notifications",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => FeedsOrNotificationsScreen(
                            pageTitle: "Notifications",
                          ),
                        ),
                      );
                    },
                  ),
                  ListTile(
                    leading: Image.asset(
                      "assets/icons/ic2.png",
                      height: 25.0,
                      width: 25.0,
                    ),
                    title: Text(
                      "Dark/ Light Mode",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    onTap: () {
                      changeDarkOrLightMode();
                      // Navigator.of(context).pop();
                    },
                  ),
                  ListTile(
                    leading: Image.asset(
                      "assets/icons/ic3.png",
                      height: 25.0,
                      width: 25.0,
                    ),
                    title: Text(
                      "Help Desk",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => FaqScreen(),
                        ),
                      );
                    },
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.settings,
                      color: Colors.white,
                      size: 20.0,
                    ),
                    title: Text(
                      "Settings",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => EditProfileScreen(
                            editScreenType: "Settings",
                          ),
                        ),
                      );
                    },
                  ),
                  ListTile(
                    leading: FaIcon(
                      FontAwesomeIcons.signOutAlt,
                      color: Colors.white,
                      size: 20.0,
                    ),
                    title: Text(
                      "Sign Out",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    onTap: () async {
                      AppFuntions.logoutFromApp();
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                            builder: (context) => LoginScreen(),
                          ),
                          (Route<dynamic> route) => false);
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class DesktopMyNavDrawer extends StatefulWidget {
  @override
  _DesktopMyNavDrawerState createState() => _DesktopMyNavDrawerState();
}

class _DesktopMyNavDrawerState extends State<DesktopMyNavDrawer> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
