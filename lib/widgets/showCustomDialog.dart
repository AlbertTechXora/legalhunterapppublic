import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/services.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import '../views/usersHomePage.dart';

class ShowCustomDialog extends StatefulWidget {
  final String title, description, buttonText, alertFrom;
  final int dataStatus;

  ShowCustomDialog({
    this.title,
    this.description,
    this.buttonText,
    this.dataStatus,
    this.alertFrom,
  });

  @override
  _ShowCustomDialogState createState() => _ShowCustomDialogState();
}

class _ShowCustomDialogState extends State<ShowCustomDialog> {
  bool isDarkMode = false;
  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<bool> isDark =
        preferences.getBool('isDark', defaultValue: false);
    isDark.listen((value) {
      setState(() {
        isDarkMode = value;
      });
    });
    // print("isDarkMode: $isDarkMode");
  }

  previousScreenNav() {
    Navigator.of(context).pushAndRemoveUntil(
      MaterialPageRoute(
        builder: (context) => UsersHomePage(),
      ),
      (Route<dynamic> route) => false,
    );
  }

  @override
  void initState() {
    super.initState();
    getSharedDataPref();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.only(
        left: 12.0,
        right: 12.0,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      elevation: 5.0,
      backgroundColor: isDarkMode ? Theme.of(context).primaryColorLight : null,
      child: Container(
        height: 160.0,
        padding: EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              child: FaIcon(
                FontAwesomeIcons.question,
                size: 40.0,
                color: isDarkMode ? Colors.white : Colors.black,
              ),
            ),
            Text(
              widget.title,
              style: Theme.of(context).textTheme.headline6,
              textAlign: TextAlign.center,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ElevatedButton(
                  child: Text(
                    widget.buttonText,
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    widget.alertFrom == "ExamScreen"
                        ? previousScreenNav()
                        : SystemNavigator.pop();
                  },
                ),
                ElevatedButton(
                  child: Text(
                    "No",
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
