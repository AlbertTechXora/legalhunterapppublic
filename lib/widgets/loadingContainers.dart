import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoadingContainers extends StatefulWidget {
  final String loaderType;
  LoadingContainers({@required this.loaderType});

  @override
  _LoadingContainersState createState() => _LoadingContainersState();
}

class _LoadingContainersState extends State<LoadingContainers> {
  @override
  Widget build(BuildContext context) {
    return widget.loaderType == "Grid"
        ? Container(
            child: GridView.count(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              crossAxisCount: 2,
              childAspectRatio: 0.85,
              padding: const EdgeInsets.all(5.0),
              mainAxisSpacing: 10.0,
              crossAxisSpacing: 10.0,
              children: List.generate(4, (index) {
                return Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  elevation: 5.0,
                  child: Container(
                    decoration: BoxDecoration(
                      // color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: SpinKitPulse(
                      size: 100.0,
                      color: Colors.grey,
                    ),
                  ),
                );
              }),
            ),
          )
        : Container(
            child: ListView.builder(
              scrollDirection: Axis.vertical,
              itemCount: 3,
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (BuildContext context, index) {
                return Container(
                  height: 160.0,
                  margin: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 8.0),
                  padding: EdgeInsets.all(5.0),
                  child: Card(
                    elevation: 5.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: SpinKitPulse(
                      size: 100.0,
                      color: Colors.grey,
                    ),
                  ),
                );
              },
            ),
          );
  }
}
