import 'package:flutter/material.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import '../views/usersHomePage.dart';
import '../views/feedsOrNotificationsScreen.dart';
import '../views/tryAnotherExamScreen.dart';
import '../views/trainingVideosScreen.dart';
// import '../views/trainingVideoPlayerScreen.dart';

class MyBottomNavBar extends StatefulWidget {
  @override
  _MyBottomNavBarState createState() => _MyBottomNavBarState();
}

class _MyBottomNavBarState extends State<MyBottomNavBar> {
  int selectedIndex = 0;
  bool isDarkMode = false;
  var iconSize = 25.0;

  buttonActionsFn(selectedItemIndex) async {
    final preferences = await StreamingSharedPreferences.instance;
    preferences.setInt('BottomAppbarItemIndex', selectedItemIndex);
    print("selectedItemIndex:$selectedItemIndex");
    if (selectedItemIndex == 0) {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => UsersHomePage(),
        ),
      );
    } else if (selectedItemIndex == 1) {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => FeedsOrNotificationsScreen(
            pageTitle: "Feeds",
          ),
        ),
      );
    } else if (selectedItemIndex == 2) {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => TryAnotherExamScreen(
            examCatId: 1,
            subCategoryList: [],
          ),
        ),
      );
    } else {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => TrainingVideosScreen(),
          // builder: (context) => TrainingVideoPlayerScreen(),
        ),
      );
    }
  }

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<int> bottomAppbarItemIndex =
        preferences.getInt('BottomAppbarItemIndex', defaultValue: 0);
    bottomAppbarItemIndex.listen((value) {
      selectedIndex = value;
    });
    var itemIndex = bottomAppbarItemIndex.getValue();
    setState(() {
      selectedIndex = itemIndex;
    });
  }

  @override
  void initState() {
    super.initState();
    getSharedDataPref();
  }

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      shape: CircularNotchedRectangle(),
      notchMargin: 5.0,
      clipBehavior: Clip.antiAlias,
      elevation: 20.0,
      child: Container(
        height: 55.0,
        // padding: EdgeInsets.only(left: 10.0, right: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            IconButton(
              // icon: Image.asset(
              //   "assets/icons/fic1.png",
              //   height: 40.0,
              //   width: 40.0,
              //   color: selectedIndex == 0
              //       ? Theme.of(context)
              //           .bottomNavigationBarTheme
              //           .selectedItemColor
              //       : null,
              // ),
              icon: Icon(
                Icons.home_outlined,
                color: selectedIndex == 0
                    ? Theme.of(context)
                        .bottomNavigationBarTheme
                        .selectedItemColor
                    : Theme.of(context)
                        .bottomNavigationBarTheme
                        .unselectedItemColor,
              ),
              iconSize: iconSize,
              onPressed: () {
                setState(() {
                  selectedIndex = 0;
                });
                buttonActionsFn(selectedIndex);
              },
            ),
            IconButton(
              // icon: Image.asset(
              //   "assets/icons/fic2.png",
              //   height: 40.0,
              //   width: 40.0,
              //   color: selectedIndex == 1
              //       ? Theme.of(context)
              //           .bottomNavigationBarTheme
              //           .selectedItemColor
              //       : null,
              // ),
              icon: Icon(
                Icons.grid_view,
                color: selectedIndex == 1
                    ? Theme.of(context)
                        .bottomNavigationBarTheme
                        .selectedItemColor
                    : Theme.of(context)
                        .bottomNavigationBarTheme
                        .unselectedItemColor,
              ),
              iconSize: iconSize,
              onPressed: () {
                setState(() {
                  selectedIndex = 1;
                });
                buttonActionsFn(selectedIndex);
              },
            ),
            // SizedBox(width: 40), // The dummy child
            IconButton(
              // icon: Image.asset(
              //   "assets/icons/fic3.png",
              //   height: 40.0,
              //   width: 40.0,
              //   color: selectedIndex == 2
              //       ? Theme.of(context)
              //           .bottomNavigationBarTheme
              //           .selectedItemColor
              //       : null,
              // ),
              icon: Icon(
                Icons.dvr_sharp,
                color: selectedIndex == 2
                    ? Theme.of(context)
                        .bottomNavigationBarTheme
                        .selectedItemColor
                    : Theme.of(context)
                        .bottomNavigationBarTheme
                        .unselectedItemColor,
              ),
              iconSize: iconSize,
              onPressed: () {
                setState(() {
                  selectedIndex = 2;
                });
                buttonActionsFn(selectedIndex);
              },
            ),
            IconButton(
              // icon: Image.asset(
              //   "assets/icons/fic4.png",
              //   height: 40.0,
              //   width: 40.0,
              //   color: selectedIndex == 3
              //       ? Theme.of(context)
              //           .bottomNavigationBarTheme
              //           .selectedItemColor
              //       : null,
              // ),
              icon: Icon(
                Icons.ondemand_video_rounded,
                color: selectedIndex == 3
                    ? Theme.of(context)
                        .bottomNavigationBarTheme
                        .selectedItemColor
                    : Theme.of(context)
                        .bottomNavigationBarTheme
                        .unselectedItemColor,
              ),
              iconSize: iconSize,
              onPressed: () {
                setState(() {
                  selectedIndex = 3;
                });
                buttonActionsFn(selectedIndex);
              },
            ),
          ],
        ),
      ),
    );
  }
}
