import 'package:flutter/material.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
// import 'package:flutter_twitter/flutter_twitter.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import '../views/usersHomePage.dart';

import '../helpers/appFunctions.dart';
import '../services/connections.dart' as Constants;

class SocialLoginButtons extends StatefulWidget {
  @override
  _SocialLoginButtonsState createState() => _SocialLoginButtonsState();
}

class _SocialLoginButtonsState extends State<SocialLoginButtons> {
  bool isPageLoading = false;
  var connectionChk;
  var socialMediaLoginStatus;
  var socialMediaLoginErrMsg;

  final googleSignIn = GoogleSignIn();
  final FacebookLogin facebookSignIn = FacebookLogin();
  // var twitterLogin = TwitterLogin(
  //   consumerKey: 'hcuPnXNZddkHxr7yw6MuPYx3k',
  //   consumerSecret: '9LmTRLcmNgnz4J78dxVp99SQXtQ6p9nfFfihNgpqMjqYfcQ7CY',
  // );

  Future socialLogin(socialAppName) async {
    print("Starting socialLogin...");
    isPageLoading = true;
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      print("socialAppName:$socialAppName");
      if (socialAppName == "Google") {
        final googleUser = await googleSignIn.signIn();
        if (googleUser == null) {
          isPageLoading = false;
          Flushbar(
            message: "Login Unsuccessful.",
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        } else {
          final googleUserAuth = await googleUser.authentication;
          final credential = GoogleAuthProvider.credential(
            accessToken: googleUserAuth.accessToken,
            idToken: googleUserAuth.idToken,
          );
          await FirebaseAuth.instance.signInWithCredential(credential);
          isPageLoading = false;

          final loggedInUser = FirebaseAuth.instance.currentUser;
          if (loggedInUser != null) {
            applicationSocialMediaLogin(
                loggedInUser.uid,
                loggedInUser.displayName,
                loggedInUser.email,
                socialAppName,
                loggedInUser.photoURL);
          }
        }
      } else if (socialAppName == "Facebook") {
        final FacebookLoginResult result =
            await facebookSignIn.logIn(['email']);
        switch (result.status) {
          case FacebookLoginStatus.loggedIn:
            final FacebookAccessToken accessToken = result.accessToken;

            //multipart/form-data GET Method to get FB Profile info
            var uri = Uri.parse(
                "https://graph.facebook.com/v2.12/me?fields=name,picture.width(800).height(800),first_name,last_name,email&access_token=${accessToken.token}");
            var request = http.MultipartRequest('GET', uri);
            var response = await request.send();
            final respStr = await response.stream.bytesToString();
            //

            var data = json.decode(respStr);
            print("profile: $data");
            if (data != null) {
              print('''
         Logged in!,
         Token: ${accessToken.token},
         User id: ${accessToken.userId},
         Expires: ${accessToken.expires},
         Permissions: ${accessToken.permissions},
         Declined permissions: ${accessToken.declinedPermissions}
         ''');
              applicationSocialMediaLogin(data['id'], data['name'],
                  data['email'], socialAppName, data['picture']['data']['url']);
            }
            break;
          case FacebookLoginStatus.cancelledByUser:
            print('Login cancelled by the FB user.');
            Flushbar(
              message: "Login cancelled by the FB user.",
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
            break;
          case FacebookLoginStatus.error:
            print('Something went wrong with the login process.\n'
                'Facebook gave us the Err: ${result.errorMessage}');
            Flushbar(
              message:
                  "Something went wrong with the login process.\nFacebook gave us the Err: ${result.errorMessage}",
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
            break;
        }
      } else {
        Flushbar(
          message: "Working on it. Coming Soon.!!",
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
        // final TwitterLoginResult result = await twitterLogin.authorize();

        // switch (result.status) {
        //   case TwitterLoginStatus.loggedIn:
        //     var session = result.session;
        //     print("session.username: ${session.username}");
        //     if (session.userId != null) {
        //       applicationSocialMediaLogin(
        //           session.userId, session.username, "", socialAppName, "");
        //     }
        //     break;
        //   case TwitterLoginStatus.cancelledByUser:
        //     print('Login cancelled by the Twitter User.');
        //     Flushbar(
        //       message: "Login cancelled by the FB user.",
        //       duration: Duration(seconds: 2),
        //       backgroundColor: Colors.red,
        //     ).show(context);
        //     break;
        //   case TwitterLoginStatus.error:
        //     print('Something went wrong with the login process.\n'
        //         'Twitter gave us the Err: ${result.errorMessage}');
        //     Flushbar(
        //       message:
        //           "Something went wrong with the login process.\nTwitter gave us the Err: ${result.errorMessage}",
        //       duration: Duration(seconds: 2),
        //       backgroundColor: Colors.red,
        //     ).show(context);
        //     break;
        // }
      }
    }
  }

  applicationSocialMediaLogin(
      providerKey, userName, userEmail, loginProvider, userProfileImage) async {
    print("Starting applicationSocialMediaLogin...");
    final preferences = await StreamingSharedPreferences.instance;
    setState(() {
      isPageLoading = true;
    });
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      if ((providerKey != "") && userName != "") {
        print('''providerKey:
            $providerKey,userName:$userName,loginProvider:$loginProvider''');

        //multipart/form-data POST Method
        Map<String, String> requestBody = <String, String>{
          "ProviderKey": providerKey,
          "CustomerName": userName,
          "CustomerEmail": userEmail,
          "LoginProvider": loginProvider,
        };
        print("requestBody:$requestBody");
        // Map<String, String> headers = <String, String>{
        //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
        // };
        var uri = Uri.parse(Constants.applicationSocialMediaLogin);
        var request = http.MultipartRequest('POST', uri)
          // ..headers.addAll(
          //     headers) //if u have headers, basic auth, token bearer... Else remove line
          ..fields.addAll(requestBody);
        var response = await request.send();
        final respStr = await response.stream.bytesToString();
        //

        print("response.request:${response.request}");
        if (response.statusCode == 200) {
          // print("Yes");
          var data = json.decode(respStr);
          print("data: $data");
          if (data != null) {
            socialMediaLoginStatus = data['LoginRequestStatus'];
            print("socialMediaLoginStatus: $socialMediaLoginStatus");
            if (socialMediaLoginStatus == true) {
              preferences.setString('LoginType', loginProvider);
              preferences.setString('ProviderKey', providerKey);
              preferences.setString('UserName', userName);
              preferences.setString('ProfileImage', userProfileImage);
              preferences.setString('Email', userEmail);
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) => UsersHomePage(),
                ),
              );
            } else {
              socialMediaLoginErrMsg = data['LoginErrorMessage'][0];
              Flushbar(
                message: socialMediaLoginErrMsg,
                duration: Duration(seconds: 2),
                backgroundColor: Colors.red,
              ).show(context);
            }
          } else {
            socialMediaLoginStatus = false;
            socialMediaLoginErrMsg = "Oops. Error occured, Please try again.";
            Flushbar(
              message: socialMediaLoginErrMsg,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          socialMediaLoginStatus = false;
          socialMediaLoginErrMsg = "Error: ${response.statusCode}.";
          Flushbar(
            message: socialMediaLoginErrMsg,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        socialMediaLoginStatus = false;
        socialMediaLoginErrMsg = "Social Media Credentials Missing.";
        Flushbar(
          message: socialMediaLoginErrMsg,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      socialMediaLoginStatus = false;
      socialMediaLoginErrMsg = connectionChk;
      Flushbar(
        message: socialMediaLoginErrMsg,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: isPageLoading
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : Column(
              children: [
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IconButton(
                        icon: FaIcon(FontAwesomeIcons.google),
                        color: Theme.of(context).primaryColor,
                        onPressed: () {
                          socialLogin("Google");
                        },
                      ),
                      SizedBox(
                        width: 45.0,
                      ),
                      IconButton(
                        icon: FaIcon(FontAwesomeIcons.facebookF),
                        color: Theme.of(context).primaryColor,
                        onPressed: () {
                          socialLogin("Facebook");
                        },
                      ),
                      SizedBox(
                        width: 45.0,
                      ),
                      IconButton(
                        icon: FaIcon(FontAwesomeIcons.twitter),
                        color: Theme.of(context).primaryColor,
                        onPressed: () {
                          socialLogin("Twitter");
                        },
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Center(
                    child: Text(
                      "Sign in with social account",
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w500,
                        color: Colors.blueGrey[500],
                      ),
                    ),
                  ),
                ),
              ],
            ),
    );
  }
}
