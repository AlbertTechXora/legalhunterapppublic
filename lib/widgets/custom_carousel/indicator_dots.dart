import 'package:flutter/material.dart';

class CustomDot extends StatelessWidget {
  final bool selected;
  final Color color;

  CustomDot({@required this.selected, this.color});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      child: DecoratedBox(
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: selected ? color ?? Color(0XFFC39D6C) : Colors.transparent,
          border: Border.all(
            width: 2,
            color: color ?? Color(0XFFC39D6C),
          ),
        ),
        child: SizedBox(height: 10, width: 10),
      ),
    );
  }
}

class IndicatorDots extends StatelessWidget {
  final Stream stream;
  final int itemCount;
  final Widget dot;
  final MainAxisAlignment mainAxisAlignment;
  final Color dotColor;

  IndicatorDots({
    @required this.stream,
    @required this.itemCount,
    this.mainAxisAlignment: MainAxisAlignment.center,
    this.dot,
    this.dotColor,
  });

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: stream,
      initialData: 0,
      builder: (context, selected) => Row(
        mainAxisAlignment: mainAxisAlignment,
        children: [
          for (int i = 0; i < itemCount; i++)
            dot ??
                CustomDot(
                  selected: i == selected.data,
                  color: dotColor,
                ),
        ],
      ),
    );
  }
}
