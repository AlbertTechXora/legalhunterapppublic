import 'package:flutter/material.dart';

class CustomCarousel extends StatelessWidget {
  final PageController pageController;
  final List<Widget> children;
  final bool scrollable;
  final Axis scrollAxis;

  CustomCarousel({
    @required this.pageController,
    @required this.children,
    this.scrollable: true,
    this.scrollAxis: Axis.horizontal,
  });

  @override
  Widget build(BuildContext context) {
    return PageView(
      physics: scrollable
          ? const AlwaysScrollableScrollPhysics()
          : const NeverScrollableScrollPhysics(),
      scrollDirection: scrollAxis,
      controller: pageController,
      children: children,
    );
  }
}
