import 'package:flutter/material.dart';

class MyLogoWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150.0,
      width: 150.0,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/logo/logo.png"),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
