import 'package:flutter/material.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

class MyAppBar extends StatefulWidget implements PreferredSizeWidget {
  final Widget leading;
  final Widget title;
  final List<Widget> actions;
  MyAppBar({this.leading, this.title, this.actions});

  @override
  _MyAppBarState createState() => _MyAppBarState();

  @override
  Size get preferredSize => const Size.fromHeight(50);
}

class _MyAppBarState extends State<MyAppBar> {
  var userName = "User Name";
  var profileImage;

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<String> username =
        preferences.getString('UserName', defaultValue: "");
    username.listen((value) {
      setState(() {
        userName = value;
      });
    });
    Preference<String> profileImg =
        preferences.getString('ProfileImage', defaultValue: "");
    profileImg.listen((value) {
      setState(() {
        profileImage = value;
      });
    });
    // print("userName: $userName");
    // print("profileImage: $profileImage");
  }

  @override
  void initState() {
    super.initState();
    // getSharedDataPref();
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      bottomOpacity: 0.0,
      automaticallyImplyLeading: false,
      key: widget.key,
      leading: widget.leading != null
          ? widget.leading
          : IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
            ),
      title: widget.title,
      actions: widget.actions != null
          ? widget.actions
          : [
              // Row(
              //   children: [
              //     Text(
              //       userName,
              //       style: TextStyle(
              //         fontWeight: FontWeight.bold,
              //         fontSize: 15.0,
              //       ),
              //     ),
              //     InkWell(
              //       child: Container(
              //         height: 50.0,
              //         width: 50.0,
              //         margin: EdgeInsets.only(
              //           right: 10.0,
              //           left: 10.0,
              //         ),
              //         decoration: BoxDecoration(
              //           // color: Colors.white,
              //           shape: BoxShape.circle,
              //         ),
              //         child: profileImage != null
              //             ? Image.network(
              //                 profileImage,
              //                 fit: BoxFit.fill,
              //               )
              //             : Image.asset(
              //                 "assets/icons/ic11.png",
              //                 fit: BoxFit.fill,
              //               ),
              //       ),
              //       onTap: () {},
              //     ),
              //   ],
              // ),
              Container(
                child: Image.asset(
                  "assets/logo/white png.png",
                  fit: BoxFit.fill,
                ),
              ),
            ],
    );
  }
}
