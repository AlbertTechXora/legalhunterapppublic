import 'dart:async';
import 'package:flutter/material.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:url_launcher/url_launcher.dart';

import './custom_carousel/custom_carousel.dart';
import './custom_carousel/indicator_dots.dart';

import '../views/quizInstructionScreen.dart';

abstract class TopIndicators {
  static final StreamController<int> _streamController =
      StreamController<int>.broadcast();

  static Stream get stream => _streamController.stream;

  static void changeIndex(int index) => _streamController.add(index);

  static void dispose() => _streamController.close();
}

class TopCarousel extends StatefulWidget {
  final List items;
  final bool isDarkMode;

  TopCarousel({@required this.items, this.isDarkMode});

  @override
  State<StatefulWidget> createState() {
    return _TopCarouselState();
  }
}

class _TopCarouselState extends State<TopCarousel> {
  int _page = 0;

  final PageController _pageController = PageController();

  Timer _timer;

  bool _isAnimating = false;

  void _goToPage(int page) => page == 0
      ? _pageController.jumpToPage(0)
      : {
          _isAnimating = true,
          _pageController
              .animateToPage(
                _page,
                duration: const Duration(milliseconds: 350),
                curve: Curves.linear,
              )
              .whenComplete(() => _isAnimating = false),
        };

  void _setTimer() => _timer = Timer.periodic(
        const Duration(seconds: 3),
        (_) {
          _page < widget.items.length - 1 ? _page++ : _page = 0;
          _goToPage(_page);
          TopIndicators.changeIndex(_page);
        },
      );

  _launchURL(itemLink) async {
    var url = 'https://$itemLink';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void initState() {
    super.initState();
    _setTimer();
    _pageController.addListener(
      () {
        if (_page != _pageController.page.round() && !_isAnimating) {
          _timer.cancel();
          _page = _pageController.page.round();
          TopIndicators.changeIndex(_page);
          _setTimer();
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200.0,
      // height: (MediaQuery.of(context).size.width - 20) * (9 / 16) + 16,
      child: Stack(
        children: [
          CustomCarousel(
            pageController: _pageController,
            children: [
              for (var item in widget.items)
                GestureDetector(
                  child: item['IsAdvertisement'] != null
                      ? Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15.0),
                            image: DecorationImage(
                              image: NetworkImage(item['leadingImage']),
                              fit: BoxFit.cover,
                            ),
                          ),
                        )
                      : Container(
                          padding: EdgeInsets.all(10.0),
                          decoration: BoxDecoration(
                            color: widget.isDarkMode
                                ? Colors.grey[800]
                                : Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Container(
                                  // margin: EdgeInsets.only(left: 5.0),
                                  // child: Image.asset(
                                  //   item['leadingImage'],
                                  //   // height: 80.0,
                                  //   // width: 80.0,
                                  // ),
                                  child: Image.network(
                                    item['leadingImage'],
                                    height: 100.0,
                                    width: 100.0,
                                  ),
                                ),
                                Container(
                                  // margin: EdgeInsets.only(left: 5.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      item['titleText'] != null
                                          ? Flexible(
                                              child: Text(
                                                item['titleText'],
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .subtitle1,
                                              ),
                                            )
                                          : Container(),
                                      item['time'] == null
                                          ? Container()
                                          : item['time'] == "Exam Going On"
                                              ? Text(
                                                  item['time'],
                                                  style: TextStyle(
                                                    color: Colors.amber,
                                                    fontSize: 14.0,
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                )
                                              : CountdownTimer(
                                                  endTime: DateTime.now()
                                                          .millisecondsSinceEpoch +
                                                      1000 *
                                                          (60 *
                                                                  (int.parse(item[
                                                                              'time']
                                                                          [0]) *
                                                                      60) +
                                                              int.parse(
                                                                  item['time']
                                                                      [1])),
                                                  widgetBuilder: (_,
                                                      CurrentRemainingTime
                                                          time) {
                                                    if (time == null) {
                                                      return Text(
                                                        'Time Out',
                                                        style: TextStyle(
                                                          color: Colors.grey,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      );
                                                    }
                                                    if (time.hours == null &&
                                                        time.min == null) {
                                                      return Text(
                                                        '00H 00M ${time.sec}S',
                                                        style: TextStyle(
                                                          color: Colors.amber,
                                                          fontSize: 14.0,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                        ),
                                                      );
                                                    }
                                                    if (time.hours == null) {
                                                      return Text(
                                                        '00H ${time.min}M ${time.sec}S',
                                                        style: TextStyle(
                                                          color: Colors.amber,
                                                          fontSize: 14.0,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                        ),
                                                      );
                                                    }
                                                    return Text(
                                                      '${time.hours}H ${time.min}M ${time.sec}S',
                                                      style: TextStyle(
                                                        color: Colors.amber,
                                                        fontSize: 14.0,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                      ),
                                                    );
                                                  },
                                                ),
                                    ],
                                  ),
                                ),
                                Container(
                                  // margin: EdgeInsets.only(left: 5.0),
                                  child: Image.asset(
                                    item['trailerImage'],
                                    // height: 80.0,
                                    // width: 80.0,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                  onTap: () async {
                    item['IsAdvertisement'] != null
                        ? _launchURL(item['AdvertisementLink'])
                        : item['bannerExamId'] != null
                            ? Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => QuizInstructionSreen(
                                    examId: item['bannerExamId'],
                                  ),
                                ),
                              )
                            : Flushbar(
                                message: "Sorry exam not yet started.!!",
                                duration: Duration(seconds: 2),
                                backgroundColor: Colors.red,
                              ).show(context);
                  },
                ),
            ],
          ),
          Positioned(
            bottom: 14,
            left: 0,
            right: 0,
            child: IndicatorDots(
              stream: TopIndicators.stream,
              itemCount: widget.items.length,
              dotColor: Colors.white,
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    _pageController.dispose();
    super.dispose();
  }
}
