import 'package:flutter/material.dart';
// import 'package:hexcolor/hexcolor.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import './remarksDialogBox.dart';

class AnswerExplainDialogBox extends StatefulWidget {
  final int qstnNo;
  final List questionAndAnsList;
  final List answerKeyList;
  AnswerExplainDialogBox({
    this.qstnNo,
    this.questionAndAnsList,
    this.answerKeyList,
  });
  @override
  _AnswerExplainDialogBoxState createState() => _AnswerExplainDialogBoxState();
}

class _AnswerExplainDialogBoxState extends State<AnswerExplainDialogBox> {
  bool isDarkMode = false;
  bool isPageLoading = false;
  var counterr = 0;
  var qstnCount = 0;
  var question;
  var questionId;
  List answerList = [];
  var providerKey;
  var correctAnswer;
  var correctOptionChar;
  var answerExplanation;
  var userAnswer;
  var userAnsOptionChar;
  var correctAnsDisp;
  var userAnsDisp;
  var currViewingQstnId;
  List qstnAnsListQstnIDs = [];

  setQuestionAndAnswerExp(qstnNoToExp) {
    print("counterr:$counterr");
    print("${widget.questionAndAnsList.length} & $counterr");
    if (qstnNoToExp > 0) {
      print("Incoming");
      for (var i = 0; i < widget.answerKeyList.length; i++) {
        if (qstnNoToExp == widget.answerKeyList[i]['QuestionID']) {
          setState(() {
            currViewingQstnId = qstnNoToExp;
            // selected = null;
            for (var j = 0; j < widget.questionAndAnsList.length; j++) {
              counterr = j;
              qstnCount = j + 1;
              if (qstnNoToExp == widget.questionAndAnsList[j]['QuestionID']) {
                question = widget.questionAndAnsList[j]['QuestionTitle'];
                answerExplanation =
                    widget.questionAndAnsList[j]['AnswerDescription'];
                break;
              }
            }
            correctAnswer = widget.answerKeyList[i]['CorrectAnswerData'];
            var correctOption =
                widget.answerKeyList[i]['CorrectOption'].split("Option");
            correctOptionChar = correctOption[1];
            correctAnsDisp = "$correctOptionChar. $correctAnswer";
            userAnswer = widget.answerKeyList[i]['UserEnterdAnswerData'];
            var userAnsOption =
                widget.answerKeyList[i]['Answer'].split("Option");
            userAnsOptionChar = userAnsOption[1];
            userAnsDisp = widget.answerKeyList[i]['IsSkipped'] == true
                ? "Skipped"
                : "$userAnsOptionChar. $userAnswer";
          });
          break;
        }
      }
    }
  }

  prevOrNextNav(prevOrNext) {
    var qstnIdToView;
    var currQstnIdIndex = qstnAnsListQstnIDs.indexOf(currViewingQstnId);
    if (prevOrNext == "Prev") {
      qstnIdToView = qstnAnsListQstnIDs.elementAt(currQstnIdIndex - 1);
    } else {
      qstnIdToView = qstnAnsListQstnIDs.elementAt(currQstnIdIndex + 1);
    }
    print("qstnIdToView:$qstnIdToView");
    setQuestionAndAnswerExp(qstnIdToView);
  }

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<String> proKey =
        preferences.getString('ProviderKey', defaultValue: "");
    Preference<bool> isDark =
        preferences.getBool('isDark', defaultValue: false);
    setState(() {
      proKey.listen((value) {
        providerKey = value;
      });
      isDark.listen((value) {
        isDarkMode = value;
      });
    });
    // print("isDarkMode: $isDarkMode");
  }

  @override
  void initState() {
    super.initState();
    // print("questionAndAnsList: ${widget.questionAndAnsList}");
    for (var i = 0; i < widget.questionAndAnsList.length; i++) {
      var qstnIDs = widget.questionAndAnsList[i]['QuestionID'];
      qstnAnsListQstnIDs.add(qstnIDs);
    }
    getSharedDataPref();
    setQuestionAndAnswerExp(widget.qstnNo);
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.only(
        left: 8.0,
        right: 8.0,
      ),
      backgroundColor: Colors.white,
      insetAnimationDuration: const Duration(milliseconds: 100),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      child: Container(
        height: MediaQuery.of(context).size.height * 0.90,
        padding: EdgeInsets.all(10.0),
        child: isPageLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      IconButton(
                        icon: Icon(
                          Icons.close,
                          color: Colors.black,
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      )
                    ],
                  ),
                  Container(
                    height: 220.0,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Container(
                          height: 200.0,
                          decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Center(
                            child: Text(
                              question,
                              style: Theme.of(context).textTheme.headline4,
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        Positioned.fill(
                          child: Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              height: 40.0,
                              width: 100.0,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(25.0),
                                  bottomRight: Radius.circular(25.0),
                                ),
                              ),
                              child: Center(
                                child: Text(
                                  "$qstnCount/${widget.questionAndAnsList.length}",
                                  style: isDarkMode
                                      ? TextStyle(
                                          fontSize: 20.0,
                                          height: 1.4,
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                        )
                                      : Theme.of(context).textTheme.headline2,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Positioned.fill(
                          child: Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                              height: 50.0,
                              width: 50.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                              ),
                              child: Center(
                                child: Text(
                                  "?",
                                  style: isDarkMode
                                      ? TextStyle(
                                          fontSize: 20.0,
                                          height: 1.4,
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                        )
                                      : Theme.of(context).textTheme.headline2,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(8.0, 10.0, 8.0, 15.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Your Answer : ",
                              style: isDarkMode
                                  ? TextStyle(
                                      fontSize: 16.0,
                                      height: 1.4,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500,
                                    )
                                  : Theme.of(context).textTheme.headline1,
                            ),
                            Flexible(
                              child: Text(
                                userAnsDisp,
                                style: TextStyle(
                                  color: userAnsDisp == correctAnsDisp
                                      ? HexColor("#1fc09f")
                                      : HexColor("#f55b5d"),
                                  fontSize: 16.0,
                                  height: 1.4,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Correct Answer : ",
                              style: isDarkMode
                                  ? TextStyle(
                                      fontSize: 16.0,
                                      height: 1.4,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500,
                                    )
                                  : Theme.of(context).textTheme.headline1,
                            ),
                            Flexible(
                              child: Text(
                                correctAnsDisp,
                                style: TextStyle(
                                  color: HexColor("#1fc09f"),
                                  fontSize: 16.0,
                                  height: 1.4,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          "Answer Explanation",
                          style: isDarkMode
                              ? TextStyle(
                                  fontSize: 20.0,
                                  height: 1.4,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                )
                              : Theme.of(context).textTheme.headline2,
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        answerExplanation == null
                            ? Container()
                            : Text(
                                answerExplanation,
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          width: 10.0,
                        ),
                        ElevatedButton(
                          onPressed: () {
                            if (counterr != 0 &&
                                widget.questionAndAnsList.length != counterr) {
                              counterr -= 1;
                              prevOrNextNav("Prev");
                            }
                            if (counterr != 0 &&
                                widget.questionAndAnsList.length == counterr) {
                              counterr -= 2;
                              prevOrNextNav("Prev");
                            }
                          },
                          child: Text(
                            "Previous",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        ElevatedButton(
                          onPressed: () {
                            if (widget.questionAndAnsList.length > counterr) {
                              counterr += 1;
                              prevOrNextNav("Next");
                            }
                          },
                          child: Text(
                            "Next Question",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Center(
                      child: ElevatedButton(
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) =>
                                RemarksDialogBox(),
                          );
                        },
                        child: Text(
                          "Report Question",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
