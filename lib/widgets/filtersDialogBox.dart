import 'package:flutter/material.dart';

class FiltersDialogBox extends StatefulWidget {
  @override
  _FiltersDialogBoxState createState() => _FiltersDialogBoxState();
}

class _FiltersDialogBoxState extends State<FiltersDialogBox> {
  Map<String, bool> filterOptions = {
    "All": true,
    "Subject 1": false,
    "Subject 2": false,
    "Subject 3": false,
    "Subject 4": false,
    "Subject 5": false,
    "Subject 6": false,
    "Subject 7": false,
    "Subject 8": false,
  };

  List selectedfilterOptions = [];

  getItems(takeFilters) {
    if (takeFilters == "Ok") {
      filterOptions.forEach((key, value) {
        if (value == true) {
          selectedfilterOptions.add(key);
        }
      });
    } else {
      filterOptions.forEach((key, value) {
        setState(() {
          filterOptions[key] = key == "All" ? true : false;
        });
      });
      // Clear array after use.
      selectedfilterOptions.clear();
    }
    // Printing all selected items on Terminal screen.
    print("selectedfilterOptions: $selectedfilterOptions");
    // Here you will get all your selected Checkbox items.
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.only(
        left: 5.0,
        right: 30.0,
        bottom: 15.0,
      ),
      // backgroundColor: Colors.white,
      insetAnimationDuration: const Duration(milliseconds: 100),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(1)),
      child: Container(
        height: MediaQuery.of(context).size.height * 0.80,
        padding: EdgeInsets.all(10.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Filters",
                  style: Theme.of(context).textTheme.headline1,
                ),
                IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                )
              ],
            ),
            Expanded(
              child: ListView(
                children: filterOptions.keys.map((String key) {
                  return SizedBox(
                    height: 32.0,
                    child: CheckboxListTile(
                      contentPadding: EdgeInsets.all(0),
                      dense: true,
                      title: Text(
                        key,
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                      value: filterOptions[key],
                      activeColor: Theme.of(context).primaryColor,
                      checkColor: Colors.white,
                      controlAffinity: ListTileControlAffinity.leading,
                      onChanged: (bool value) {
                        setState(() {
                          filterOptions["All"] = false;
                          filterOptions[key] = value;
                        });
                      },
                    ),
                  );
                }).toList(),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      getItems("Ok");
                      Navigator.pop(context);
                    },
                    child: Text(
                      "Ok",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 5.0,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      getItems("Reset");
                    },
                    child: Text(
                      "Reset",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
