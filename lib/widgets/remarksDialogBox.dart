import 'package:flutter/material.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

class RemarksDialogBox extends StatefulWidget {
  @override
  _RemarksDialogBoxState createState() => _RemarksDialogBoxState();
}

class _RemarksDialogBoxState extends State<RemarksDialogBox> {
  bool isDarkMode = false;
  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<bool> isDark =
        preferences.getBool('isDark', defaultValue: false);

    setState(() {
      isDark.listen((value) {
        isDarkMode = value;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getSharedDataPref();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.only(
        left: 15.0,
        right: 15.0,
      ),
      backgroundColor: Colors.white,
      insetAnimationDuration: const Duration(milliseconds: 100),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      child: Container(
        height: 380.0,
        padding: EdgeInsets.all(15.0),
        child: ListView(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                  icon: Icon(
                    Icons.close,
                    color: Colors.black,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                )
              ],
            ),
            Text(
              "Remarks",
              style: isDarkMode
                  ? TextStyle(
                      fontSize: 18.0,
                      height: 1.4,
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontFamily: "assets/fonts/Stolzl-Bold.ttf",
                    )
                  : Theme.of(context).textTheme.headline6,
            ),
            SizedBox(
              height: 15.0,
            ),
            TextField(
              maxLines: 7,
              // controller: selectedDateCntlr,
              keyboardType: TextInputType.text,
              style: Theme.of(context).textTheme.bodyText1,
              decoration: InputDecoration(
                hintText: 'Write your remarks here',
                hintStyle: TextStyle(
                  fontSize: 14.0,
                  color: Colors.grey,
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: const BorderSide(color: Colors.black, width: 1.0),
                ),
                filled: true,
                contentPadding: EdgeInsets.all(16),
                fillColor: Colors.white70,
              ),
            ),
            SizedBox(
              height: 15.0,
            ),
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                margin: EdgeInsets.fromLTRB(60.0, 15.0, 60.0, 15.0),
                height: 45.0,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.circular(35.0),
                ),
                child: Center(
                  child: Text(
                    "Submit",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
