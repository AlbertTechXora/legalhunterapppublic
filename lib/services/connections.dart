library _constants;

const String appURL = "https://legalhunter.techxora.com";

//API Connections
const String applicationLogin = "$appURL/Application/ApplicationLogin?";
const String applicationSignUp = "$appURL/Application/ApplicationRegistration?";
const String applicationSocialMediaLogin =
    "$appURL/Application/ApplicationSocialMediaLogin?";
const String getCategoriesList = "$appURL/Application/GetCategories";
const String getExamListByCategory = "$appURL/Application/GetExamList?";
const String getExamDetails = "$appURL/Application/GetExamQuestionDetails?";
const String getNotification = "$appURL/Application/GetNotification";
const String getLatestFeeds = "$appURL/Application/GetLatestFeeds";
const String getSubscriptionPlans = "$appURL/Application/GetSubscriptionPlans";
const String getTrainingVideoList = "$appURL/Application/GetTrainingVideoList";
const String postUserSubscription = "$appURL/Application/PostUserSubscription?";
const String getUserProfile = "$appURL/Application/GetUserProfile?";
const String postUserExamDetail = "$appURL/Application/PostExamResults?";
const String updateUserProfile = "$appURL/Application/UpdateUserProfile?";
const String getVideoCategories = "$appURL/Application/GetVideoCategories";
const String updateUserProfileImage =
    "$appURL/Application/UpdateUserProfileImage";
const String updateUserSettings = "$appURL/Application/UpdateUserSettings?";
const String getExamScoreAndPerformance =
    "$appURL/Application/GetExamScoreAndPerformance?";
const String activateSubscription = "$appURL/Application/ActivateSubscription?";
const String forgotPassword = "$appURL/Application/ForgotPassword?";
const String resetForgotPassword = "$appURL/Application/ResetForgotPassword?";
