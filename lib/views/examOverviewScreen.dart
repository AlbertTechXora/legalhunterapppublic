import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';
// import 'package:hexcolor/hexcolor.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:another_flushbar/flushbar.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import '../helpers/appFunctions.dart';
import '../services/connections.dart' as Constants;

import '../widgets/myAppBar.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myNavBar.dart';
import '../widgets/noInternetWidget.dart';
import '../widgets/gradientText.dart';

import './answerSheetScreen.dart';
import './tryAnotherExamScreen.dart';

class ExamOverviewScreen extends StatefulWidget {
  final String overviewType;
  final Map currExamDetailData;
  final List questionAndAnsList;
  final int userSelectedParentCatId;
  final List userSelectedSubCatList;
  final List previousAttemptsList;
  final List examRankList;
  ExamOverviewScreen({
    @required this.overviewType,
    this.currExamDetailData,
    this.questionAndAnsList,
    this.userSelectedParentCatId,
    this.userSelectedSubCatList,
    this.previousAttemptsList,
    this.examRankList,
  });
  @override
  _ExamOverviewScreenState createState() => _ExamOverviewScreenState();
}

class _ExamOverviewScreenState extends State<ExamOverviewScreen> {
  var apiRequestStatus;
  var apiErrMessage;
  var connectionChk;
  bool isPageLoading = false;
  var providerKey;

  double totalQuestions;
  double attemptedQtns;
  double correctAnswers;
  double wrongAnswers;
  double unAnswered;
  var percentOfMarks;
  // var examDuration = "0h 30m 20s";
  // var examRankObtained = "365/525";
  var examDuration;
  var examRankObtained;
  var examAttendedOn;

  var correctAnsPercent;
  var wrongAnsPercent;
  var noAnsPercent;

  var questionList;

  Map<String, double> dataMap = {};
  Map summaryMap = {};

  List<Color> colorList = [
    // HexColor("#1fc09f"),
    // HexColor("#f55b5d"),
    // HexColor("#747474"),
    HexColor("#00ffae"),
    HexColor("#ff5c6d"),
    HexColor("#e3e1e1"),
  ];

  generateGraphDataMap() {
    dataMap["Correct Answer"] = correctAnswers;
    dataMap["Wrong Answer"] = wrongAnswers;
    dataMap["Unanswered"] = unAnswered;
    print("dataMap:$dataMap");
  }

  setExamOverViewDat() {
    if (widget.overviewType == "Previous") {
      getExamDetails(widget.currExamDetailData['ExamID']);
      setState(() {
        examAttendedOn = widget.currExamDetailData['ExamAttemptedOnString'];
      });
    }
    setState(() {
      totalQuestions = widget.currExamDetailData['TotalQuestions'] != null
          ? widget.currExamDetailData['TotalQuestions'].toDouble()
          : 0;
      attemptedQtns =
          widget.currExamDetailData['TotalQuestionsAttempted'] != null
              ? widget.currExamDetailData['TotalQuestionsAttempted'].toDouble()
              : 0;
      correctAnswers = widget.currExamDetailData['TotalCorrectAnswers'] != null
          ? widget.currExamDetailData['TotalCorrectAnswers'].toDouble()
          : 0;
      wrongAnswers = widget.currExamDetailData['TotalWrongAnswers'] != null
          ? widget.currExamDetailData['TotalWrongAnswers'].toDouble()
          : 0;
      examDuration = widget.currExamDetailData['TotalTimeTaken'] != null
          ? widget.currExamDetailData['TotalTimeTaken'].split(":")
          : null;
      examRankObtained = widget.currExamDetailData['ExamRank'] != null
          ? widget.currExamDetailData['ExamRank']
          : 0;
      percentOfMarks = (correctAnswers / totalQuestions) * 100;
      // print("percentOfMarks: $percentOfMarks");
      if (!percentOfMarks.isNaN) {
        percentOfMarks = percentOfMarks.toStringAsFixed(0);
      } else {
        percentOfMarks = 0;
      }
      unAnswered = totalQuestions - attemptedQtns;
      correctAnsPercent = (correctAnswers / totalQuestions) * 100;
      wrongAnsPercent = (wrongAnswers / totalQuestions) * 100;
      noAnsPercent = (unAnswered / totalQuestions) * 100;
    });
    generateGraphDataMap();
    generateSummaryDataMap();
  }

  generateSummaryDataMap() {
    summaryMap["Total Questions"] = totalQuestions.toStringAsFixed(0);
    summaryMap["Attempted"] = attemptedQtns.toStringAsFixed(0);
    summaryMap["Correct Answers"] = correctAnswers.toStringAsFixed(0);
    summaryMap["Wrong Answers"] = wrongAnswers.toStringAsFixed(0);
    summaryMap["Time"] = examDuration == null
        ? "00h 00m 00s"
        : "${examDuration[0]}h ${examDuration[1]}m ${examDuration[2]}s";
    summaryMap["Your Rank"] = examRankObtained;
    print("summaryMap:$summaryMap");
    // for (var entry in summaryMap.entries) {
    //   print("Key: ${entry.key}");
    //   print("Value: ${entry.value}");
    // }
  }

  getExamDetails(examId) async {
    setState(() {
      isPageLoading = true;
    });
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      //multipart/form-data POST Method
      Map<String, String> requestBody = <String, String>{
        "ExamID": examId.toString(),
      };
      // Map<String, String> headers = <String, String>{
      //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
      // };
      var uri = Uri.parse(Constants.getExamDetails);
      var request = http.MultipartRequest('POST', uri)
        // ..headers.addAll(
        //     headers) //if u have headers, basic auth, token bearer... Else remove line
        ..fields.addAll(requestBody);
      var response = await request.send();
      final respStr = await response.stream.bytesToString();
      //

      print("response.request:${response.request}");
      if (response.statusCode == 200) {
        var data = json.decode(respStr);
        // print("data: $data");
        if (data != null) {
          apiRequestStatus = data['Status'];
          print("apiRequestStatus: $apiRequestStatus");
          if (apiRequestStatus == "Success") {
            setState(() {
              questionList = data['QuestionList'];
            });
            // print("questionListOverview: $questionList");
          } else {
            apiErrMessage = data['Errors'][0];
            Flushbar(
              message: apiErrMessage,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          apiRequestStatus = false;
          apiErrMessage = "No data found.";
          Flushbar(
            message: apiErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        apiRequestStatus = false;
        apiErrMessage = "Error: ${response.statusCode}.";
        Flushbar(
          message: apiErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      apiRequestStatus = false;
      apiErrMessage = connectionChk;
      Flushbar(
        message: apiErrMessage,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
  }

  getExamPrevData(userExamHistoryID) async {
    setState(() {
      isPageLoading = true;
    });
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      //multipart/form-data POST Method
      Map<String, String> requestBody = <String, String>{
        "ApplicationUserExamHistoryID": userExamHistoryID.toString(),
        "ProviderKey": providerKey,
      };
      print("requestBody:$requestBody");
      // Map<String, String> headers = <String, String>{
      //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
      // };
      var uri = Uri.parse(Constants.getExamScoreAndPerformance);
      var request = http.MultipartRequest('POST', uri)
        // ..headers.addAll(
        //     headers) //if u have headers, basic auth, token bearer... Else remove line
        ..fields.addAll(requestBody);
      var response = await request.send();
      final respStr = await response.stream.bytesToString();
      //

      print("response.request:${response.request}");
      if (response.statusCode == 200) {
        var data = json.decode(respStr);
        // print("data: $data");
        if (data != null) {
          apiRequestStatus = data['Status'];
          print("apiRequestStatus: $apiRequestStatus");
          if (apiRequestStatus == "Success") {
            var examResult = data['ExamResult'];
            var examRankList = data['ExamRankList'];
            var lastAttemptsForCurrentExam = data['LastAttemptsForCurrentExam'];
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => ExamOverviewScreen(
                  overviewType: "Previous",
                  currExamDetailData: examResult,
                  previousAttemptsList: lastAttemptsForCurrentExam,
                  examRankList: examRankList,
                ),
              ),
            );
          } else {
            apiErrMessage = data['Errors'][0];
            Flushbar(
              message: apiErrMessage,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          apiRequestStatus = false;
          apiErrMessage = "No data found.";
          Flushbar(
            message: apiErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        apiRequestStatus = false;
        apiErrMessage = "Error: ${response.statusCode}.";
        Flushbar(
          message: apiErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      apiRequestStatus = false;
      apiErrMessage = connectionChk;
      Flushbar(
        message: apiErrMessage,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
  }

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<String> proKey =
        preferences.getString('ProviderKey', defaultValue: "");
    setState(() {
      providerKey = proKey.getValue();
    });
  }

  @override
  void initState() {
    super.initState();
    getSharedDataPref();
    setExamOverViewDat();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(),
      bottomNavigationBar: MyBottomNavBar(),
      drawer: MyNavDrawer(),
      body: isPageLoading
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : connectionChk != null
              ? Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      NoInternetWidget(),
                      ElevatedButton(
                        onPressed: () {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    super.widget),
                          );
                        },
                        child: Text("Retry"),
                      ),
                    ],
                  ),
                )
              : Container(
                  child: ListView(
                    children: [
                      widget.overviewType == "Previous" &&
                              widget.previousAttemptsList != null
                          // widget.overviewType == "Previous"
                          ? Container(
                              width: MediaQuery.of(context).size.width,
                              height: 50.0,
                              margin: EdgeInsets.only(top: 10.0, left: 10.0),
                              child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                // itemCount: widget.previousAttemptsList.length,
                                itemCount: 1,
                                itemBuilder: (BuildContext context, index) {
                                  return GestureDetector(
                                    onTap: () {
                                      getExamPrevData(
                                          widget.previousAttemptsList[index]
                                              ['ApplicationUserExamHistoryID']);
                                      // getExamPrevData(258);
                                    },
                                    child: Container(
                                      margin: EdgeInsets.all(3.0),
                                      padding: EdgeInsets.all(3.0),
                                      width: 130.0,
                                      decoration: BoxDecoration(
                                        color:
                                            Theme.of(context).primaryColorLight,
                                        borderRadius:
                                            BorderRadius.circular(25.0),
                                      ),
                                      child: Center(
                                        child: Text(
                                          widget.previousAttemptsList[index]
                                              ['ExamName'],
                                          // "Dummy",
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline4,
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            )
                          : Container(),
                      // widget.overviewType == "Previous"
                      //     ? Container(
                      //         margin: EdgeInsets.all(10.0),
                      //         child: Row(
                      //           mainAxisAlignment:
                      //               MainAxisAlignment.spaceBetween,
                      //           children: [
                      //             Text(
                      //               "EXAM OVERVIEW",
                      //               style:
                      //                   Theme.of(context).textTheme.headline3,
                      //             ),
                      //             Text(
                      //               examAttendedOn,
                      //               style: TextStyle(
                      //                 color: Colors.grey,
                      //                 fontWeight: FontWeight.w500,
                      //               ),
                      //             )
                      //           ],
                      //         ),
                      //       )
                      //     : Container(
                      //         margin: EdgeInsets.only(top: 25.0),
                      //         child: Center(
                      //           child: Text(
                      //             "EXAM OVERVIEW",
                      //             style: Theme.of(context).textTheme.headline2,
                      //           ),
                      //         ),
                      //       ),
                      // Container(
                      //   margin: EdgeInsets.fromLTRB(10.0, 20.0, 10.0, 20.0),
                      //   child: Row(
                      //     mainAxisAlignment: MainAxisAlignment.spaceAround,
                      //     crossAxisAlignment: CrossAxisAlignment.center,
                      //     children: [
                      //       PieChart(
                      //         dataMap: dataMap,
                      //         animationDuration: Duration(milliseconds: 800),
                      //         chartLegendSpacing: 32,
                      //         chartRadius:
                      //             MediaQuery.of(context).size.width / 3,
                      //         colorList: colorList,
                      //         initialAngleInDegree: 0,
                      //         chartType: ChartType.ring,
                      //         ringStrokeWidth: 30,
                      //         centerText: "$percentOfMarks%",
                      //         legendOptions: LegendOptions(
                      //           showLegendsInRow: false,
                      //           legendPosition: LegendPosition.right,
                      //           showLegends: false,
                      //           legendShape: BoxShape.rectangle,
                      //           legendTextStyle: TextStyle(
                      //             color: Theme.of(context).primaryColor,
                      //             fontWeight: FontWeight.bold,
                      //           ),
                      //         ),
                      //         chartValuesOptions: ChartValuesOptions(
                      //           chartValueStyle: TextStyle(
                      //             color: HexColor("#1fc09f"),
                      //             fontWeight: FontWeight.bold,
                      //             fontSize: 30.0,
                      //           ),
                      //           showChartValueBackground: false,
                      //           showChartValues: false,
                      //           showChartValuesInPercentage: false,
                      //           showChartValuesOutside: false,
                      //         ),
                      //       ),
                      //       Container(
                      //         // margin: EdgeInsets.only(left: 15.0),
                      //         child: Column(
                      //           children: [
                      //             Container(
                      //               height: 50.0,
                      //               width: 190.0,
                      //               margin: EdgeInsets.only(bottom: 10.0),
                      //               decoration: BoxDecoration(
                      //                 color: HexColor("#D7F5ED"),
                      //                 borderRadius: BorderRadius.circular(25.0),
                      //               ),
                      //               child: Row(
                      //                 crossAxisAlignment:
                      //                     CrossAxisAlignment.center,
                      //                 children: [
                      //                   Container(
                      //                     height: 40.0,
                      //                     width: 65.0,
                      //                     margin: EdgeInsets.only(
                      //                         left: 5.0, right: 5.0),
                      //                     decoration: BoxDecoration(
                      //                       color: HexColor("#1fc09f"),
                      //                       borderRadius:
                      //                           BorderRadius.circular(25.0),
                      //                     ),
                      //                     child: Center(
                      //                       child: Text(
                      //                         correctAnswers.toStringAsFixed(0),
                      //                         style: Theme.of(context)
                      //                             .textTheme
                      //                             .subtitle1,
                      //                       ),
                      //                     ),
                      //                   ),
                      //                   Text(
                      //                     "Correct",
                      //                     style: TextStyle(
                      //                       color: HexColor("#1fc09f"),
                      //                       fontSize: 16.0,
                      //                       height: 1.4,
                      //                       fontWeight: FontWeight.w500,
                      //                     ),
                      //                   ),
                      //                 ],
                      //               ),
                      //             ),
                      //             Container(
                      //               height: 50.0,
                      //               width: 190.0,
                      //               margin: EdgeInsets.only(bottom: 10.0),
                      //               decoration: BoxDecoration(
                      //                 color: HexColor("#FBE6E5"),
                      //                 borderRadius: BorderRadius.circular(25.0),
                      //               ),
                      //               child: Row(
                      //                 crossAxisAlignment:
                      //                     CrossAxisAlignment.center,
                      //                 children: [
                      //                   Container(
                      //                     height: 40.0,
                      //                     width: 65.0,
                      //                     margin: EdgeInsets.only(
                      //                         left: 5.0, right: 5.0),
                      //                     decoration: BoxDecoration(
                      //                       color: HexColor("#f55b5d"),
                      //                       borderRadius:
                      //                           BorderRadius.circular(25.0),
                      //                     ),
                      //                     child: Center(
                      //                       child: Text(
                      //                         wrongAnswers.toStringAsFixed(0),
                      //                         style: Theme.of(context)
                      //                             .textTheme
                      //                             .subtitle1,
                      //                       ),
                      //                     ),
                      //                   ),
                      //                   Text(
                      //                     "Wrong",
                      //                     style: TextStyle(
                      //                       color: HexColor("#f55b5d"),
                      //                       fontSize: 16.0,
                      //                       height: 1.4,
                      //                       fontWeight: FontWeight.w500,
                      //                     ),
                      //                   ),
                      //                 ],
                      //               ),
                      //             ),
                      //             Container(
                      //               height: 50.0,
                      //               width: 190.0,
                      //               margin: EdgeInsets.only(bottom: 10.0),
                      //               decoration: BoxDecoration(
                      //                 color: HexColor("#F4F4F4"),
                      //                 borderRadius: BorderRadius.circular(25.0),
                      //               ),
                      //               child: Row(
                      //                 crossAxisAlignment:
                      //                     CrossAxisAlignment.center,
                      //                 children: [
                      //                   Container(
                      //                     height: 40.0,
                      //                     width: 65.0,
                      //                     margin: EdgeInsets.only(
                      //                         left: 5.0, right: 5.0),
                      //                     decoration: BoxDecoration(
                      //                       color: HexColor("#747474"),
                      //                       borderRadius:
                      //                           BorderRadius.circular(25.0),
                      //                     ),
                      //                     child: Center(
                      //                       child: Text(
                      //                         dataMap["Unanswered"]
                      //                             .toStringAsFixed(0),
                      //                         style: Theme.of(context)
                      //                             .textTheme
                      //                             .subtitle1,
                      //                       ),
                      //                     ),
                      //                   ),
                      //                   Text(
                      //                     "Unanswered",
                      //                     style: TextStyle(
                      //                       color: HexColor("#747474"),
                      //                       fontSize: 16.0,
                      //                       height: 1.4,
                      //                       fontWeight: FontWeight.w500,
                      //                     ),
                      //                   ),
                      //                 ],
                      //               ),
                      //             ),
                      //           ],
                      //         ),
                      //       ),
                      //     ],
                      //   ),
                      // ),
                      Card(
                        elevation: 5.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        margin: EdgeInsets.fromLTRB(10.0, 20.0, 10.0, 20.0),
                        child: Container(
                          padding: EdgeInsets.all(10.0),
                          child: Column(
                            children: [
                              widget.overviewType == "Previous"
                                  ? Container(
                                      margin: EdgeInsets.all(10.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "EXAM OVERVIEW",
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline3,
                                          ),
                                          Text(
                                            examAttendedOn,
                                            style: TextStyle(
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w500,
                                            ),
                                          )
                                        ],
                                      ),
                                    )
                                  : Container(
                                      margin: EdgeInsets.all(10.0),
                                      child: Center(
                                        child: Text(
                                          "EXAM OVERVIEW",
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline2,
                                        ),
                                      ),
                                    ),
                              Container(
                                margin: EdgeInsets.only(top: 10.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      child: Column(
                                        children: [
                                          Container(
                                            height: 50.0,
                                            width: 150.0,
                                            margin:
                                                EdgeInsets.only(bottom: 10.0),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Container(
                                                  height: 50.0,
                                                  width: 5.0,
                                                  margin: EdgeInsets.only(
                                                      left: 5.0, right: 5.0),
                                                  decoration: BoxDecoration(
                                                    gradient: LinearGradient(
                                                      begin:
                                                          Alignment.topCenter,
                                                      end: Alignment
                                                          .bottomCenter,
                                                      colors: [
                                                        HexColor("#00ffae"),
                                                        HexColor("#aff8d5"),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      "Correct Answer",
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText1,
                                                    ),
                                                    GradientText(
                                                      "${correctAnsPercent.toStringAsFixed(0)}%",
                                                      fontSize: 16.0,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      gradient: LinearGradient(
                                                          colors: [
                                                            HexColor("#00ffae"),
                                                            HexColor("#aff8d5"),
                                                          ]),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            height: 50.0,
                                            width: 150.0,
                                            margin:
                                                EdgeInsets.only(bottom: 10.0),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Container(
                                                  height: 50.0,
                                                  width: 5.0,
                                                  margin: EdgeInsets.only(
                                                      left: 5.0, right: 5.0),
                                                  decoration: BoxDecoration(
                                                    gradient: LinearGradient(
                                                      begin:
                                                          Alignment.topCenter,
                                                      end: Alignment
                                                          .bottomCenter,
                                                      colors: [
                                                        HexColor("#ff5c6d"),
                                                        HexColor("#fc8297"),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      "Wrong Answer",
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText1,
                                                    ),
                                                    GradientText(
                                                      "${wrongAnsPercent.toStringAsFixed(0)}%",
                                                      fontSize: 16.0,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      gradient: LinearGradient(
                                                          colors: [
                                                            HexColor("#ff5c6d"),
                                                            HexColor("#fc8297"),
                                                          ]),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            height: 50.0,
                                            width: 150.0,
                                            margin:
                                                EdgeInsets.only(bottom: 10.0),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Container(
                                                  height: 50.0,
                                                  width: 5.0,
                                                  margin: EdgeInsets.only(
                                                      left: 5.0, right: 5.0),
                                                  decoration: BoxDecoration(
                                                    gradient: LinearGradient(
                                                      begin:
                                                          Alignment.topCenter,
                                                      end: Alignment
                                                          .bottomCenter,
                                                      colors: [
                                                        HexColor("#e3e1e1"),
                                                        HexColor("#fff"),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      "No Answer",
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText1,
                                                    ),
                                                    GradientText(
                                                      "${noAnsPercent.toStringAsFixed(0)}%",
                                                      fontSize: 16.0,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      gradient: LinearGradient(
                                                          colors: [
                                                            HexColor("#e3e1e1"),
                                                            HexColor("#fff"),
                                                          ]),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    PieChart(
                                      dataMap: dataMap,
                                      animationDuration:
                                          Duration(milliseconds: 800),
                                      chartLegendSpacing: 32,
                                      chartRadius:
                                          MediaQuery.of(context).size.width /
                                              2.5,
                                      colorList: colorList,
                                      initialAngleInDegree: 0,
                                      chartType: ChartType.disc,
                                      ringStrokeWidth: 30,
                                      // centerText: "$percentOfMarks%",
                                      legendOptions: LegendOptions(
                                        showLegendsInRow: false,
                                        legendPosition: LegendPosition.right,
                                        showLegends: false,
                                        legendShape: BoxShape.rectangle,
                                        legendTextStyle: TextStyle(
                                          color: Theme.of(context).primaryColor,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      chartValuesOptions: ChartValuesOptions(
                                        chartValueStyle: TextStyle(
                                          color: HexColor("#1fc09f"),
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30.0,
                                        ),
                                        showChartValueBackground: false,
                                        showChartValues: false,
                                        showChartValuesInPercentage: false,
                                        showChartValuesOutside: false,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        child: Column(
                          children: [
                            Text(
                              "RESULT SUMMARY",
                              style: Theme.of(context).textTheme.headline3,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(15.0),
                        child: Card(
                          elevation: 5.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Container(
                            padding: EdgeInsets.all(15.0),
                            child: Column(
                              children: [
                                // for (var i = 0; i < resultSummary.length; i++)
                                //   Column(
                                //     children: [
                                //       Row(
                                //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                //         children: [
                                //           Text(
                                //             resultSummary[i]['SummaryTitle'],
                                //             style: Theme.of(context).textTheme.subtitle2,
                                //           ),
                                //           Text(
                                //             resultSummary[i]['SummaryValue'],
                                //             style: Theme.of(context).textTheme.bodyText1,
                                //           ),
                                //         ],
                                //       ),
                                //       Divider(
                                //         thickness: 2,
                                //       ),
                                //     ],
                                //   ),
                                for (var entry in summaryMap.entries)
                                  Column(
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "${entry.key}",
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle2,
                                          ),
                                          Text(
                                            "${entry.value}",
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText1,
                                          ),
                                        ],
                                      ),
                                      Divider(
                                        thickness: 2,
                                      ),
                                    ],
                                  ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(
                              width: 10.0,
                            ),
                            ElevatedButton(
                              onPressed: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) => AnswerSheetScreen(
                                      answerKeyList: widget
                                          .currExamDetailData['QuestionList'],
                                      questionAndAnsList:
                                          widget.overviewType == "Previous"
                                              ? questionList
                                              : widget.questionAndAnsList,
                                      examRankList:
                                          widget.overviewType == "Previous"
                                              ? widget.examRankList
                                              : widget.currExamDetailData[
                                                  'ExamRankList'],
                                      userSelectedParentCatId:
                                          widget.userSelectedParentCatId,
                                      userSelectedSubCatList:
                                          widget.userSelectedSubCatList,
                                    ),
                                  ),
                                );
                              },
                              child: Text(
                                "Answer Review",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            ElevatedButton(
                              onPressed: () {
                                // print(
                                //     "widget.userSelectedParentCatId: ${widget.userSelectedParentCatId}");
                                // print(
                                //     "widget.userSelectedSubCatList: ${widget.userSelectedSubCatList}");
                                widget.userSelectedParentCatId != null &&
                                        widget.userSelectedSubCatList != null
                                    ? Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              TryAnotherExamScreen(
                                            examCatId:
                                                widget.userSelectedParentCatId,
                                            subCategoryList:
                                                widget.userSelectedSubCatList,
                                          ),
                                        ),
                                      )
                                    : Flushbar(
                                        message:
                                            "Sorry.!! Option not available right now.",
                                        duration: Duration(seconds: 2),
                                        backgroundColor: Colors.red,
                                      ).show(context);
                              },
                              child: Text(
                                "Try Another Exam",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
    );
  }
}
