import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:another_flushbar/flushbar.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import '../helpers/appFunctions.dart';
import '../services/connections.dart' as Constants;

import '../widgets/myAppBar.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myNavBar.dart';
import '../widgets/noInternetWidget.dart';

import './examOverviewScreen.dart';

class ExamDetailsScreen extends StatefulWidget {
  final int userExamCategoryId;
  final List userAttemptedExamDetails;
  ExamDetailsScreen({
    this.userExamCategoryId,
    this.userAttemptedExamDetails,
  });
  @override
  _ExamDetailsScreenState createState() => _ExamDetailsScreenState();
}

class _ExamDetailsScreenState extends State<ExamDetailsScreen> {
  var apiRequestStatus;
  var apiErrMessage;
  var connectionChk;
  bool isPageLoading = false;
  var providerKey;
  List userAttemptedExamList = [];

  createCategoryWiseDetailList() {
    print("widget.userExamCategoryId: ${widget.userExamCategoryId}");
    // print(
    //     "widget.userAttemptedExamDetails: ${widget.userAttemptedExamDetails}");
    if (widget.userAttemptedExamDetails.length > 0) {
      for (var i = 0; i < widget.userAttemptedExamDetails.length; i++) {
        if (widget.userExamCategoryId ==
            widget.userAttemptedExamDetails[i]['ExamCategoryIDf']) {
          var tempUsersExamDetMap = {
            "ExamID": widget.userAttemptedExamDetails[i]['ExamID'],
            "ExamCategoryIDf": widget.userAttemptedExamDetails[i]
                ['ExamCategoryIDf'],
            "ExamName": widget.userAttemptedExamDetails[i]['ExamName'],
            "ExamAttemptedOnFormatted": widget.userAttemptedExamDetails[i]
                ['ExamAttemptedOnFormatted'],
            "ApplicationUserExamHistoryID": widget.userAttemptedExamDetails[i]
                ['ApplicationUserExamHistoryID'],
          };
          userAttemptedExamList.add(tempUsersExamDetMap);
        }
      }
    }
  }

  getResultOftheExam(useExamHistoryId) async {
    setState(() {
      isPageLoading = true;
    });
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      //multipart/form-data POST Method
      Map<String, String> requestBody = <String, String>{
        "ApplicationUserExamHistoryID": useExamHistoryId.toString(),
        "ProviderKey": providerKey,
      };
      print("requestBody:$requestBody");
      // Map<String, String> headers = <String, String>{
      //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
      // };
      var uri = Uri.parse(Constants.getExamScoreAndPerformance);
      var request = http.MultipartRequest('POST', uri)
        // ..headers.addAll(
        //     headers) //if u have headers, basic auth, token bearer... Else remove line
        ..fields.addAll(requestBody);
      var response = await request.send();
      final respStr = await response.stream.bytesToString();
      //

      print("response.request:${response.request}");
      if (response.statusCode == 200) {
        var data = json.decode(respStr);
        // print("data: $data");
        if (data != null) {
          apiRequestStatus = data['Status'];
          print("apiRequestStatus: $apiRequestStatus");
          if (apiRequestStatus == "Success") {
            var examResult = data['ExamResult'];
            var examRankList = data['ExamRankList'];
            var lastAttemptsForCurrentExam = data['LastAttemptsForCurrentExam'];
            var selectedParentCategoryId =
                data['ExamResult']['ParentCategoryID'];
            var examSubCategoryList;
            connectionChk = await AppFuntions.connectionCheck();
            print("connectionChk: $connectionChk");
            if (connectionChk == null) {
              //multipart/form-data POST Method
              var uri = Uri.parse(Constants.getCategoriesList);
              var request = http.MultipartRequest('POST', uri);
              var response = await request.send();
              final respStr = await response.stream.bytesToString();

              print("response.request:${response.request}");
              if (response.statusCode == 200) {
                var data = json.decode(respStr);
                // print("data: $data");
                if (data != null) {
                  apiRequestStatus = data['Status'];
                  print("apiRequestStatus: $apiRequestStatus");
                  if (apiRequestStatus == "Success") {
                    // print("Yes");
                    // setState(() {
                    examSubCategoryList = data['ChildCategoryList'];
                    // });
                  }
                }
              }
            }

            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => ExamOverviewScreen(
                  overviewType: "Previous",
                  currExamDetailData: examResult,
                  previousAttemptsList: lastAttemptsForCurrentExam,
                  examRankList: examRankList,
                  userSelectedParentCatId: selectedParentCategoryId,
                  userSelectedSubCatList: examSubCategoryList,
                ),
              ),
            );
          } else {
            apiErrMessage = data['Errors'][0];
            Flushbar(
              message: apiErrMessage,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          apiRequestStatus = false;
          apiErrMessage = "No data found.";
          Flushbar(
            message: apiErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        apiRequestStatus = false;
        apiErrMessage = "Error: ${response.statusCode}.";
        Flushbar(
          message: apiErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      apiRequestStatus = false;
      apiErrMessage = connectionChk;
      Flushbar(
        message: apiErrMessage,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
  }

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<String> proKey =
        preferences.getString('ProviderKey', defaultValue: "");
    setState(() {
      providerKey = proKey.getValue();
    });
  }

  @override
  void initState() {
    super.initState();
    getSharedDataPref();
    createCategoryWiseDetailList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(),
      bottomNavigationBar: MyBottomNavBar(),
      drawer: MyNavDrawer(),
      body: isPageLoading
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : connectionChk != null
              ? Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      NoInternetWidget(),
                      ElevatedButton(
                        onPressed: () {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    super.widget),
                          );
                        },
                        child: Text("Retry"),
                      ),
                    ],
                  ),
                )
              : Container(
                  child: ListView(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10.0),
                        child: Column(
                          children: [
                            Text(
                              "Exam Details",
                              style: Theme.of(context).textTheme.headline6,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10.0, left: 15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Exam Name",
                              style: TextStyle(
                                color: Theme.of(context).primaryColorLight,
                                fontSize: 16.0,
                                height: 1.4,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            SizedBox(width: 10.0),
                            Text(
                              "Date",
                              style: TextStyle(
                                color: Theme.of(context).primaryColorLight,
                                fontSize: 16.0,
                                height: 1.4,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            SizedBox(width: 10.0),
                            Container(
                              width: 100.0,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        // height: 50.0,
                        // margin: EdgeInsets.only(top: 10.0),
                        child: ListView.builder(
                          scrollDirection: Axis.vertical,
                          itemCount: userAttemptedExamList.length,
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemBuilder: (BuildContext context, index) {
                            return Container(
                              margin: EdgeInsets.all(5.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Card(
                                elevation: 5.0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0)),
                                child: Container(
                                  padding: EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Flexible(
                                        child: Text(
                                          userAttemptedExamList[index]
                                              ['ExamName'],
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorLight,
                                            fontSize: 14.0,
                                            height: 1.4,
                                          ),
                                        ),
                                      ),
                                      SizedBox(width: 10.0),
                                      Text(
                                        userAttemptedExamList[index]
                                            ['ExamAttemptedOnFormatted'],
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .primaryColorLight,
                                          fontSize: 14.0,
                                          height: 1.4,
                                        ),
                                      ),
                                      SizedBox(width: 10.0),
                                      GestureDetector(
                                        onTap: () {
                                          getResultOftheExam(userAttemptedExamList[
                                                  index]
                                              ['ApplicationUserExamHistoryID']);
                                        },
                                        child: Container(
                                          width: 105.0,
                                          height: 30.0,
                                          decoration: BoxDecoration(
                                            color:
                                                Theme.of(context).primaryColor,
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                          ),
                                          child: Center(
                                            child: Text(
                                              "View Result",
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
    );
  }
}
