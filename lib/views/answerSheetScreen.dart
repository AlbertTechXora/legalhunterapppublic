import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import '../widgets/myAppBar.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myNavBar.dart';
import '../widgets/answerExplainDialogBox.dart';

import './ranklistScreen.dart';
import './tryAnotherExamScreen.dart';

class AnswerSheetScreen extends StatefulWidget {
  final List answerKeyList;
  final List questionAndAnsList;
  final List examRankList;
  final int userSelectedParentCatId;
  final List userSelectedSubCatList;
  AnswerSheetScreen({
    this.answerKeyList,
    this.questionAndAnsList,
    this.examRankList,
    this.userSelectedParentCatId,
    this.userSelectedSubCatList,
  });
  @override
  _AnswerSheetScreenState createState() => _AnswerSheetScreenState();
}

class _AnswerSheetScreenState extends State<AnswerSheetScreen> {
  // var k = 1;
  // var rowcount;
  bool isDarkMode = false;
  List answerSheetList = [];
  createAnswerSheetListCustom() {
    if (widget.answerKeyList != null && widget.questionAndAnsList != null) {
      for (var k = 0; k < widget.questionAndAnsList.length; k++) {
        var questionAndAnsQstnId = widget.questionAndAnsList[k]['QuestionID'];
        for (var i = 0; i < widget.answerKeyList.length; i++) {
          var optionSelected =
              widget.answerKeyList[i]['Answer'].split("Option");
          var isCorrectAnswer = widget.answerKeyList[i]['IsCorrectAnswer'];
          if (questionAndAnsQstnId == widget.answerKeyList[i]['QuestionID']) {
            var tempAnsSheetMap = {
              "QuestionID": widget.answerKeyList[i]['QuestionID'],
              "Answer": optionSelected[1],
              "IsSkipped": widget.answerKeyList[i]['IsSkipped'],
              "IsCorrect": isCorrectAnswer == true ? "Yes" : "No",
            };
            // print("tempAnsSheetMap: $tempAnsSheetMap");
            answerSheetList.add(tempAnsSheetMap);
          }
        }
      }
    }
  }

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<bool> isDark =
        preferences.getBool('isDark', defaultValue: false);
    isDark.listen((value) {
      setState(() {
        isDarkMode = value;
      });
    });
    // print("isDarkMode: $isDarkMode");
  }

  @override
  void initState() {
    super.initState();
    getSharedDataPref();
    // widget.answerKeyList.length > 5
    //     ? rowcount = 5
    //     : rowcount = widget.answerKeyList.length;
    createAnswerSheetListCustom();
    // print("RanklistOnAnsSheet: ${widget.examRankList}");
    // print("AnswerKeyListOnAnsSheet: ${widget.answerKeyList}");
    // print("QuestionAndAnsListOnAnsSheet: ${widget.questionAndAnsList}");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(),
      bottomNavigationBar: MyBottomNavBar(),
      drawer: MyNavDrawer(),
      body: Container(
        margin: EdgeInsets.all(15.0),
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.only(
                top: 15.0,
                bottom: 15.0,
              ),
              child: Center(
                child: Text(
                  "Answer Sheet",
                  style: Theme.of(context).textTheme.headline5,
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                color: isDarkMode
                    ? Theme.of(context).primaryColorLight
                    : Theme.of(context).primaryColor,
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: answerSheetList.isEmpty
                  ? Center(
                      child: Text(
                      "No Data Found.",
                      style: Theme.of(context).textTheme.headline4,
                    ))
                  : GridView.count(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      crossAxisCount: 5,
                      childAspectRatio: 1.5,
                      padding: const EdgeInsets.all(5.0),
                      crossAxisSpacing: 1,
                      children: List.generate(answerSheetList.length, (index) {
                        return Container(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Center(
                                child: Text(
                                  index < 9 ? "0${index + 1}" : "${index + 1}",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) =>
                                        AnswerExplainDialogBox(
                                      qstnNo: answerSheetList[index]
                                          ['QuestionID'],
                                      answerKeyList: widget.answerKeyList,
                                      questionAndAnsList:
                                          widget.questionAndAnsList,
                                    ),
                                  );
                                },
                                child: Container(
                                  padding: EdgeInsets.all(5.0),
                                  margin: EdgeInsets.only(left: 3.0),
                                  height: 35.0,
                                  width: 35.0,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  child: Center(
                                    child: answerSheetList[index]
                                                ['IsSkipped'] ==
                                            true
                                        ? Container()
                                        : Text(
                                            "${answerSheetList[index]['Answer']}",
                                            style: TextStyle(
                                              color: answerSheetList[index]
                                                          ['IsCorrect'] ==
                                                      "Yes"
                                                  ? Colors.green
                                                  : Colors.red,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18.0,
                                            ),
                                          ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      }),
                    ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: 15.0,
                bottom: 15.0,
              ),
              child: Center(
                child: Text(
                  "Note : Click your answer button and know\nthe correct answer and its explanation.",
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: 10.0,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      widget.examRankList != null
                          ? Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => RanklistScreen(
                                  rankList: widget.examRankList,
                                ),
                              ),
                            )
                          : Flushbar(
                              message: "Sorry.!! No Rank List Found.",
                              duration: Duration(seconds: 2),
                              backgroundColor: Colors.red,
                            ).show(context);
                    },
                    child: Text(
                      "Rank List",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 13.0,
                      ),
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      widget.userSelectedParentCatId != null &&
                              widget.userSelectedSubCatList != null
                          ? Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => TryAnotherExamScreen(
                                  examCatId: widget.userSelectedParentCatId,
                                  subCategoryList:
                                      widget.userSelectedSubCatList,
                                ),
                              ),
                            )
                          : Flushbar(
                              message:
                                  "Sorry.!! Option not available right now.",
                              duration: Duration(seconds: 2),
                              backgroundColor: Colors.red,
                            ).show(context);
                    },
                    child: Text(
                      "Try Another Exam",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 13.0,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
