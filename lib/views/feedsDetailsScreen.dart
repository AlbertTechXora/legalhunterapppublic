import 'package:flutter/material.dart';

import '../widgets/myAppBar.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myNavBar.dart';

class FeedsDetailsScreen extends StatefulWidget {
  final String feedsImage;
  final String feedsTitle;
  final String feedsDesc;
  final String feedsTimeStamp;
  FeedsDetailsScreen({
    this.feedsImage,
    this.feedsTitle,
    this.feedsDesc,
    this.feedsTimeStamp,
  });
  @override
  _FeedsDetailsScreenState createState() => _FeedsDetailsScreenState();
}

class _FeedsDetailsScreenState extends State<FeedsDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(),
      bottomNavigationBar: MyBottomNavBar(),
      drawer: MyNavDrawer(),
      body: Container(
        margin: EdgeInsets.all(10.0),
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.only(
                top: 10.0,
                bottom: 10.0,
              ),
              height: 180.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                image: DecorationImage(
                  image: NetworkImage(widget.feedsImage),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    widget.feedsTitle,
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  Text(
                    widget.feedsTimeStamp,
                    style: TextStyle(
                      color: Colors.grey,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
            ),
            widget.feedsDesc != null
                ? Text(
                    widget.feedsDesc,
                    style: Theme.of(context).textTheme.bodyText1,
                    textAlign: TextAlign.justify,
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
