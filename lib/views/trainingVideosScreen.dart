import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:another_flushbar/flushbar.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import '../helpers/appFunctions.dart';
import '../services/connections.dart' as Constants;

import '../widgets/myAppBar.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myNavBar.dart';
import '../widgets/subscriptionLockWidget.dart';
import '../widgets/noInternetWidget.dart';
import '../widgets/showCustomDialog.dart';

import './categoryWiseVideosScreen.dart';

class TrainingVideosScreen extends StatefulWidget {
  @override
  _TrainingVideosScreenState createState() => _TrainingVideosScreenState();
}

class _TrainingVideosScreenState extends State<TrainingVideosScreen> {
  List videoCategories = [];
  var apiRequestStatus;
  var apiErrMessage;
  var connectionChk;
  bool isPageLoading = false;
  var providerKey;

  getVideoCategories() async {
    setState(() {
      isPageLoading = true;
    });
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      //multipart/form-data POST Method
      Map<String, String> requestBody = <String, String>{
        "ProviderKey": providerKey,
      };
      // Map<String, String> headers = <String, String>{
      //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
      // };
      var uri = Uri.parse(Constants.getVideoCategories);
      var request = http.MultipartRequest('POST', uri)
        // ..headers.addAll(
        //     headers) //if u have headers, basic auth, token bearer... Else remove line
        ..fields.addAll(requestBody);
      var response = await request.send();
      final respStr = await response.stream.bytesToString();
      //

      print("response.request:${response.request}");
      if (response.statusCode == 200) {
        var data = json.decode(respStr);
        // print("data: $data");
        if (data != null) {
          apiRequestStatus = data['Status'];
          print("apiRequestStatus: $apiRequestStatus");
          if (apiRequestStatus == "Success") {
            // print("Yes");
            setState(() {
              videoCategories = data['ParentCategoryList'];
            });
            if (videoCategories.isEmpty) {
              apiErrMessage = "No Video Categories Found.";
              Flushbar(
                message: apiErrMessage,
                duration: Duration(seconds: 2),
                backgroundColor: Colors.red,
              ).show(context);
            }
          } else {
            apiErrMessage = data['Errors'][0];
            Flushbar(
              message: apiErrMessage,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          apiRequestStatus = false;
          apiErrMessage = "No data found.";
          Flushbar(
            message: apiErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        apiRequestStatus = false;
        apiErrMessage = "Error: ${response.statusCode}.";
        Flushbar(
          message: apiErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      apiRequestStatus = false;
      apiErrMessage = connectionChk;
      Flushbar(
        message: apiErrMessage,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
  }

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<String> proKey =
        preferences.getString('ProviderKey', defaultValue: "");
    proKey.listen((value) {
      setState(() {
        providerKey = value;
      });
    });
    // print("isDarkMode: $isDarkMode");
  }

  @override
  void initState() {
    super.initState();
    getSharedDataPref();
    getVideoCategories();
  }

  Widget _buildItemsList() {
    Widget itemCards;

    // var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    // final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
    // final double itemWidth = size.width / 2;

    if (videoCategories.length > 0) {
      // itemCards = GridView.count(
      //   shrinkWrap: true,
      //   physics: NeverScrollableScrollPhysics(),
      //   scrollDirection: Axis.vertical,
      //   crossAxisCount: 2,
      //   childAspectRatio: 1,
      //   padding: const EdgeInsets.all(6.0),
      //   crossAxisSpacing: 3.0,
      //   children: List.generate(videoCategories.length, (index) {
      //     return Stack(
      //       children: [
      //         Positioned.fill(
      //           bottom: 0.0,
      //           child: Container(
      // child: Column(
      //               children: [
      //                 Card(
      //                   elevation: 5.0,
      //                   shape: RoundedRectangleBorder(
      //                     borderRadius: BorderRadius.circular(10.0),
      //                   ),
      //                   margin: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 10.0),
      //                   child: Container(
      //                     height: 120.0,
      //                     width: 120.0,
      //                     // margin: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 10.0),
      //                     decoration: BoxDecoration(
      //                       borderRadius: BorderRadius.circular(10.0),
      //                       image: DecorationImage(
      //                         // image: AssetImage(videoCategories[index]['categoryImage']),
      //                         image: NetworkImage(
      //                             videoCategories[index]['CategoryImageUrl']),
      //                         fit: BoxFit.cover,
      //                       ),
      //                     ),
      //                   ),
      //                 ),
      //                 Container(
      //                   // decoration: BoxDecoration(
      //                   //   color: Colors.blue[100],
      //                   //   backgroundBlendMode: BlendMode.color,
      //                   //   borderRadius: BorderRadius.circular(10.0),
      //                   // ),
      //                   child: Center(
      //                     child: Text(
      //                       videoCategories[index]['CategoryName'],
      //                       style: Theme.of(context).textTheme.bodyText1,
      //                       textAlign: TextAlign.center,
      //                       overflow: TextOverflow.ellipsis,
      //                     ),
      //                   ),
      //                 ),
      //               ],
      //             ),
      //           ),
      //         ),
      //         Positioned.fill(
      //           child: Material(
      //             color: Colors.transparent,
      //             child: InkWell(
      //               borderRadius: BorderRadius.circular(10),
      //               highlightColor: Colors.transparent,
      //               splashColor: Theme.of(context).highlightColor,
      //               onTap: () {
      //                 Navigator.of(context).push(
      //                   MaterialPageRoute(
      //                     builder: (context) => CategoryWiseVideosScreen(
      //                       videoCategoryId: videoCategories[index]
      //                           ['CategoryID'],
      //                       videoCategoryName: videoCategories[index]
      //                           ['CategoryName'],
      //                     ),
      //                   ),
      //                 );
      //               },
      //             ),
      //           ),
      //         ),
      //       ],
      //     );
      //   }),
      // );
      itemCards = Container(
        child: ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemCount: videoCategories.length,
          itemBuilder: (BuildContext context, index) {
            return InkWell(
              borderRadius: BorderRadius.circular(15),
              highlightColor: Colors.transparent,
              splashColor: Theme.of(context).highlightColor,
              onTap: videoCategories[index]['IsAllowedInSubscription'] == false
                  ? () {
                      Flushbar(
                        message: "Please Subscribe to avail this Option.!!",
                        duration: Duration(seconds: 2),
                        backgroundColor: Colors.red,
                      ).show(context);
                    }
                  : () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => CategoryWiseVideosScreen(
                            videoCategoryId: videoCategories[index]
                                ['CategoryID'],
                            videoCategoryName: videoCategories[index]
                                ['CategoryName'],
                          ),
                        ),
                      );
                    },
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(bottom: 10.0),
                    decoration: BoxDecoration(
                      // color:
                      //     videoCategories[index]['IsAllowedInSubscription'] == false
                      //         ? Colors.grey[400]
                      //         : null,
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Card(
                          elevation: 5.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: Container(
                            height: 150.0,
                            width: MediaQuery.of(context).size.width,
                            // margin: EdgeInsets.all(5.0),
                            // padding: EdgeInsets.all(15.0),
                            decoration: BoxDecoration(
                              // color: Colors.white,
                              borderRadius: BorderRadius.circular(15.0),
                              image: DecorationImage(
                                // image: AssetImage("assets/images/7.jpg"),
                                image: NetworkImage(
                                    videoCategories[index]['CategoryImageUrl']),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 5.0),
                          child: Center(
                            child: Text(
                              videoCategories[index]['CategoryName'],
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  videoCategories[index]['IsAllowedInSubscription'] == false
                      ? Positioned.fill(
                          child: Align(
                            alignment: Alignment.topRight,
                            child: SubscriptionLockWidget(),
                          ),
                        )
                      : Container(),
                ],
              ),
            );
          },
        ),
      );
    } else {
      itemCards = Container(
        child: Center(
          child: Text(
            'No Videos.',
            style: TextStyle(
              color: Colors.grey,
            ),
          ),
        ),
      );
    }
    return itemCards;
  }

  Future<bool> _onWillPop() async {
    String titleText = "Confirm to Exit Legalhunter.?";
    String bottomBtnText = "Yes";
    return (await showDialog(
          context: context,
          builder: (BuildContext context) => ShowCustomDialog(
            title: titleText,
            buttonText: bottomBtnText,
          ),
        )) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: MyAppBar(),
        bottomNavigationBar: MyBottomNavBar(),
        drawer: MyNavDrawer(),
        body: isPageLoading
            ? Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              )
            : connectionChk != null
                ? Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        NoInternetWidget(),
                        ElevatedButton(
                          onPressed: () {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      super.widget),
                            );
                          },
                          child: Text("Retry"),
                        ),
                      ],
                    ),
                  )
                : Container(
                    margin: EdgeInsets.all(10.0),
                    child: ListView(
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            left: 10.0,
                          ),
                          child: Text(
                            "Video Classes",
                            style: Theme.of(context).textTheme.headline1,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: 10.0,
                            bottom: 10.0,
                          ),
                          child: _buildItemsList(),
                        ),
                      ],
                    ),
                  ),
      ),
    );
  }
}
