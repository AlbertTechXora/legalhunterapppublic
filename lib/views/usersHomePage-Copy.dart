// import 'package:flutter/material.dart';

// import '../widgets/myAppBar.dart';
// import '../widgets/myBottomNavBar.dart';
// import '../widgets/myNavBar.dart';

// import './quizInstructionScreen.dart';
// import './tryAnotherExamScreen.dart';
// import './feedsOrNotificationsScreen.dart';
// import './trainingVideosScreen.dart';

// class UsersHomePage extends StatefulWidget {
//   @override
//   _UsersHomePageState createState() => _UsersHomePageState();
// }

// class _UsersHomePageState extends State<UsersHomePage> {
//   List<Map<String, String>> latestFeeds = [
//     {
//       "image": "assets/images/4.jpg",
//     },
//     {
//       "image": "assets/images/5.jpg",
//     },
//     {
//       "image": "assets/images/6.jpg",
//     },
//     {
//       "image": "assets/images/7.jpg",
//     },
//   ];

//   List<Map<String, String>> activities = [
//     {
//       "tileText": "Subscription\nPlans",
//       "image": "assets/images/4.jpg",
//       "page": "",
//     },
//     {
//       "tileText": "Training\nVideos",
//       "image": "assets/images/5.jpg",
//       "page": "Training Videos",
//     },
//     {
//       "tileText": "All\nFeeds",
//       "image": "assets/images/6.jpg",
//       "page": "",
//     },
//     {
//       "tileText": "Offers\nList",
//       "image": "assets/images/7.jpg",
//       "page": "",
//     },
//   ];

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: MyAppBar(),
//       bottomNavigationBar: MyBottomNavBar(),
//       drawer: MyNavDrawer(),
//       body: Container(
//         margin: EdgeInsets.all(10.0),
//         child: ListView(
//           children: [
//             GestureDetector(
//               onTap: () {
//                 Navigator.of(context).push(
//                   MaterialPageRoute(
//                     builder: (context) => FeedsOrNotificationsScreen(
//                       pageTitle: "Feeds",
//                     ),
//                   ),
//                 );
//               },
//               child: Text(
//                 "Latest Feeds",
//                 style: Theme.of(context).textTheme.headline6,
//               ),
//             ),
//             Container(
//               width: MediaQuery.of(context).size.width,
//               height: 110.0,
//               child: ListView.builder(
//                 scrollDirection: Axis.horizontal,
//                 itemCount: latestFeeds.length,
//                 itemBuilder: (BuildContext context, index) {
//                   return GestureDetector(
//                     onTap: () {},
//                     child: Container(
//                       margin: EdgeInsets.all(3.0),
//                       padding: EdgeInsets.all(3.0),
//                       decoration: BoxDecoration(
//                         color: Colors.white,
//                         shape: BoxShape.circle,
//                         border: Border.all(
//                           color: Theme.of(context).primaryColor,
//                           width: 2,
//                         ),
//                       ),
//                       child: Container(
//                         height: 50.0,
//                         width: 50.0,
//                         decoration: BoxDecoration(
//                           shape: BoxShape.circle,
//                           image: DecorationImage(
//                             image: AssetImage(latestFeeds[index]['image']),
//                             fit: BoxFit.fill,
//                           ),
//                         ),
//                       ),
//                     ),
//                   );
//                 },
//               ),
//             ),
//             Text(
//               "Daily Exams",
//               style: Theme.of(context).textTheme.headline6,
//             ),
//             Row(
//               // mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: [
//                 Expanded(
//                   child: GestureDetector(
//                     onTap: () {
//                       Navigator.of(context).push(
//                         MaterialPageRoute(
//                           builder: (context) => TryAnotherExamScreen(),
//                         ),
//                       );
//                     },
//                     child: Card(
//                       shape: RoundedRectangleBorder(
//                         borderRadius: BorderRadius.circular(15),
//                       ),
//                       elevation: 5.0,
//                       child: Container(
//                         decoration: BoxDecoration(
//                           color: Theme.of(context).primaryColor,
//                           borderRadius: BorderRadius.circular(15),
//                         ),
//                         child: Center(
//                           child: Row(
//                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                             children: [
//                               Container(
//                                 // width: MediaQuery.of(context).size.width * 0.25,
//                                 padding: EdgeInsets.all(8.0),
//                                 child: Column(
//                                   crossAxisAlignment: CrossAxisAlignment.center,
//                                   mainAxisAlignment: MainAxisAlignment.center,
//                                   children: [
//                                     Image.asset(
//                                       "assets/icons/4.png",
//                                       height: 50.0,
//                                       width: 50.0,
//                                       color:
//                                           Theme.of(context).primaryColorLight,
//                                     ),
//                                     Text(
//                                       "Category 1",
//                                       style:
//                                           Theme.of(context).textTheme.headline4,
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                               Container(
//                                 width: 60.0,
//                                 height: 120.0,
//                                 decoration: BoxDecoration(
//                                   color: Colors.white,
//                                   borderRadius: BorderRadius.only(
//                                     bottomLeft: Radius.circular(40.0),
//                                     topLeft: Radius.circular(40.0),
//                                     bottomRight: Radius.circular(15.0),
//                                     topRight: Radius.circular(15.0),
//                                   ),
//                                 ),
//                                 child: Center(
//                                   child: Text(
//                                     "25\nExams",
//                                     style:
//                                         Theme.of(context).textTheme.subtitle2,
//                                     textAlign: TextAlign.center,
//                                   ),
//                                 ),
//                               ),
//                             ],
//                           ),
//                         ),
//                       ),
//                     ),
//                   ),
//                 ),
//                 Expanded(
//                   child: GestureDetector(
//                     onTap: () {
//                       Navigator.of(context).push(
//                         MaterialPageRoute(
//                           builder: (context) => TryAnotherExamScreen(),
//                         ),
//                       );
//                     },
//                     child: Card(
//                       shape: RoundedRectangleBorder(
//                         borderRadius: BorderRadius.circular(15),
//                       ),
//                       elevation: 5.0,
//                       child: Container(
//                         decoration: BoxDecoration(
//                           color: Colors.white,
//                           borderRadius: BorderRadius.circular(15),
//                         ),
//                         child: Center(
//                           child: Row(
//                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                             children: [
//                               Container(
//                                 // width: MediaQuery.of(context).size.width * 0.25,
//                                 padding: EdgeInsets.all(8.0),
//                                 child: Column(
//                                   crossAxisAlignment: CrossAxisAlignment.center,
//                                   mainAxisAlignment: MainAxisAlignment.center,
//                                   children: [
//                                     Image.asset(
//                                       "assets/icons/4.png",
//                                       height: 50.0,
//                                       width: 50.0,
//                                       color:
//                                           Theme.of(context).primaryColorLight,
//                                     ),
//                                     Text(
//                                       "Category 2",
//                                       style:
//                                           Theme.of(context).textTheme.subtitle2,
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                               Container(
//                                 width: 60.0,
//                                 height: 120.0,
//                                 decoration: BoxDecoration(
//                                   color: Theme.of(context).primaryColor,
//                                   borderRadius: BorderRadius.only(
//                                     bottomLeft: Radius.circular(40.0),
//                                     topLeft: Radius.circular(40.0),
//                                     bottomRight: Radius.circular(15.0),
//                                     topRight: Radius.circular(15.0),
//                                   ),
//                                 ),
//                                 child: Center(
//                                   child: Text(
//                                     "5\nExams",
//                                     style:
//                                         Theme.of(context).textTheme.bodyText2,
//                                     textAlign: TextAlign.center,
//                                   ),
//                                 ),
//                               ),
//                             ],
//                           ),
//                         ),
//                       ),
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//             SizedBox(
//               height: 5.0,
//             ),
//             Row(
//               // mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: [
//                 Expanded(
//                   child: GestureDetector(
//                     onTap: () {
//                       Navigator.of(context).push(
//                         MaterialPageRoute(
//                           builder: (context) => QuizInstructionSreen(
//                             examId: 1,
//                           ),
//                         ),
//                       );
//                     },
//                     child: Card(
//                       shape: RoundedRectangleBorder(
//                         borderRadius: BorderRadius.circular(15),
//                       ),
//                       elevation: 5.0,
//                       child: Container(
//                         decoration: BoxDecoration(
//                           color: Colors.white,
//                           borderRadius: BorderRadius.circular(15),
//                         ),
//                         child: Center(
//                           child: Row(
//                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                             children: [
//                               Container(
//                                 // width: MediaQuery.of(context).size.width * 0.25,
//                                 padding: EdgeInsets.all(8.0),
//                                 child: Column(
//                                   crossAxisAlignment: CrossAxisAlignment.center,
//                                   mainAxisAlignment: MainAxisAlignment.center,
//                                   children: [
//                                     Image.asset(
//                                       "assets/icons/4.png",
//                                       height: 50.0,
//                                       width: 50.0,
//                                       color:
//                                           Theme.of(context).primaryColorLight,
//                                     ),
//                                     Text(
//                                       "Daily Quiz",
//                                       style:
//                                           Theme.of(context).textTheme.subtitle2,
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                               Container(
//                                 width: 60.0,
//                                 height: 120.0,
//                                 decoration: BoxDecoration(
//                                   color: Theme.of(context).primaryColor,
//                                   borderRadius: BorderRadius.only(
//                                     bottomLeft: Radius.circular(40.0),
//                                     topLeft: Radius.circular(40.0),
//                                     bottomRight: Radius.circular(15.0),
//                                     topRight: Radius.circular(15.0),
//                                   ),
//                                 ),
//                                 child: Center(
//                                   child: Text(
//                                     "16 h\n20 m",
//                                     style:
//                                         Theme.of(context).textTheme.bodyText2,
//                                     textAlign: TextAlign.center,
//                                   ),
//                                 ),
//                               ),
//                             ],
//                           ),
//                         ),
//                       ),
//                     ),
//                   ),
//                 ),
//                 Expanded(
//                   child: GestureDetector(
//                     onTap: () {
//                       Navigator.of(context).push(
//                         MaterialPageRoute(
//                           builder: (context) => TryAnotherExamScreen(),
//                         ),
//                       );
//                     },
//                     child: Card(
//                       shape: RoundedRectangleBorder(
//                         borderRadius: BorderRadius.circular(15),
//                       ),
//                       elevation: 5.0,
//                       child: Container(
//                         decoration: BoxDecoration(
//                           color: Theme.of(context).primaryColor,
//                           borderRadius: BorderRadius.circular(15),
//                         ),
//                         child: Center(
//                           child: Row(
//                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                             children: [
//                               Container(
//                                 // width: MediaQuery.of(context).size.width * 0.25,
//                                 padding: EdgeInsets.all(8.0),
//                                 child: Column(
//                                   crossAxisAlignment: CrossAxisAlignment.center,
//                                   mainAxisAlignment: MainAxisAlignment.center,
//                                   children: [
//                                     Image.asset(
//                                       "assets/icons/4.png",
//                                       height: 50.0,
//                                       width: 50.0,
//                                       color:
//                                           Theme.of(context).primaryColorLight,
//                                     ),
//                                     Text(
//                                       "Category 4",
//                                       style:
//                                           Theme.of(context).textTheme.headline4,
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                               Container(
//                                 width: 60.0,
//                                 height: 120.0,
//                                 decoration: BoxDecoration(
//                                   color: Colors.white,
//                                   borderRadius: BorderRadius.only(
//                                     bottomLeft: Radius.circular(40.0),
//                                     topLeft: Radius.circular(40.0),
//                                     bottomRight: Radius.circular(15.0),
//                                     topRight: Radius.circular(15.0),
//                                   ),
//                                 ),
//                                 child: Center(
//                                   child: Text(
//                                     "45\nExams",
//                                     style:
//                                         Theme.of(context).textTheme.subtitle2,
//                                     textAlign: TextAlign.center,
//                                   ),
//                                 ),
//                               ),
//                             ],
//                           ),
//                         ),
//                       ),
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//             SizedBox(
//               height: 5.0,
//             ),
//             Card(
//               shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.circular(15),
//               ),
//               elevation: 5.0,
//               child: Container(
//                 height: 115.0,
//                 child: Center(
//                   child: Row(
//                     children: [
//                       Container(
//                         margin: EdgeInsets.only(left: 5.0),
//                         child: Image.asset(
//                           "assets/images/nextQuiz.png",
//                           height: 75.0,
//                           width: 75.0,
//                         ),
//                       ),
//                       Container(
//                         margin: EdgeInsets.only(left: 5.0),
//                         child: Column(
//                           mainAxisAlignment: MainAxisAlignment.center,
//                           crossAxisAlignment: CrossAxisAlignment.center,
//                           children: [
//                             Text(
//                               "The next quiz will start after",
//                               style: Theme.of(context).textTheme.headline3,
//                             ),
//                             Container(
//                               width: 120,
//                               height: 35,
//                               decoration: BoxDecoration(
//                                 color: Theme.of(context).primaryColor,
//                                 borderRadius: BorderRadius.circular(10.0),
//                               ),
//                               child: Center(
//                                 child: Text(
//                                   "16H 20M",
//                                   style: Theme.of(context).textTheme.subtitle1,
//                                 ),
//                               ),
//                             ),
//                           ],
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             ),
//             SizedBox(
//               height: 5.0,
//             ),
//             Text(
//               "Activities",
//               style: Theme.of(context).textTheme.headline6,
//             ),
//             Container(
//               width: MediaQuery.of(context).size.width,
//               height: 130.0,
//               child: ListView.builder(
//                 scrollDirection: Axis.horizontal,
//                 itemCount: activities.length,
//                 itemBuilder: (BuildContext context, index) {
//                   return GestureDetector(
//                     onTap: activities[index]['page'] == "Training Videos"
//                         ? () {
//                             Navigator.of(context).push(
//                               MaterialPageRoute(
//                                 builder: (context) => TrainingVideosScreen(),
//                               ),
//                             );
//                           }
//                         : () {},
//                     child: Container(
//                       margin: EdgeInsets.all(3.0),
//                       width: 130.0,
//                       decoration: BoxDecoration(
//                         borderRadius: BorderRadius.circular(15.0),
//                         image: DecorationImage(
//                           image: AssetImage(activities[index]['image']),
//                           fit: BoxFit.fill,
//                         ),
//                       ),
//                       child: Container(
//                         padding: EdgeInsets.all(5.0),
//                         decoration: BoxDecoration(
//                           color: Colors.blue[100],
//                           backgroundBlendMode: BlendMode.color,
//                           borderRadius: BorderRadius.circular(10.0),
//                         ),
//                         child: Center(
//                           child: Row(
//                             children: [
//                               Text(
//                                 activities[index]['tileText'],
//                                 style: Theme.of(context).textTheme.subtitle1,
//                               ),
//                             ],
//                           ),
//                         ),
//                       ),
//                     ),
//                   );
//                 },
//               ),
//             ),
//             SizedBox(
//               height: 15.0,
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
