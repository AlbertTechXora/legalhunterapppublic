import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:another_flushbar/flushbar.dart';

import '../helpers/appFunctions.dart';
import '../services/connections.dart' as Constants;

import './resetPassword.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  bool isPageLoading = false;
  var resetRequestStatus;
  var resetErrMessage;
  var connectionChk;

  TextEditingController emailController = new TextEditingController();

  forgotPasswordAndGetOtp() async {
    setState(() {
      isPageLoading = true;
    });
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      var email = emailController.text;
      if (email != "") {
        //multipart/form-data POST Method
        Map<String, String> requestBody = <String, String>{
          "Email": email,
        };
        // Map<String, String> headers = <String, String>{
        //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
        // };
        var uri = Uri.parse(Constants.forgotPassword);
        var request = http.MultipartRequest('POST', uri)
          // ..headers.addAll(
          //     headers) //if u have headers, basic auth, token bearer... Else remove line
          ..fields.addAll(requestBody);
        var response = await request.send();
        final respStr = await response.stream.bytesToString();
        //

        print("response.request:${response.request}");
        if (response.statusCode == 200) {
          // print("Yes");
          var data = json.decode(respStr);
          print("data: $data");
          if (data != null) {
            resetRequestStatus = data['Status'];
            print("resetRequestStatus: $resetRequestStatus");
            if (resetRequestStatus == "Success") {
              Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                  builder: (context) => ResetPassword(
                    userEmail: email,
                  ),
                ),
              );
            } else {
              // resetErrMessage = data['Errors'][0];
              resetErrMessage = "Oops..Err Occured.";
              Flushbar(
                message: resetErrMessage,
                duration: Duration(seconds: 2),
                backgroundColor: Colors.red,
              ).show(context);
            }
          } else {
            resetRequestStatus = false;
            resetErrMessage = "No data found.";
            Flushbar(
              message: resetErrMessage,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          resetRequestStatus = false;
          resetErrMessage = "Error: ${response.statusCode}.";
          Flushbar(
            message: resetErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        resetRequestStatus = false;
        resetErrMessage = "Please enter your email.";
        Flushbar(
          message: resetErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      resetRequestStatus = false;
      resetErrMessage = connectionChk;
      Flushbar(
        message: resetErrMessage,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: isPageLoading
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Forgot Password?",
                    style: Theme.of(context).textTheme.headline5,
                  ),
                  Card(
                    margin: EdgeInsets.fromLTRB(25.0, 20.0, 25.0, 20.0),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25),
                    ),
                    elevation: 5.0,
                    child: TextField(
                      controller: emailController,
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.normal,
                      ),
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        hintText: 'Email',
                        hintStyle: TextStyle(
                          fontSize: 14.0,
                          color: Colors.grey,
                          fontWeight: FontWeight.normal,
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25),
                          borderSide: BorderSide(
                            width: 0,
                            style: BorderStyle.none,
                          ),
                        ),
                        filled: true,
                        contentPadding: EdgeInsets.all(16),
                        fillColor: Colors.white70,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 15.0),
                    child: Text(
                      "We'll sent you an OTP to your email.",
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      forgotPasswordAndGetOtp();
                    },
                    child: Text(
                      "Get OTP",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}
