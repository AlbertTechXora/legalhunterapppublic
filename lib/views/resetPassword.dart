import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:another_flushbar/flushbar.dart';

import '../helpers/appFunctions.dart';
import '../services/connections.dart' as Constants;

import './loginScreen.dart';

class ResetPassword extends StatefulWidget {
  final String userEmail;
  ResetPassword({@required this.userEmail});
  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  bool isPageLoading = false;
  var resetRequestStatus;
  var resetErrMessage;
  var connectionChk;

  TextEditingController emailController = new TextEditingController();
  TextEditingController otpController = new TextEditingController();
  TextEditingController newPasswordController = new TextEditingController();
  TextEditingController confirmNewPasswordController =
      new TextEditingController();

  resetForgotPassword() async {
    setState(() {
      isPageLoading = true;
    });
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      var email = emailController.text;
      var otp = otpController.text;
      var password = newPasswordController.text;
      var confirmPassword = confirmNewPasswordController.text;
      if (email != "" && otp != "" && password != "" && confirmPassword != "") {
        //multipart/form-data POST Method
        Map<String, String> requestBody = <String, String>{
          "Email": email,
          "OTP": otp,
          "Password": password,
          "ConfirmPassword": confirmPassword,
        };
        print("requestBody:$requestBody");
        // Map<String, String> headers = <String, String>{
        //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
        // };
        var uri = Uri.parse(Constants.resetForgotPassword);
        var request = http.MultipartRequest('POST', uri)
          // ..headers.addAll(
          //     headers) //if u have headers, basic auth, token bearer... Else remove line
          ..fields.addAll(requestBody);
        var response = await request.send();
        final respStr = await response.stream.bytesToString();
        //

        print("response.request:${response.request}");
        if (response.statusCode == 200) {
          // print("Yes");
          var data = json.decode(respStr);
          print("data: $data");
          if (data != null) {
            resetRequestStatus = data['Status'];
            print("resetRequestStatus: $resetRequestStatus");
            if (resetRequestStatus == "Success") {
              Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                  builder: (context) => LoginScreen(),
                ),
              );
            } else {
              // resetErrMessage = data['Errors'][0];
              resetErrMessage = "Oops..Err Occured.";
              Flushbar(
                message: resetErrMessage,
                duration: Duration(seconds: 2),
                backgroundColor: Colors.red,
              ).show(context);
            }
          } else {
            resetRequestStatus = false;
            resetErrMessage = "No data found.";
            Flushbar(
              message: resetErrMessage,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          resetRequestStatus = false;
          resetErrMessage = "Error: ${response.statusCode}.";
          Flushbar(
            message: resetErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        resetRequestStatus = false;
        resetErrMessage = "All fields are required.";
        Flushbar(
          message: resetErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      resetRequestStatus = false;
      resetErrMessage = connectionChk;
      Flushbar(
        message: resetErrMessage,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    emailController.text = widget.userEmail;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: isPageLoading
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : SafeArea(
              child: Center(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Reset Your Password",
                        style: Theme.of(context).textTheme.headline5,
                      ),
                      Card(
                        margin: EdgeInsets.fromLTRB(25.0, 15.0, 25.0, 0.0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25),
                        ),
                        elevation: 5.0,
                        child: TextField(
                          enabled: false,
                          controller: emailController,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                          ),
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: 'Email',
                            hintStyle: TextStyle(
                              fontSize: 14.0,
                              color: Colors.grey,
                              fontWeight: FontWeight.normal,
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25),
                              borderSide: BorderSide(
                                width: 0,
                                style: BorderStyle.none,
                              ),
                            ),
                            filled: true,
                            contentPadding: EdgeInsets.all(16),
                            fillColor: Colors.white70,
                          ),
                        ),
                      ),
                      Card(
                        margin: EdgeInsets.fromLTRB(25.0, 15.0, 25.0, 0.0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25),
                        ),
                        elevation: 5.0,
                        child: TextField(
                          controller: otpController,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                          ),
                          obscureText: true,
                          obscuringCharacter: "*",
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: 'Enter your OTP',
                            hintStyle: TextStyle(
                              fontSize: 14.0,
                              color: Colors.grey,
                              fontWeight: FontWeight.normal,
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25),
                              borderSide: BorderSide(
                                width: 0,
                                style: BorderStyle.none,
                              ),
                            ),
                            filled: true,
                            contentPadding: EdgeInsets.all(16),
                            fillColor: Colors.white70,
                          ),
                        ),
                      ),
                      Card(
                        margin: EdgeInsets.fromLTRB(25.0, 15.0, 25.0, 0.0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25),
                        ),
                        elevation: 5.0,
                        child: TextField(
                          controller: newPasswordController,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                          ),
                          obscureText: true,
                          obscuringCharacter: "*",
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: 'New Password',
                            hintStyle: TextStyle(
                              fontSize: 14.0,
                              color: Colors.grey,
                              fontWeight: FontWeight.normal,
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25),
                              borderSide: BorderSide(
                                width: 0,
                                style: BorderStyle.none,
                              ),
                            ),
                            filled: true,
                            contentPadding: EdgeInsets.all(16),
                            fillColor: Colors.white70,
                          ),
                        ),
                      ),
                      Card(
                        margin: EdgeInsets.fromLTRB(25.0, 15.0, 25.0, 20.0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25),
                        ),
                        elevation: 5.0,
                        child: TextField(
                          controller: confirmNewPasswordController,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                          ),
                          obscureText: true,
                          obscuringCharacter: "*",
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: 'Confirm New Password',
                            hintStyle: TextStyle(
                              fontSize: 14.0,
                              color: Colors.grey,
                              fontWeight: FontWeight.normal,
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25),
                              borderSide: BorderSide(
                                width: 0,
                                style: BorderStyle.none,
                              ),
                            ),
                            filled: true,
                            contentPadding: EdgeInsets.all(16),
                            fillColor: Colors.white70,
                          ),
                        ),
                      ),
                      ElevatedButton(
                        onPressed: () {
                          resetForgotPassword();
                        },
                        child: Text(
                          "Reset Password",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}
