import 'package:flutter/material.dart';

import '../widgets/myAppBar.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myNavBar.dart';

class RanklistScreen extends StatefulWidget {
  final List rankList;
  RanklistScreen({this.rankList});
  @override
  _RanklistScreenState createState() => _RanklistScreenState();
}

class _RanklistScreenState extends State<RanklistScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(),
      bottomNavigationBar: MyBottomNavBar(),
      drawer: MyNavDrawer(),
      body: Container(
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.only(top: 25.0, left: 15.0, right: 15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 150.0,
                    child: Text(
                      "Username",
                      style: Theme.of(context).textTheme.headline6,
                    ),
                  ),
                  Text(
                    "Score",
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  Text(
                    "Rank",
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ],
              ),
            ),
            // Container(
            //   margin: EdgeInsets.only(top: 10.0),
            //   color: Theme.of(context).primaryColorLight,
            //   child: Column(
            //     children: [
            //       Container(
            //         margin: EdgeInsets.all(8.0),
            //         padding: EdgeInsets.only(
            //           left: 8.0,
            //           top: 8.0,
            //           bottom: 8.0,
            //           right: 20.0,
            //         ),
            //         child: Row(
            //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           children: [
            //             Text(
            //               "Your Name",
            //               style: Theme.of(context).textTheme.subtitle1,
            //             ),
            //             Text(
            //               "76/100",
            //               style: Theme.of(context).textTheme.subtitle1,
            //             ),
            //             Text(
            //               "26",
            //               style: Theme.of(context).textTheme.subtitle1,
            //             ),
            //           ],
            //         ),
            //       ),
            //     ],
            //   ),
            // ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: widget.rankList.length,
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (BuildContext context, index) {
                  return Container(
                    margin: EdgeInsets.all(5.0),
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.only(
                            left: 8.0,
                            top: 8.0,
                            bottom: 8.0,
                            right: 20.0,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                width: 145.0,
                                child: Text(
                                  widget.rankList[index]['FullName'],
                                  style: Theme.of(context).textTheme.headline3,
                                ),
                              ),
                              Text(
                                "${widget.rankList[index]['MarksScored']}",
                                style: Theme.of(context).textTheme.headline3,
                              ),
                              Text(
                                widget.rankList[index]['ExamRank'] < 10
                                    ? "0${widget.rankList[index]['ExamRank']}"
                                    : "${widget.rankList[index]['ExamRank']}",
                                style: Theme.of(context).textTheme.headline3,
                              ),
                            ],
                          ),
                        ),
                        Divider(
                          thickness: 1,
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
