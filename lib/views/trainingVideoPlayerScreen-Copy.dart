// import 'package:flutter/material.dart';
// import 'package:video_player/video_player.dart';
// import 'package:chewie/chewie.dart';

// import '../widgets/myAppBar.dart';
// // import '../widgets/myBottomNavBar.dart';
// import '../widgets/myNavBar.dart';

// class TrainingVideoPlayerScreen extends StatefulWidget {
//   final String videoLink;
//   TrainingVideoPlayerScreen({@required this.videoLink});
//   @override
//   _TrainingVideoPlayerScreenState createState() =>
//       _TrainingVideoPlayerScreenState();
// }

// class _TrainingVideoPlayerScreenState extends State<TrainingVideoPlayerScreen> {
//   // VideoPlayerController _videoPlayerController1;
//   VideoPlayerController _videoPlayerController2;
//   ChewieController _chewieController;

//   @override
//   void initState() {
//     super.initState();
//     initializePlayer();
//   }

//   @override
//   void dispose() {
//     // _videoPlayerController1.dispose();
//     _videoPlayerController2.dispose();
//     _videoPlayerController2.pause();
//     _chewieController.dispose();
//     _chewieController.pause();
//     super.dispose();
//   }

//   Future<void> initializePlayer() async {
//     // _videoPlayerController1 = VideoPlayerController.network(
//     //     'https://assets.mixkit.co/videos/preview/mixkit-forest-stream-in-the-sunlight-529-large.mp4');
//     // _videoPlayerController2 = VideoPlayerController.network(
//     //     'https://www.radiantmediaplayer.com/media/big-buck-bunny-360p.mp4');
//     // _videoPlayerController2 = VideoPlayerController.network(widget.videoLink);

//     await Future.wait([
//       // _videoPlayerController1.initialize(),
//       _videoPlayerController2.initialize()
//     ]);
//     _chewieController = ChewieController(
//       videoPlayerController: _videoPlayerController2,
//       autoPlay: true,
//       looping: true,
//       allowFullScreen: true,
//       // Try playing around with some of these other options:

//       // showControls: false,
//       // materialProgressColors: ChewieProgressColors(
//       //   playedColor: Colors.red,
//       //   handleColor: Colors.blue,
//       //   backgroundColor: Colors.grey,
//       //   bufferedColor: Colors.lightGreen,
//       // ),
//       // placeholder: Container(
//       //   color: Colors.grey,
//       // ),
//       // autoInitialize: true,
//     );
//     setState(() {});
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: MyAppBar(),
//       // bottomNavigationBar: MyBottomNavBar(),
//       drawer: MyNavDrawer(),
//       body: Column(
//         children: <Widget>[
//           Expanded(
//             child: Center(
//               child: _chewieController != null &&
//                       _chewieController
//                           .videoPlayerController.value.isInitialized
//                   ? Chewie(
//                       controller: _chewieController,
//                     )
//                   : Column(
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       children: const [
//                         CircularProgressIndicator(),
//                         SizedBox(height: 20),
//                         Text('Loading'),
//                       ],
//                     ),
//             ),
//           ),
//           // Row(
//           //   children: <Widget>[
//           //     Expanded(
//           //       child: TextButton(
//           //         onPressed: () {
//           //           setState(() {
//           //             _chewieController.dispose();
//           //             _videoPlayerController1.pause();
//           //             _videoPlayerController1.seekTo(const Duration());
//           //             _chewieController = ChewieController(
//           //               videoPlayerController: _videoPlayerController1,
//           //               autoPlay: true,
//           //               looping: true,
//           //             );
//           //           });
//           //         },
//           //         child: const Padding(
//           //           padding: EdgeInsets.symmetric(vertical: 16.0),
//           //           child: Text("Landscape Video"),
//           //         ),
//           //       ),
//           //     ),
//           //     Expanded(
//           //       child: TextButton(
//           //         onPressed: () {
//           //           setState(() {
//           //             _chewieController.dispose();
//           //             _videoPlayerController2.pause();
//           //             _videoPlayerController2.seekTo(const Duration());
//           //             _chewieController = ChewieController(
//           //               videoPlayerController: _videoPlayerController2,
//           //               autoPlay: true,
//           //               looping: true,
//           //             );
//           //           });
//           //         },
//           //         child: const Padding(
//           //           padding: EdgeInsets.symmetric(vertical: 16.0),
//           //           child: Text("Portrait Video"),
//           //         ),
//           //       ),
//           //     )
//           //   ],
//           // ),
//         ],
//       ),
//     );
//   }
// }
