import 'package:flutter/material.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';
// import 'package:hexcolor/hexcolor.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:another_flushbar/flushbar.dart';

import '../helpers/appFunctions.dart';
import '../services/connections.dart' as Constants;

import '../widgets/myAppBar.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myNavBar.dart';
import '../widgets/top_carousel.dart';

import './quizInstructionScreen.dart';
import './tryAnotherExamScreen.dart';
import './feedsOrNotificationsScreen.dart';
import './trainingVideosScreen.dart';
import './subscriptionScreen.dart';

class UsersHomePage extends StatefulWidget {
  @override
  _UsersHomePageState createState() => _UsersHomePageState();
}

class _UsersHomePageState extends State<UsersHomePage> {
  bool isDarkMode = false;
  var apiRequestStatus;
  var apiErrMessage;
  var connectionChk;
  bool isPageLoading = false;
  List examCategoryList = [];

  List<Map<String, String>> latestFeeds = [
    {
      "image": "assets/images/newDashImages1.png",
    },
    {
      "image": "assets/images/newDashImages2.png",
    },
    {
      "image": "assets/images/newDashImages3.png",
    },
    {
      "image": "assets/images/newDashImages4.png",
    },
  ];

  List<Map<String, String>> activities = [
    {
      "tileText": "Training Videos",
      "image": "assets/images/newDashImages10.png",
      "page": "Training Videos",
    },
    {
      "tileText": "All Feeds",
      "image": "assets/images/newDashImages9.png",
      "page": "Feeds",
    },
    {
      "tileText": "Subscription Plans",
      "image": "assets/images/A4.png",
      "page": "Subscription",
    },
  ];

  List<Map<String, String>> centreBannerList = [
    {
      "leadingImage": "assets/images/newDashImages12.png",
      "titleText": "The Next Quiz",
      "time": "18H 30M 20S",
      "trailerImage": "assets/images/newDashImages11.png",
    },
    {
      "leadingImage": "assets/images/newDashImages12.png",
      "titleText": "The Next Quiz 1",
      "time": "20H 30M 20S",
      "trailerImage": "assets/images/newDashImages11.png",
    },
    {
      "leadingImage": "assets/images/newDashImages12.png",
      "titleText": "The Next Quiz 2",
      "time": "24H 30M 20S",
      "trailerImage": "assets/images/newDashImages11.png",
    },
  ];

  getExamCategories() async {
    setState(() {
      isPageLoading = true;
    });
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      //multipart/form-data POST Method
      // Map<String, String> requestBody = <String, String>{
      //   "CategoryID": categoryId,
      // };
      // Map<String, String> headers = <String, String>{
      //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
      // };
      var uri = Uri.parse(Constants.getCategoriesList);
      var request = http.MultipartRequest('POST', uri);
      // ..headers.addAll(
      //     headers) //if u have headers, basic auth, token bearer... Else remove line
      // ..fields.addAll(requestBody);
      var response = await request.send();
      final respStr = await response.stream.bytesToString();
      //

      print("response.request:${response.request}");
      if (response.statusCode == 200) {
        var data = json.decode(respStr);
        // print("data: $data");
        if (data != null) {
          apiRequestStatus = data['Status'];
          print("apiRequestStatus: $apiRequestStatus");
          if (apiRequestStatus == "Success") {
            // print("Yes");
            setState(() {
              examCategoryList = data['ParentCategoryList'];
            });
            if (examCategoryList.isEmpty) {
              apiErrMessage = "No Exam Categories Found.";
              Flushbar(
                message: apiErrMessage,
                duration: Duration(seconds: 2),
                backgroundColor: Colors.red,
              ).show(context);
            }
          } else {
            apiErrMessage = data['Errors'][0];
            Flushbar(
              message: apiErrMessage,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          apiRequestStatus = false;
          apiErrMessage = "No data found.";
          Flushbar(
            message: apiErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        apiRequestStatus = false;
        apiErrMessage = "Error: ${response.statusCode}.";
        Flushbar(
          message: apiErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      apiRequestStatus = false;
      apiErrMessage = connectionChk;
      Flushbar(
        message: apiErrMessage,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
  }

  checkInCategoryList(examCategory) {
    if (examCategoryList.isNotEmpty) {
      for (var i = 0; i <= examCategoryList.length; i++) {
        var examCatName = examCategoryList[i]['CategoryName'];
        if (examCatName == examCategory) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => TryAnotherExamScreen(
                examCatId: examCategoryList[i]['CategoryID'],
                subCategoryList: [],
              ),
            ),
          );
          break;
        }
      }
    } else {
      Flushbar(
        message: "Sorry, Category seems Empty. Try Again Later.!!",
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
  }

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<bool> isDark =
        preferences.getBool('isDark', defaultValue: false);
    isDark.listen((value) {
      setState(() {
        isDarkMode = value;
      });
    });
    // print("isDarkMode: $isDarkMode");
  }

  @override
  void initState() {
    super.initState();
    getSharedDataPref();
    getExamCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: MyAppBar(),
      bottomNavigationBar: MyBottomNavBar(),
      drawer: MyNavDrawer(),
      body: isPageLoading
          ? Container(
              color: Colors.white,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : SingleChildScrollView(
              child: Container(
                color: Theme.of(context).primaryColor,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // Container(
                          //   margin: EdgeInsets.only(top: 10.0, left: 10.0),
                          //   child: Text(
                          //     "What would you\nlike to do\ntoday?",
                          //     style: TextStyle(
                          //       color: Colors.white,
                          //       fontSize: 18.0,
                          //       height: 1.4,
                          //       fontWeight: FontWeight.w500,
                          //     ),
                          //     textAlign: TextAlign.start,
                          //   ),
                          // ),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: 80.0,
                            margin: EdgeInsets.only(left: 15.0),
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: latestFeeds.length,
                              itemBuilder: (BuildContext context, index) {
                                return GestureDetector(
                                  onTap: () {},
                                  child: Container(
                                    margin: EdgeInsets.only(right: 15.0),
                                    padding: EdgeInsets.all(2.0),
                                    decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                        colors: [
                                          Colors.white,
                                          Colors.white,
                                          Colors.amber,
                                          Colors.amberAccent,
                                        ],
                                        begin: Alignment.topCenter,
                                        end: Alignment.bottomCenter,
                                      ),
                                      shape: BoxShape.circle,
                                    ),
                                    child: Container(
                                      height: 65.0,
                                      width: 65.0,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                          image: AssetImage(
                                              latestFeeds[index]['image']),
                                          fit: BoxFit.contain,
                                        ),
                                        border: Border.all(
                                          color: Theme.of(context).primaryColor,
                                          width: 2,
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                          Container(
                            // color: isDarkMode ? Colors.black12 : HexColor("#ECECEC"),
                            color: HexColor("#ECECEC"),
                            margin: EdgeInsets.only(top: 120.0),
                            // height: MediaQuery.of(context).size.height,
                            child: Column(
                              children: [
                                Container(
                                  margin: EdgeInsets.fromLTRB(
                                      15.0, 450.0, 15.0, 10.0),
                                  child: TopCarousel(items: centreBannerList),
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 200.0,
                                  margin:
                                      EdgeInsets.only(left: 15.0, top: 10.0),
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: activities.length,
                                    itemBuilder: (BuildContext context, index) {
                                      return GestureDetector(
                                        onTap: activities[index]['page'] ==
                                                "Training Videos"
                                            ? () {
                                                Navigator.of(context).push(
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        TrainingVideosScreen(),
                                                  ),
                                                );
                                              }
                                            : activities[index]['page'] ==
                                                    "Subscription"
                                                ? () {
                                                    Navigator.of(context).push(
                                                      MaterialPageRoute(
                                                        builder: (context) =>
                                                            SubscriptionScreen(),
                                                      ),
                                                    );
                                                  }
                                                : activities[index]['page'] ==
                                                        "Feeds"
                                                    ? () {
                                                        Navigator.of(context)
                                                            .push(
                                                          MaterialPageRoute(
                                                            builder: (context) =>
                                                                FeedsOrNotificationsScreen(
                                                              pageTitle:
                                                                  "Feeds",
                                                            ),
                                                          ),
                                                        );
                                                      }
                                                    : () {},
                                        child: Container(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Card(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          15.0),
                                                ),
                                                child: Container(
                                                  height: 135.0,
                                                  width: 200.5,
                                                  margin: EdgeInsets.all(5.0),
                                                  padding: EdgeInsets.all(15.0),
                                                  decoration: BoxDecoration(
                                                    // color: Colors.white,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15.0),
                                                    // image: DecorationImage(
                                                    //   image: AssetImage(
                                                    //       activities[index]['image']),
                                                    //   fit: BoxFit.fill,
                                                    // ),
                                                  ),
                                                  child: Image.asset(
                                                      activities[index]
                                                          ['image']),
                                                ),
                                              ),
                                              Container(
                                                width: 200.5,
                                                child: Center(
                                                  child: Text(
                                                    activities[index]
                                                        ['tileText'],
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText1,
                                                    textAlign: TextAlign.start,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Positioned.fill(
                      top: 75.0,
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Container(
                          height: 560.0,
                          width: MediaQuery.of(context).size.width,
                          // margin: EdgeInsets.all(5.0),
                          // padding: EdgeInsets.all(15.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Container(
                                // height: 580.0,
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.of(context).push(
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                QuizInstructionSreen(
                                              examId: 1,
                                            ),
                                          ),
                                        );
                                      },
                                      child: Card(
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15),
                                        ),
                                        elevation: 1.0,
                                        child: Container(
                                          height: 245.0,
                                          width: 165.0,
                                          padding: EdgeInsets.all(15.0),
                                          decoration: BoxDecoration(
                                            // color: Theme.of(context).primaryColor,
                                            borderRadius:
                                                BorderRadius.circular(15),
                                          ),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Container(
                                                padding: EdgeInsets.all(5.0),
                                                child: Image.asset(
                                                  "assets/images/newDashImages8.png",
                                                  // height: 100.0,
                                                  // width: 100.0,
                                                ),
                                              ),
                                              Text(
                                                "Daily Quiz",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .headline3,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Card(
                                                    elevation: 5.0,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              8.0),
                                                    ),
                                                    child: Container(
                                                      height: 30.0,
                                                      width: 30.0,
                                                      child: Center(
                                                        child: Text(
                                                          "18",
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .bodyText1,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Text(
                                                    ":",
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText1,
                                                  ),
                                                  Card(
                                                    elevation: 5.0,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              8.0),
                                                    ),
                                                    child: Container(
                                                      height: 30.0,
                                                      width: 30.0,
                                                      child: Center(
                                                        child: Text(
                                                          "30",
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .bodyText1,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Text(
                                                    ":",
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText1,
                                                  ),
                                                  Card(
                                                    elevation: 5.0,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              8.0),
                                                    ),
                                                    child: Container(
                                                      height: 30.0,
                                                      width: 30.0,
                                                      child: Center(
                                                        child: Text(
                                                          "20",
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .bodyText1,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    // SizedBox(
                                    //   height: 15.0,
                                    // ),
                                    GestureDetector(
                                      onTap: () {
                                        checkInCategoryList("Test Series");
                                        // Navigator.of(context).push(
                                        //   MaterialPageRoute(
                                        //     builder: (context) =>
                                        //         TryAnotherExamScreen(),
                                        //   ),
                                        // );
                                      },
                                      child: Card(
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15),
                                        ),
                                        elevation: 1.0,
                                        child: Container(
                                          height: 245.0,
                                          width: 165.0,
                                          decoration: BoxDecoration(
                                            // color: Theme.of(context).primaryColor,
                                            borderRadius:
                                                BorderRadius.circular(15),
                                          ),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Container(
                                                padding: EdgeInsets.all(5.0),
                                                child: Image.asset(
                                                  "assets/images/newDashImages5.png",
                                                  // height: 100.0,
                                                  // width: 100.0,
                                                ),
                                              ),
                                              Text(
                                                "Test Series",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .headline3,
                                              ),
                                              Text(
                                                "Total Exams : 50",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                // height: 580.0,
                                // margin: EdgeInsets.only(top: 50.0),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        checkInCategoryList(
                                            "Previous Year Question Paper");
                                        // Navigator.of(context).push(
                                        //   MaterialPageRoute(
                                        //     builder: (context) =>
                                        //         TryAnotherExamScreen(),
                                        //   ),
                                        // );
                                      },
                                      child: Card(
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15),
                                        ),
                                        elevation: 1.0,
                                        child: Container(
                                          height: 245.0,
                                          width: 165.0,
                                          decoration: BoxDecoration(
                                            // color: Theme.of(context).primaryColor,
                                            borderRadius:
                                                BorderRadius.circular(15),
                                          ),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Container(
                                                padding: EdgeInsets.all(5.0),
                                                child: Image.asset(
                                                  "assets/images/newDashImages2.png",
                                                  // height: 100.0,
                                                  // width: 100.0,
                                                ),
                                              ),
                                              Text(
                                                "Previous Year\nQuestion Paper",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .headline3,
                                                textAlign: TextAlign.center,
                                              ),
                                              Text(
                                                "Total Exams : 50",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    // SizedBox(
                                    //   height: 15.0,
                                    // ),
                                    GestureDetector(
                                      onTap: () {
                                        checkInCategoryList("Free Mocktest");
                                        // Navigator.of(context).push(
                                        //   MaterialPageRoute(
                                        //     builder: (context) =>
                                        //         TryAnotherExamScreen(),
                                        //   ),
                                        // );
                                      },
                                      child: Card(
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15),
                                        ),
                                        elevation: 1.0,
                                        child: Container(
                                          height: 245.0,
                                          width: 165.0,
                                          decoration: BoxDecoration(
                                            // color: Theme.of(context).primaryColor,
                                            borderRadius:
                                                BorderRadius.circular(15),
                                          ),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Container(
                                                padding: EdgeInsets.all(5.0),
                                                child: Image.asset(
                                                  "assets/images/newDashImages6.png",
                                                  // height: 100.0,
                                                  // width: 100.0,
                                                ),
                                              ),
                                              Text(
                                                "Free Mocktest",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .headline3,
                                              ),
                                              Text(
                                                "Total Exams : 50",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
