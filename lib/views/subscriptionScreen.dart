import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:another_flushbar/flushbar.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import '../helpers/appFunctions.dart';
import '../services/connections.dart' as Constants;

import '../widgets/myAppBar.dart';
import '../widgets/myNavBar.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/noInternetWidget.dart';

class SubscriptionScreen extends StatefulWidget {
  @override
  _SubscriptionScreenState createState() => _SubscriptionScreenState();
}

class _SubscriptionScreenState extends State<SubscriptionScreen> {
  var apiRequestStatus;
  var apiErrMessage;
  var connectionChk;
  bool isPageLoading = false;
  List subscriptionPlans = [];
  var selected;
  var selectedSubscriptionPlan;
  var providerKey;
  bool isDarkMode = false;
  var subscriptionId;

  getSubscriptionPlans() async {
    setState(() {
      isPageLoading = true;
    });
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      //multipart/form-data POST Method
      // Map<String, String> requestBody = <String, String>{
      //   "Username": username,
      //   "Password": password,
      // };
      // Map<String, String> headers = <String, String>{
      //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
      // };
      var uri = Uri.parse(Constants.getSubscriptionPlans);
      var request = http.MultipartRequest('POST', uri);
      // ..headers.addAll(
      //     headers) //if u have headers, basic auth, token bearer... Else remove line
      // ..fields.addAll(requestBody);
      var response = await request.send();
      final respStr = await response.stream.bytesToString();
      //

      print("response.request:${response.request}");
      if (response.statusCode == 200) {
        var data = json.decode(respStr);
        // print("data: $data");
        if (data != null) {
          apiRequestStatus = data['Status'];
          print("apiRequestStatus: $apiRequestStatus");
          if (apiRequestStatus == "Success") {
            // print("Yes");
            setState(() {
              subscriptionPlans = data['SubscriptionPlans'];
            });
            for (var i = 0; i < subscriptionPlans.length; i++) {
              var subscription = subscriptionPlans[i]['SubscriptionTypeID'];
              if (subscription == subscriptionId) {
                setState(() {
                  selected = i;
                });
                break;
              }
            }
            if (subscriptionPlans.isEmpty) {
              apiErrMessage = "No Subscription Plans Found.";
              Flushbar(
                message: apiErrMessage,
                duration: Duration(seconds: 2),
                backgroundColor: Colors.red,
              ).show(context);
            }
          } else {
            apiErrMessage = data['Errors'][0];
            Flushbar(
              message: apiErrMessage,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          apiRequestStatus = false;
          apiErrMessage = "No data found.";
          Flushbar(
            message: apiErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        apiRequestStatus = false;
        apiErrMessage = "Error: ${response.statusCode}.";
        Flushbar(
          message: apiErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      apiRequestStatus = false;
      apiErrMessage = connectionChk;
      Flushbar(
        message: apiErrMessage,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
  }

  subscribeNow() async {
    setState(() {
      isPageLoading = true;
    });
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      //multipart/form-data POST Method
      Map<String, String> requestBody = <String, String>{
        "ProviderKey": providerKey,
        "SubscriptionTypeID": selectedSubscriptionPlan.toString(),
      };
      // Map<String, String> headers = <String, String>{
      //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
      // };
      var uri = Uri.parse(Constants.activateSubscription);
      var request = http.MultipartRequest('POST', uri)
        // ..headers.addAll(
        //     headers) //if u have headers, basic auth, token bearer... Else remove line
        ..fields.addAll(requestBody);
      var response = await request.send();
      final respStr = await response.stream.bytesToString();
      //

      print("response.request:${response.request}");
      if (response.statusCode == 200) {
        var data = json.decode(respStr);
        // print("data: $data");
        if (data != null) {
          apiRequestStatus = data['Status'];
          print("apiRequestStatus: $apiRequestStatus");
          if (apiRequestStatus == "Success") {
            // print("Yes");
            Flushbar(
              message: apiRequestStatus,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.green,
            ).show(context);
          } else {
            apiErrMessage = data['Errors'][0];
            Flushbar(
              message: apiErrMessage,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          apiRequestStatus = false;
          apiErrMessage = "No data found.";
          Flushbar(
            message: apiErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        apiRequestStatus = false;
        apiErrMessage = "Error: ${response.statusCode}.";
        Flushbar(
          message: apiErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      apiRequestStatus = false;
      apiErrMessage = connectionChk;
      Flushbar(
        message: apiErrMessage,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
  }

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<String> proKey =
        preferences.getString('ProviderKey', defaultValue: "");
    Preference<int> subscriptionID =
        preferences.getInt('SubscriptionID', defaultValue: 0);
    Preference<bool> isDark =
        preferences.getBool('isDark', defaultValue: false);
    proKey.listen((value) {
      setState(() {
        providerKey = value;
      });
    });
    subscriptionID.listen((value) {
      setState(() {
        subscriptionId = value;
      });
    });
    isDark.listen((value) {
      setState(() {
        isDarkMode = value;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getSharedDataPref();
    getSubscriptionPlans();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(),
      bottomNavigationBar: MyBottomNavBar(),
      drawer: MyNavDrawer(),
      body: isPageLoading
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : connectionChk != null
              ? Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      NoInternetWidget(),
                      ElevatedButton(
                        onPressed: () {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    super.widget),
                          );
                        },
                        child: Text("Retry"),
                      ),
                    ],
                  ),
                )
              : Container(
                  child: ListView(
                    children: [
                      Container(
                        margin: EdgeInsets.all(10.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              height: 250.0,
                              width: 250.0,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage("assets/images/1.png"),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            Text(
                              "Our Subscription Plans",
                              style: Theme.of(context).textTheme.headline2,
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                left: 10.0,
                                right: 10.0,
                              ),
                              child: Text(
                                "Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                                style: Theme.of(context).textTheme.bodyText1,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 180.0,
                        margin: EdgeInsets.only(top: 15.0, left: 10.0),
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: subscriptionPlans.length,
                          itemBuilder: (BuildContext context, index) {
                            return GestureDetector(
                              onTap: () {
                                setState(() {
                                  selectedSubscriptionPlan =
                                      subscriptionPlans[index]
                                          ['SubscriptionTypeID'];
                                });
                              },
                              onTapDown: (TapDownDetails details) {
                                setState(() {
                                  selected = index;
                                });
                              },
                              child: Card(
                                elevation: 5.0,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                color: selected == index
                                    ? Theme.of(context).highlightColor
                                    : null,
                                child: Container(
                                  margin: EdgeInsets.all(10.0),
                                  width: 160.0,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        subscriptionPlans[index]
                                            ['SubscriptionName'],
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline3,
                                      ),
                                      Text(
                                        subscriptionPlans[index]['Validity']
                                                .toString() +
                                            " Days",
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline6,
                                      ),
                                      Text(
                                        "Tax % :" +
                                            subscriptionPlans[index]
                                                    ['TaxPercentage']
                                                .toString(),
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .primaryColorLight,
                                        ),
                                      ),
                                      Text(
                                        "Disc Amt :" +
                                            subscriptionPlans[index]
                                                    ['DiscountAmount']
                                                .toString(),
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .primaryColorLight,
                                        ),
                                      ),
                                      Text(
                                        "Cost :" +
                                            subscriptionPlans[index]
                                                    ['SubscriptionCost']
                                                .toString(),
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .primaryColorLight,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                      // GestureDetector(
                      //   onTap: () {
                      //     subscribeNow();
                      //   },
                      //   child: Container(
                      //     margin: EdgeInsets.fromLTRB(30.0, 15.0, 30.0, 15.0),
                      //     height: 45.0,
                      //     decoration: BoxDecoration(
                      //       color: Theme.of(context).primaryColor,
                      //       borderRadius: BorderRadius.circular(35.0),
                      //     ),
                      //     child: Center(
                      //       child: Text(
                      //         "Subscribe Now",
                      //         style: Theme.of(context).textTheme.subtitle1,
                      //       ),
                      //     ),
                      //   ),
                      // ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.fromLTRB(30.0, 15.0, 30.0, 15.0),
                        child: ElevatedButton(
                          onPressed: () {
                            subscribeNow();
                          },
                          child: Text("Subscribe Now"),
                        ),
                      ),
                    ],
                  ),
                ),
    );
  }
}
