import 'package:flutter/material.dart';
// import 'package:flutter_color/flutter_color.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import '../widgets/myAppBar.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myNavBar.dart';

import './examDetailsScreen.dart';
import './editProfileScreen.dart';
import './subscriptionScreen.dart';

class UsersProfilePage extends StatefulWidget {
  final List userAttemptedExamDetails;
  final List userExamCategoryListDetails;
  UsersProfilePage({
    this.userAttemptedExamDetails,
    this.userExamCategoryListDetails,
  });
  @override
  _UsersProfilePageState createState() => _UsersProfilePageState();
}

class _UsersProfilePageState extends State<UsersProfilePage> {
  var userName = "User Name";
  var userEmail = "username@gmail.com";
  var profileImage;
  var providerKey;
  var subscriptionPlanName;
  var subscriptionPlanValidFrom;
  var subscriptionPlanExpiryOn;
  bool isDarkMode = false;
  var numberOfDaysPassed;
  var numberOfExamsAttended;
  var highestScoreAchieved;

  List usersProfileList = [
    {
      "title": "Number of Days",
      "value": "75",
    },
    {
      "title": "Exams Attended",
      "value": "12",
    },
    {
      "title": "Highest Score",
      "value": "85",
    },
  ];

  Map usersProfileMap = {};

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<String> username =
        preferences.getString('UserName', defaultValue: "");
    Preference<bool> isDark =
        preferences.getBool('isDark', defaultValue: false);
    Preference<String> useremail =
        preferences.getString('Email', defaultValue: "");
    Preference<String> profileImg =
        preferences.getString('ProfileImage', defaultValue: "");
    Preference<String> proKey =
        preferences.getString('ProviderKey', defaultValue: "");
    Preference<String> subscriptionName =
        preferences.getString('SubscriptionName', defaultValue: "");
    Preference<String> subscriptionValidFrom =
        preferences.getString('SubscriptionValidFrom', defaultValue: "");
    Preference<String> subscriptionExpiryOn =
        preferences.getString('SubscriptionExpiryOn', defaultValue: "");
    Preference<int> noOfDaysPassed =
        preferences.getInt('NumberOfDaysPassed', defaultValue: 0);
    Preference<int> noOfExamsAttended =
        preferences.getInt('NumberOfExamsAttended', defaultValue: 0);
    Preference<int> highScoreAchieved =
        preferences.getInt('HighestScoreAchieved', defaultValue: 0);

    username.listen((value) {
      setState(() {
        userName = value;
      });
    });
    useremail.listen((value) {
      setState(() {
        userEmail = value;
      });
    });
    profileImg.listen((value) {
      setState(() {
        profileImage = value;
      });
    });
    proKey.listen((value) {
      setState(() {
        providerKey = value;
      });
    });
    subscriptionName.listen((value) {
      setState(() {
        subscriptionPlanName = value;
      });
    });
    subscriptionValidFrom.listen((value) {
      setState(() {
        subscriptionPlanValidFrom = value;
      });
    });
    subscriptionExpiryOn.listen((value) {
      setState(() {
        subscriptionPlanExpiryOn = value;
      });
    });
    isDark.listen((value) {
      setState(() {
        isDarkMode = value;
      });
    });
    // print("userName: $userName");
    // print("profileImage: $profileImage");
    noOfDaysPassed.listen((value) {
      setState(() {
        numberOfDaysPassed = value;
      });
      // print("numberOfDaysPassed:$numberOfDaysPassed");
      generateUserProfileMap();
    });
    noOfExamsAttended.listen((value) {
      setState(() {
        numberOfExamsAttended = value;
      });
      generateUserProfileMap();
    });
    highScoreAchieved.listen((value) {
      setState(() {
        highestScoreAchieved = value;
      });
      generateUserProfileMap();
    });
  }

  generateUserProfileMap() {
    usersProfileMap["Number of Days"] = numberOfDaysPassed;
    usersProfileMap["Exams Attended"] = numberOfExamsAttended;
    usersProfileMap["Highest Score"] = highestScoreAchieved;
    print("usersProfileMap: $usersProfileMap");
  }

  @override
  void initState() {
    super.initState();
    getSharedDataPref();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(),
      bottomNavigationBar: MyBottomNavBar(),
      drawer: MyNavDrawer(),
      body: Container(
        // color: Theme.of(context).primaryColor,
        child: ListView(
          children: [
            Stack(
              alignment: Alignment.topCenter,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 95.0),
                  padding: EdgeInsets.all(10.0),
                  // height: MediaQuery.of(context).size.height,
                  decoration: BoxDecoration(
                    // color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30.0),
                      topRight: Radius.circular(30.0),
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.only(
                          top: 85.0,
                          bottom: 25.0,
                        ),
                        child: ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => EditProfileScreen(
                                  editScreenType: "UserProfile",
                                ),
                              ),
                            );
                          },
                          child: Text("Edit Profile"),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 85.0,
                        margin: EdgeInsets.only(
                          // top: 85.0,
                          bottom: 25.0,
                        ),
                        // child: ListView.builder(
                        //   scrollDirection: Axis.horizontal,
                        //   itemCount: usersProfileList.length,
                        //   itemBuilder: (BuildContext context, index) {
                        //     return GestureDetector(
                        //       onTap: () {},
                        //       child: Card(
                        //         elevation: 5.0,
                        //         shape: RoundedRectangleBorder(
                        //           borderRadius: BorderRadius.circular(15.0),
                        //         ),
                        //         margin: EdgeInsets.all(3.0),
                        //         color: Theme.of(context).primaryColor,
                        //         child: Container(
                        //           // margin: EdgeInsets.all(3.0),
                        //           // padding: EdgeInsets.all(5.0),
                        //           width: 150.0,
                        //           // decoration: BoxDecoration(
                        //           //   color: index / 2 == 1 || index / 2 == 0
                        //           //       ? Theme.of(context).primaryColor
                        //           //       : Theme.of(context).primaryColorLight,
                        //           //   borderRadius: BorderRadius.circular(15.0),
                        //           // ),
                        //           child: Center(
                        //             child: Column(
                        //               crossAxisAlignment:
                        //                   CrossAxisAlignment.center,
                        //               mainAxisAlignment:
                        //                   MainAxisAlignment.center,
                        //               children: [
                        //                 Text(
                        //                   usersProfileList[index]['title'],
                        //                   // style: TextStyle(),
                        //                 ),
                        //                 Text(
                        //                   usersProfileList[index]['value'],
                        //                   style: Theme.of(context)
                        //                       .textTheme
                        //                       .headline4,
                        //                 ),
                        //               ],
                        //             ),
                        //           ),
                        //         ),
                        //       ),
                        //     );
                        //   },
                        // ),
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: usersProfileMap.length,
                          itemBuilder: (BuildContext context, index) {
                            String usersProfileMapKey =
                                usersProfileMap.keys.elementAt(index);
                            var usersProfileMapValue = usersProfileMap.values
                                .elementAt(index)
                                .toString();
                            return GestureDetector(
                              onTap: () {},
                              child: Card(
                                elevation: 5.0,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                margin: EdgeInsets.all(3.0),
                                color: isDarkMode
                                    ? null
                                    : Theme.of(context).primaryColor,
                                child: Container(
                                  // margin: EdgeInsets.all(3.0),
                                  // padding: EdgeInsets.all(5.0),
                                  width: 150.0,
                                  // decoration: BoxDecoration(
                                  //   color: index / 2 == 1 || index / 2 == 0
                                  //       ? Theme.of(context).primaryColor
                                  //       : Theme.of(context).primaryColorLight,
                                  //   borderRadius: BorderRadius.circular(15.0),
                                  // ),
                                  child: Center(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          usersProfileMapKey,
                                        ),
                                        Text(
                                          usersProfileMapValue,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline4,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                      Container(
                        child: Column(
                          children: [
                            Text(
                              "Exam Results",
                              style: Theme.of(context).textTheme.headline6,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                        child: GridView.count(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          scrollDirection: Axis.vertical,
                          crossAxisCount: 2,
                          childAspectRatio: 2,
                          padding: const EdgeInsets.all(5.0),
                          mainAxisSpacing: 4.0,
                          crossAxisSpacing: 4.0,
                          children: List.generate(
                              widget.userExamCategoryListDetails.length,
                              (index) {
                            return Stack(
                              children: [
                                Positioned.fill(
                                  bottom: 0.0,
                                  child: Card(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    elevation: 5.0,
                                    child: Container(
                                      padding: EdgeInsets.all(8.0),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          widget.userExamCategoryListDetails[
                                              index]['CategoryName'],
                                          style: TextStyle(
                                            color: Colors.grey,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16.0,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Positioned.fill(
                                  child: Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      borderRadius: BorderRadius.circular(10),
                                      highlightColor: Colors.transparent,
                                      splashColor:
                                          Theme.of(context).highlightColor,
                                      onTap: () {
                                        Navigator.of(context).push(
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                ExamDetailsScreen(
                                              userExamCategoryId: widget
                                                      .userExamCategoryListDetails[
                                                  index]['CategoryID'],
                                              userAttemptedExamDetails: widget
                                                  .userAttemptedExamDetails,
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                ),
                              ],
                            );
                          }),
                        ),
                        // child: Column(
                        //   children: [
                        //     Row(
                        //       // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        //       children: [
                        //         Expanded(
                        //           child: GestureDetector(
                        //             onTap: () {
                        //               Navigator.of(context).push(
                        //                 MaterialPageRoute(
                        //                   builder: (context) =>
                        //                       ExamDetailsScreen(),
                        //                 ),
                        //               );
                        //             },
                        //             child: Card(
                        //               shape: RoundedRectangleBorder(
                        //                 borderRadius: BorderRadius.circular(10),
                        //               ),
                        //               elevation: 5.0,
                        //               child: Container(
                        //                 // width: 160.0,
                        //                 width:
                        //                     MediaQuery.of(context).size.width *
                        //                         0.45,
                        //                 decoration: BoxDecoration(
                        //                   borderRadius:
                        //                       BorderRadius.circular(10),
                        //                 ),
                        //                 child: Row(
                        //                   mainAxisAlignment:
                        //                       MainAxisAlignment.spaceBetween,
                        //                   children: [
                        //                     Container(
                        //                       padding: EdgeInsets.all(10.0),
                        //                       child: Column(
                        //                         crossAxisAlignment:
                        //                             CrossAxisAlignment.start,
                        //                         children: [
                        //                           Text(
                        //                             "Exam",
                        //                             style: TextStyle(
                        //                               color: Colors.grey,
                        //                               fontWeight:
                        //                                   FontWeight.bold,
                        //                               fontSize: 16.0,
                        //                             ),
                        //                           ),
                        //                           Text(
                        //                             "Category 1",
                        //                             style: TextStyle(
                        //                               color: Colors.grey,
                        //                               fontWeight:
                        //                                   FontWeight.bold,
                        //                               fontSize: 16.0,
                        //                             ),
                        //                           ),
                        //                         ],
                        //                       ),
                        //                     ),
                        //                   ],
                        //                 ),
                        //               ),
                        //             ),
                        //           ),
                        //         ),
                        //         SizedBox(
                        //           width: 10.0,
                        //         ),
                        //         Expanded(
                        //           child: GestureDetector(
                        //             onTap: () {
                        //               Navigator.of(context).push(
                        //                 MaterialPageRoute(
                        //                   builder: (context) =>
                        //                       ExamDetailsScreen(),
                        //                 ),
                        //               );
                        //             },
                        //             child: Card(
                        //               shape: RoundedRectangleBorder(
                        //                 borderRadius: BorderRadius.circular(10),
                        //               ),
                        //               elevation: 5.0,
                        //               child: Container(
                        //                 // width: 160.0,
                        //                 width:
                        //                     MediaQuery.of(context).size.width *
                        //                         0.45,
                        //                 decoration: BoxDecoration(
                        //                   borderRadius:
                        //                       BorderRadius.circular(10),
                        //                 ),
                        //                 child: Row(
                        //                   mainAxisAlignment:
                        //                       MainAxisAlignment.spaceBetween,
                        //                   children: [
                        //                     Container(
                        //                       padding: EdgeInsets.all(10.0),
                        //                       child: Column(
                        //                         crossAxisAlignment:
                        //                             CrossAxisAlignment.start,
                        //                         children: [
                        //                           Text(
                        //                             "Exam",
                        //                             style: TextStyle(
                        //                               color: Colors.grey,
                        //                               fontWeight:
                        //                                   FontWeight.bold,
                        //                               fontSize: 16.0,
                        //                             ),
                        //                           ),
                        //                           Text(
                        //                             "Category 2",
                        //                             style: TextStyle(
                        //                               color: Colors.grey,
                        //                               fontWeight:
                        //                                   FontWeight.bold,
                        //                               fontSize: 16.0,
                        //                             ),
                        //                           ),
                        //                         ],
                        //                       ),
                        //                     ),
                        //                   ],
                        //                 ),
                        //               ),
                        //             ),
                        //           ),
                        //         ),
                        //       ],
                        //     ),
                        //     SizedBox(
                        //       height: 10.0,
                        //     ),
                        //     Row(
                        //       // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        //       children: [
                        //         Expanded(
                        //           child: GestureDetector(
                        //             onTap: () {
                        //               Navigator.of(context).push(
                        //                 MaterialPageRoute(
                        //                   builder: (context) =>
                        //                       ExamDetailsScreen(),
                        //                 ),
                        //               );
                        //             },
                        //             child: Card(
                        //               shape: RoundedRectangleBorder(
                        //                 borderRadius: BorderRadius.circular(10),
                        //               ),
                        //               elevation: 5.0,
                        //               child: Container(
                        //                 // width: 160.0,
                        //                 width:
                        //                     MediaQuery.of(context).size.width *
                        //                         0.45,
                        //                 decoration: BoxDecoration(
                        //                   borderRadius:
                        //                       BorderRadius.circular(10),
                        //                 ),
                        //                 child: Row(
                        //                   mainAxisAlignment:
                        //                       MainAxisAlignment.spaceBetween,
                        //                   children: [
                        //                     Container(
                        //                       padding: EdgeInsets.all(10.0),
                        //                       child: Column(
                        //                         crossAxisAlignment:
                        //                             CrossAxisAlignment.start,
                        //                         children: [
                        //                           Text(
                        //                             "Exam",
                        //                             style: TextStyle(
                        //                               color: Colors.grey,
                        //                               fontWeight:
                        //                                   FontWeight.bold,
                        //                               fontSize: 16.0,
                        //                             ),
                        //                           ),
                        //                           Text(
                        //                             "Category 3",
                        //                             style: TextStyle(
                        //                               color: Colors.grey,
                        //                               fontWeight:
                        //                                   FontWeight.bold,
                        //                               fontSize: 16.0,
                        //                             ),
                        //                           ),
                        //                         ],
                        //                       ),
                        //                     ),
                        //                   ],
                        //                 ),
                        //               ),
                        //             ),
                        //           ),
                        //         ),
                        //         SizedBox(
                        //           width: 10.0,
                        //         ),
                        //         Expanded(
                        //           child: GestureDetector(
                        //             onTap: () {
                        //               Navigator.of(context).push(
                        //                 MaterialPageRoute(
                        //                   builder: (context) =>
                        //                       ExamDetailsScreen(),
                        //                 ),
                        //               );
                        //             },
                        //             child: Card(
                        //               shape: RoundedRectangleBorder(
                        //                 borderRadius: BorderRadius.circular(10),
                        //               ),
                        //               elevation: 5.0,
                        //               child: Container(
                        //                 // width: 160.0,
                        //                 width:
                        //                     MediaQuery.of(context).size.width *
                        //                         0.45,
                        //                 decoration: BoxDecoration(
                        //                   borderRadius:
                        //                       BorderRadius.circular(10),
                        //                 ),
                        //                 child: Row(
                        //                   mainAxisAlignment:
                        //                       MainAxisAlignment.spaceBetween,
                        //                   children: [
                        //                     Container(
                        //                       padding: EdgeInsets.all(10.0),
                        //                       child: Column(
                        //                         crossAxisAlignment:
                        //                             CrossAxisAlignment.start,
                        //                         children: [
                        //                           Text(
                        //                             "Exam",
                        //                             style: TextStyle(
                        //                               color: Colors.grey,
                        //                               fontWeight:
                        //                                   FontWeight.bold,
                        //                               fontSize: 16.0,
                        //                             ),
                        //                           ),
                        //                           Text(
                        //                             "Category 4",
                        //                             style: TextStyle(
                        //                               color: Colors.grey,
                        //                               fontWeight:
                        //                                   FontWeight.bold,
                        //                               fontSize: 16.0,
                        //                             ),
                        //                           ),
                        //                         ],
                        //                       ),
                        //                     ),
                        //                   ],
                        //                 ),
                        //               ),
                        //             ),
                        //           ),
                        //         ),
                        //       ],
                        //     ),
                        //   ],
                        // ),
                      ),
                      // Container(
                      //   margin: EdgeInsets.only(top: 10.0),
                      //   child: Card(
                      //     elevation: 5.0,
                      //     shape: RoundedRectangleBorder(
                      //       borderRadius: BorderRadius.circular(10.0),
                      //     ),
                      //     child: Container(
                      //       padding: EdgeInsets.all(10.0),
                      //       child: Column(
                      //         children: [
                      //           Text(
                      //             "Exam Details",
                      //             style: Theme.of(context).textTheme.headline6,
                      //           ),
                      //           SizedBox(
                      //             height: 10.0,
                      //           ),
                      //           Row(
                      //             mainAxisAlignment:
                      //                 MainAxisAlignment.spaceBetween,
                      //             children: [
                      //               Text(
                      //                 "Exam Name",
                      //                 style: TextStyle(
                      //                   color: HexColor("#1fc09f"),
                      //                   fontWeight: FontWeight.bold,
                      //                 ),
                      //               ),
                      //               SizedBox(
                      //                 width: 10.0,
                      //               ),
                      //               Text(
                      //                 "Date",
                      //                 style: TextStyle(
                      //                   color: HexColor("#1fc09f"),
                      //                   fontWeight: FontWeight.bold,
                      //                 ),
                      //               ),
                      //               SizedBox(
                      //                 width: 110.0,
                      //               ),
                      //             ],
                      //           ),
                      //           for (var i = 0;
                      //               i < widget.userAttemptedExamDetails.length;
                      //               i++)
                      //             Container(
                      //               margin: EdgeInsets.only(top: 10.0),
                      //               child: Row(
                      //                 crossAxisAlignment:
                      //                     CrossAxisAlignment.center,
                      //                 mainAxisAlignment:
                      //                     MainAxisAlignment.spaceBetween,
                      //                 children: [
                      //                   Flexible(
                      //                     child: Text(
                      //                       widget.userAttemptedExamDetails[i]
                      //                           ['ExamName'],
                      //                       style: Theme.of(context)
                      //                           .textTheme
                      //                           .bodyText1,
                      //                     ),
                      //                   ),
                      //                   SizedBox(
                      //                     width: 10.0,
                      //                   ),
                      //                   Text(
                      //                     widget.userAttemptedExamDetails[i]
                      //                         ['ExamAttemptedOnFormatted'],
                      //                     style: Theme.of(context)
                      //                         .textTheme
                      //                         .bodyText1,
                      //                   ),
                      //                   SizedBox(
                      //                     width: 10.0,
                      //                   ),
                      //                   Container(
                      //                     width: 100.0,
                      //                     height: 35.0,
                      //                     decoration: BoxDecoration(
                      //                       borderRadius:
                      //                           BorderRadius.circular(15.0),
                      //                       color:
                      //                           Theme.of(context).primaryColor,
                      //                     ),
                      //                     child: Center(
                      //                       child: Text(
                      //                         "View Result",
                      //                         style: TextStyle(
                      //                           fontWeight: FontWeight.w500,
                      //                         ),
                      //                       ),
                      //                     ),
                      //                   )
                      //                 ],
                      //               ),
                      //             )
                      //         ],
                      //       ),
                      //     ),
                      //   ),
                      // ),
                      Container(
                        margin: EdgeInsets.only(top: 10.0),
                        child: Card(
                          elevation: 5.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Container(
                            padding: EdgeInsets.all(10.0),
                            child: Column(
                              children: [
                                Text(
                                  "Subscription Details",
                                  style: Theme.of(context).textTheme.headline6,
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                Row(
                                  children: [
                                    Text(
                                      "Your Plan ",
                                      style: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorLight,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(
                                      ":",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    subscriptionPlanName != null
                                        ? Text(
                                            subscriptionPlanName,
                                            style: isDarkMode
                                                ? Theme.of(context)
                                                    .textTheme
                                                    .bodyText2
                                                : Theme.of(context)
                                                    .textTheme
                                                    .bodyText1,
                                          )
                                        : Container(),
                                  ],
                                ),
                                SizedBox(
                                  height: 5.0,
                                ),
                                Row(
                                  children: [
                                    Text(
                                      "Date of Subscription ",
                                      style: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorLight,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(
                                      ":",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    subscriptionPlanValidFrom != null
                                        ? Text(
                                            subscriptionPlanValidFrom,
                                            style: isDarkMode
                                                ? Theme.of(context)
                                                    .textTheme
                                                    .bodyText2
                                                : Theme.of(context)
                                                    .textTheme
                                                    .bodyText1,
                                          )
                                        : Container(),
                                  ],
                                ),
                                SizedBox(
                                  height: 5.0,
                                ),
                                Row(
                                  children: [
                                    Text(
                                      "Date of Expiry ",
                                      style: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorLight,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(
                                      ":",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    subscriptionPlanExpiryOn != null
                                        ? Text(
                                            subscriptionPlanExpiryOn,
                                            style: isDarkMode
                                                ? Theme.of(context)
                                                    .textTheme
                                                    .bodyText2
                                                : Theme.of(context)
                                                    .textTheme
                                                    .bodyText1,
                                          )
                                        : Container(),
                                  ],
                                ),
                                SizedBox(
                                  height: 5.0,
                                ),
                                Row(
                                  children: [
                                    Text(
                                      "Benefits ",
                                      style: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorLight,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(
                                      ":",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(
                                      "Lorem ipsum dolor sit amet.",
                                      style: isDarkMode
                                          ? Theme.of(context)
                                              .textTheme
                                              .bodyText2
                                          : Theme.of(context)
                                              .textTheme
                                              .bodyText1,
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    ElevatedButton(
                                      onPressed: () {
                                        Navigator.of(context).push(
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                SubscriptionScreen(),
                                          ),
                                        );
                                      },
                                      child: Text(
                                        "Renew Plan",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                    ElevatedButton(
                                      onPressed: () {
                                        Navigator.of(context).push(
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                SubscriptionScreen(),
                                          ),
                                        );
                                      },
                                      child: Text(
                                        "Upgrade Plan",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: 40.0,
                  child: Column(
                    children: [
                      Container(
                        height: 110.0,
                        width: 110.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15.0),
                          image: DecorationImage(
                            // image: AssetImage("assets/images/8.jpg"),
                            image: profileImage != null
                                ? NetworkImage(profileImage)
                                : AssetImage("assets/icons/ic10.png"),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      userName != null
                          ? Container(
                              margin: EdgeInsets.only(top: 10.0),
                              child: Text(
                                userName,
                                style: TextStyle(
                                  color: Theme.of(context).primaryColor,
                                ),
                              ),
                            )
                          : Container(),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
