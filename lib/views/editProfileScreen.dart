import 'dart:io';

import 'package:flutter/material.dart';
// import 'package:hexcolor/hexcolor.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:another_flushbar/flushbar.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';
import 'package:path/path.dart' as Path;

// import '../widgets/myLogoWidget.dart';
import '../widgets/myAppBar.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myNavBar.dart';
import '../widgets/noInternetWidget.dart';

import '../helpers/appFunctions.dart';
import '../services/connections.dart' as Constants;

class EditProfileScreen extends StatefulWidget {
  final String editScreenType;
  EditProfileScreen({this.editScreenType});
  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth > 1200) {
          return DesktopEditProfileScreen();
        } else if (constraints.maxWidth > 800 && constraints.maxWidth < 1200) {
          return MobileEditProfileScreen(
            editScreenType: widget.editScreenType,
          );
        } else {
          return MobileEditProfileScreen(
            editScreenType: widget.editScreenType,
          );
        }
      },
    );
  }
}

class MobileEditProfileScreen extends StatefulWidget {
  final String editScreenType;
  MobileEditProfileScreen({this.editScreenType});
  @override
  _MobileEditProfileScreenState createState() =>
      _MobileEditProfileScreenState();
}

class _MobileEditProfileScreenState extends State<MobileEditProfileScreen> {
  bool iAgreeTerms = false;
  var profileUpdateStatus;
  var profileUpdateErrMessage;
  var connectionChk;
  bool isPageLoading = false;
  var providerKey;
  var profileImage;
  bool showAll = false;
  bool isDarkMode = false;

  TextEditingController dobTextCntlr = new TextEditingController();
  TextEditingController firstNameTextCntlr = new TextEditingController();
  TextEditingController lastNameTextCntlr = new TextEditingController();
  TextEditingController emailTextCntlr = new TextEditingController();
  TextEditingController contactNumTextCntlr = new TextEditingController();
  TextEditingController passwordTextCntlr = new TextEditingController();
  TextEditingController occupationTextCntlr = new TextEditingController();
  TextEditingController educationQualificationTextCntlr =
      new TextEditingController();
  TextEditingController subjectTextCntlr = new TextEditingController();
  TextEditingController lastInstitutionTextCntlr = new TextEditingController();

  DateTime selectedDate = DateTime.now();
  var formatter = new DateFormat('dd-MM-yyyy');
  var inputFormatter = new DateFormat('yyyy-MM-dd');

  List imgPickerOptions = [
    {"iconData": Icons.camera, "optionsLabel": "Camera"},
    {"iconData": Icons.photo, "optionsLabel": "Gallery"}
  ];
  List<String> _genders = ['Male', 'Female', 'Others'];
  String _selectedGender;
  List<String> _district = [
    'Trivandrum',
    'Kollam',
    'Alappuzha',
    'Pathanamthitta',
    'Kottayam',
    'Idukki',
    'Ernakulam',
    'Thrissur',
    'Palakkad',
    'Malappuram',
    'Kozhikode',
    'Wayanad',
    'Kannur',
    'Kasaragod',
  ];
  String _selectedDistrict;
  List<String> _courses = ['5Yr LLB', '3Yr LLB'];
  String _selectedCourse;
  List<String> _entranceTypes = ['Fresher', 'Repeater'];
  String _selectedEntranceType;

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime.now().subtract(Duration(days: 365 * 80)),
      lastDate: DateTime(4020),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: Theme.of(context).primaryColor, //Head background
            accentColor: Theme.of(context).accentColor, //selection color
            colorScheme:
                ColorScheme.light(primary: Theme.of(context).primaryColor),
            buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
          ),
          child: child,
        );
      },
    );
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
        dobTextCntlr.text = formatter.format(selectedDate).toString();
      });
    }
  }

  updateUserProfile() async {
    setState(() {
      isPageLoading = true;
    });
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      var contactNum = contactNumTextCntlr.text;
      var lastName = lastNameTextCntlr.text;
      var firstName = firstNameTextCntlr.text;
      var email = emailTextCntlr.text;
      var occupation = occupationTextCntlr.text;
      var educationQualification = educationQualificationTextCntlr.text;
      var subject = subjectTextCntlr.text;
      var lastInstitution = lastInstitutionTextCntlr.text;
      var fullName = firstName + " " + lastName;
      var dob = formatter.format(selectedDate).toString();
      var password = passwordTextCntlr.text;
      //multipart/form-data POST Method
      Map<String, String> requestBody = widget.editScreenType == "Settings"
          ? <String, String>{
              "ProviderKey": providerKey,
              "Email": email,
              "PhoneNumber": contactNum,
              "Password": password,
            }
          : <String, String>{
              "ProviderKey": providerKey,
              "FirstName": firstName,
              "LastName": lastName,
              "FullName": fullName,
              "Email": email,
              "PhoneNumber": contactNum,
              "Gender": _selectedGender,
              "Occupation": occupation,
              "District": _selectedDistrict,
              "EducationalQualification": educationQualification,
              "Subject": subject,
              "LastInstitutionStudied": lastInstitution,
              "Course": _selectedCourse,
              "EntranceType": _selectedEntranceType,
              "DateOfBirth": dob,
              "AddressLine1": "",
              "AddressLine2": "",
            };
      // Map<String, String> headers = <String, String>{
      //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
      // };
      var uri = widget.editScreenType == "Settings"
          ? Uri.parse(Constants.updateUserSettings)
          : Uri.parse(Constants.updateUserProfile);
      var request = http.MultipartRequest('POST', uri)
        // ..headers.addAll(
        //     headers) //if u have headers, basic auth, token bearer... Else remove line
        ..fields.addAll(requestBody);
      var response = await request.send();
      final respStr = await response.stream.bytesToString();
      //

      print("response.request:${response.request}");
      if (response.statusCode == 200) {
        var data = json.decode(respStr);
        print("data: $data");
        if (data != null) {
          profileUpdateStatus = data['Status'];
          profileUpdateErrMessage = data['Errors'];
          print("profileUpdateStatus: $profileUpdateStatus");
          if (profileUpdateStatus == "Success") {
            Flushbar(
              message: widget.editScreenType == "Settings"
                  ? "Profile Settings Updated Successfully."
                  : "Profile Updated Successfully.",
              duration: Duration(seconds: 2),
              backgroundColor: Colors.green,
            ).show(context);
          } else {
            Flushbar(
              message: profileUpdateErrMessage[0],
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          profileUpdateStatus = false;
          profileUpdateErrMessage = "No data found.";
          Flushbar(
            message: profileUpdateErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        profileUpdateStatus = false;
        profileUpdateErrMessage = "Error: ${response.statusCode}.";
        Flushbar(
          message: profileUpdateErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      profileUpdateStatus = false;
      profileUpdateErrMessage = connectionChk;
      Flushbar(
        message: profileUpdateErrMessage,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
  }

  //Image Picking
  File imageFile;
  PickedFile _img;
  final ImagePicker _picker = ImagePicker();
  getProfileImg(src) async {
    final preferences = await StreamingSharedPreferences.instance;
    setState(() {
      isPageLoading = true;
    });
    final image = await _picker.getImage(source: src);
    setState(() {
      _img = image;
    });
    imageFile = File(_img.path);
    print("_img:=> $_img");
    print("imageFile:=> $imageFile");

    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      // open a byteStream
      var stream = new http.ByteStream(imageFile.openRead());
      stream.cast();
      // get file length
      var length = await imageFile.length();

      // string to uri
      var uri = Uri.parse(Constants.updateUserProfileImage);

      // create multipart request
      var request = new http.MultipartRequest("POST", uri);

      // if you need more parameters to parse, add those like this. i added "user_id". here this "ProviderKey" is a key of the API request
      request.fields["ProviderKey"] = providerKey;

      // multipart that takes file.. here this "ProfileImage" is a key of the API request
      var multipartFile = new http.MultipartFile(
          'UserProfileImage', stream, length,
          filename: Path.basename(imageFile.path));

      // add file to multipart
      request.files.add(multipartFile);

      // send request to upload image
      await request.send().then((response) async {
        // listen for response
        response.stream.transform(utf8.decoder).listen((value) async {
          // print("ResponseVal: $value");
          if (response.statusCode == 200) {
            var imgUploadData = json.decode(value);
            print("imgUploadData:$imgUploadData");
            if (imgUploadData != null) {
              var imgUploadDataStatus = imgUploadData['Status'];
              if (imgUploadDataStatus == "Success") {
                var profileImage = imgUploadData['ProfileImage'];
                preferences.setString('ProfileImage', profileImage);
                Flushbar(
                  message: "Profile Image Updated Successfully.",
                  duration: Duration(seconds: 2),
                  backgroundColor: Colors.green,
                ).show(context);
                getSharedDataPref();
              } else {
                Flushbar(
                  message: "Profile Image Update Failed.",
                  duration: Duration(seconds: 2),
                  backgroundColor: Colors.red,
                ).show(context);
              }
            } else {
              Flushbar(
                message: "Empty UploadData.",
                duration: Duration(seconds: 2),
                backgroundColor: Colors.red,
              ).show(context);
            }
          } else {
            Flushbar(
              message: "Error: ${response.statusCode}",
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        });
      }).catchError((e) {
        print(e);
      });
    } else {
      Flushbar(
        message: connectionChk,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
  }
  //Image picking End

  showImgPickerOptionsBottomSheet() {
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25.0),
          topRight: Radius.circular(25.0),
        ),
      ),
      context: context,
      builder: (BuildContext _) {
        return Container(
          // height: MediaQuery.of(context).size.height * 0.05,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25.0),
              topRight: Radius.circular(25.0),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView.separated(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              primary: true,
              itemCount: imgPickerOptions.length,
              // itemCount: 1,
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  leading: Icon(imgPickerOptions[index]['iconData']),
                  title: Text(
                    imgPickerOptions[index]['optionsLabel'],
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  onTap: () {
                    if (imgPickerOptions[index]['optionsLabel'] == "Camera") {
                      getProfileImg(ImageSource.camera);
                    } else {
                      getProfileImg(ImageSource.gallery);
                    }
                    Navigator.pop(context);
                  },
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            ),
          ),
        );
      },
      isScrollControlled: true,
    );
  }

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<String> username =
        preferences.getString('UserName', defaultValue: "");
    Preference<String> useremail =
        preferences.getString('Email', defaultValue: "");
    Preference<String> profileImg =
        preferences.getString('ProfileImage', defaultValue: "");
    Preference<String> proKey =
        preferences.getString('ProviderKey', defaultValue: "");
    Preference<String> dateofBirth =
        preferences.getString('DateofBirth', defaultValue: "");
    Preference<String> phoneNumber =
        preferences.getString('PhoneNumber', defaultValue: "");

    Preference<String> userGender =
        preferences.getString('Gender', defaultValue: "");
    Preference<String> userOccupation =
        preferences.getString('Occupation', defaultValue: "");
    Preference<String> district =
        preferences.getString('District', defaultValue: "");
    Preference<String> userEducationalQualification =
        preferences.getString('EducationalQualification', defaultValue: "");
    Preference<String> userSubject =
        preferences.getString('Subject', defaultValue: "");
    Preference<String> userLastInstitutionStudied =
        preferences.getString('LastInstitutionStudied', defaultValue: "");
    Preference<String> course =
        preferences.getString('Course', defaultValue: "");
    Preference<String> entranceType =
        preferences.getString('EntranceType', defaultValue: "");
    Preference<bool> isDark =
        preferences.getBool('isDark', defaultValue: false);

    setState(() {
      username.listen((value) {
        firstNameTextCntlr.text = value;
      });
      proKey.listen((value) {
        providerKey = value;
      });
      dateofBirth.listen((value) {
        dobTextCntlr.text = value;
      });
      phoneNumber.listen((value) {
        contactNumTextCntlr.text = value;
      });
      profileImg.listen((value) {
        profileImage = value;
      });
      useremail.listen((value) {
        emailTextCntlr.text = value;
      });

      userOccupation.listen((value) {
        occupationTextCntlr.text = value;
      });
      userEducationalQualification.listen((value) {
        educationQualificationTextCntlr.text = value;
      });
      userSubject.listen((value) {
        subjectTextCntlr.text = value;
      });
      userLastInstitutionStudied.listen((value) {
        lastInstitutionTextCntlr.text = value;
      });
      district.listen((value) {
        if (value != "") {
          _selectedDistrict = value;
        }
      });
      userGender.listen((value) {
        if (value != "") {
          _selectedGender = value;
        }
      });
      course.listen((value) {
        if (value != "") {
          _selectedCourse = value;
        }
      });
      entranceType.listen((value) {
        if (value != "") {
          _selectedEntranceType = value;
        }
      });
      isDark.listen((value) {
        isDarkMode = value;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getSharedDataPref();
    widget.editScreenType == "Settings"
        ? setState(() {
            showAll = false;
          })
        : setState(() {
            showAll = true;
          });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(),
      bottomNavigationBar: MyBottomNavBar(),
      drawer: MyNavDrawer(),
      body: isPageLoading
          ? Container(
              child: Center(child: CircularProgressIndicator()),
            )
          : connectionChk != null
              ? Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      NoInternetWidget(),
                      ElevatedButton(
                        onPressed: () {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    super.widget),
                          );
                        },
                        child: Text("Retry"),
                      ),
                    ],
                  ),
                )
              : SingleChildScrollView(
                  child: SafeArea(
                    child: Container(
                      // color: Theme.of(context).primaryColor,
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(18.0),
                            decoration: BoxDecoration(
                              // color: Colors.white,
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(30.0),
                                bottomRight: Radius.circular(30.0),
                              ),
                            ),
                            child: Column(
                              children: <Widget>[
                                // MyLogoWidget(),
                                // SizedBox(
                                //   height: 15.0,
                                // ),
                                Text(
                                  widget.editScreenType == "Settings"
                                      ? "Update Profile Settings"
                                      : "Update Profile Details",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20.0,
                                    color: isDarkMode
                                        ? Colors.white
                                        : Theme.of(context).primaryColor,
                                  ),
                                ),
                                SizedBox(
                                  height: 30.0,
                                ),
                                Container(
                                  child: Column(
                                    children: [
                                      Container(
                                        height: 110.0,
                                        width: 110.0,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                          image: DecorationImage(
                                            // image: AssetImage("assets/images/8.jpg"),
                                            image: profileImage != null
                                                ? NetworkImage(profileImage)
                                                : AssetImage(
                                                    "assets/icons/ic10.png"),
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 10.0),
                                        child: TextButton(
                                          onPressed:
                                              showImgPickerOptionsBottomSheet,
                                          child: Text(
                                            "Edit Photo",
                                            style: TextStyle(
                                              color: isDarkMode
                                                  ? Colors.white
                                                  : Theme.of(context)
                                                      .primaryColor,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 15.0,
                                ),
                                showAll
                                    ? Container(
                                        margin: EdgeInsets.only(bottom: 15.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'First Name',
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                color: HexColor("#494949"),
                                                fontWeight: FontWeight.w500,
                                                fontSize: 16.0,
                                              ),
                                            ),
                                            TextField(
                                              controller: firstNameTextCntlr,
                                              // autofocus: true,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1,
                                              decoration: InputDecoration(
                                                hintText:
                                                    'Enter your first name',
                                                hintStyle: TextStyle(
                                                  color: Theme.of(context)
                                                      .primaryColorLight,
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                                // labelText: 'First Name',
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    : Container(),
                                showAll
                                    ? Container(
                                        margin: EdgeInsets.only(bottom: 15.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Last Name',
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                color: HexColor("#494949"),
                                                fontWeight: FontWeight.w500,
                                                fontSize: 16.0,
                                              ),
                                            ),
                                            TextField(
                                              controller: lastNameTextCntlr,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1,
                                              decoration: InputDecoration(
                                                hintText:
                                                    'Enter your last name',
                                                hintStyle: TextStyle(
                                                  color: Theme.of(context)
                                                      .primaryColorLight,
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                                // labelText: 'Last Name',
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    : Container(),
                                showAll
                                    ? Container(
                                        margin: EdgeInsets.only(bottom: 15.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Date of Birth',
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                color: HexColor("#494949"),
                                                fontWeight: FontWeight.w500,
                                                fontSize: 16.0,
                                              ),
                                            ),
                                            TextField(
                                              keyboardType:
                                                  TextInputType.datetime,
                                              controller: dobTextCntlr,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1,
                                              decoration: InputDecoration(
                                                hintText:
                                                    'Enter your date of birth',
                                                hintStyle: TextStyle(
                                                  color: Theme.of(context)
                                                      .primaryColorLight,
                                                  fontSize: 12.0,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                                // labelText: 'Date of Birth',
                                              ),
                                              onTap: () {
                                                _selectDate(context);
                                              },
                                            ),
                                          ],
                                        ),
                                      )
                                    : Container(),
                                Container(
                                  margin: EdgeInsets.only(bottom: 15.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Email',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          color: HexColor("#494949"),
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16.0,
                                        ),
                                      ),
                                      TextField(
                                        keyboardType:
                                            TextInputType.emailAddress,
                                        controller: emailTextCntlr,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                        decoration: InputDecoration(
                                          hintText: 'Enter your email',
                                          hintStyle: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorLight,
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.normal,
                                          ),
                                          // labelText: 'Email',
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(bottom: 15.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Contact Number',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          color: HexColor("#494949"),
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16.0,
                                        ),
                                      ),
                                      TextField(
                                        keyboardType: TextInputType.phone,
                                        controller: contactNumTextCntlr,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                        decoration: InputDecoration(
                                          hintText: 'Enter your contact number',
                                          hintStyle: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorLight,
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.normal,
                                          ),
                                          // labelText: 'Contact Number',
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                showAll
                                    ? Container(
                                        margin: EdgeInsets.only(bottom: 15.0),
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Gender',
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                color: HexColor("#494949"),
                                                fontWeight: FontWeight.w500,
                                                fontSize: 16.0,
                                              ),
                                            ),
                                            DropdownButton<String>(
                                              isExpanded: true,
                                              value: _selectedGender,
                                              items: _genders.map((String val) {
                                                return new DropdownMenuItem<
                                                    String>(
                                                  value: val,
                                                  child: new Text(
                                                    val,
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText1,
                                                  ),
                                                );
                                              }).toList(),
                                              hint: Text(
                                                "Select Gender",
                                                style: TextStyle(
                                                  color: Theme.of(context)
                                                      .primaryColorLight,
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                              ),
                                              onChanged: (newVal) {
                                                setState(() {
                                                  _selectedGender = newVal;
                                                });
                                                print(
                                                    "Gender: $_selectedGender");
                                              },
                                            ),
                                          ],
                                        ),
                                      )
                                    : Container(),
                                showAll
                                    ? Container(
                                        margin: EdgeInsets.only(bottom: 15.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Occupation',
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                color: HexColor("#494949"),
                                                fontWeight: FontWeight.w500,
                                                fontSize: 16.0,
                                              ),
                                            ),
                                            TextField(
                                              keyboardType: TextInputType.text,
                                              controller: occupationTextCntlr,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1,
                                              decoration: InputDecoration(
                                                hintText:
                                                    'Enter your Occupation',
                                                hintStyle: TextStyle(
                                                  color: Theme.of(context)
                                                      .primaryColorLight,
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                                // labelText: 'Contact Number',
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    : Container(),
                                showAll
                                    ? Container(
                                        margin: EdgeInsets.only(bottom: 15.0),
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'District',
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                color: HexColor("#494949"),
                                                fontWeight: FontWeight.w500,
                                                fontSize: 16.0,
                                              ),
                                            ),
                                            DropdownButton<String>(
                                              isExpanded: true,
                                              value: _selectedDistrict,
                                              items:
                                                  _district.map((String val) {
                                                return new DropdownMenuItem<
                                                    String>(
                                                  value: val,
                                                  child: new Text(
                                                    val,
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText1,
                                                  ),
                                                );
                                              }).toList(),
                                              hint: Text(
                                                "Select Your District",
                                                style: TextStyle(
                                                  color: Theme.of(context)
                                                      .primaryColorLight,
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                              ),
                                              onChanged: (newVal) {
                                                setState(() {
                                                  _selectedDistrict = newVal;
                                                });
                                                print(
                                                    "District: $_selectedDistrict");
                                              },
                                            ),
                                          ],
                                        ),
                                      )
                                    : Container(),
                                showAll
                                    ? Container(
                                        margin: EdgeInsets.only(bottom: 15.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Education Qualification',
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                color: HexColor("#494949"),
                                                fontWeight: FontWeight.w500,
                                                fontSize: 16.0,
                                              ),
                                            ),
                                            TextField(
                                              keyboardType: TextInputType.text,
                                              controller:
                                                  educationQualificationTextCntlr,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1,
                                              decoration: InputDecoration(
                                                hintText:
                                                    'Enter your Education Qualification',
                                                hintStyle: TextStyle(
                                                  color: Theme.of(context)
                                                      .primaryColorLight,
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                                // labelText: 'Contact Number',
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    : Container(),
                                showAll
                                    ? Container(
                                        margin: EdgeInsets.only(bottom: 15.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Subject',
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                color: HexColor("#494949"),
                                                fontWeight: FontWeight.w500,
                                                fontSize: 16.0,
                                              ),
                                            ),
                                            TextField(
                                              keyboardType: TextInputType.text,
                                              controller: subjectTextCntlr,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1,
                                              decoration: InputDecoration(
                                                hintText: 'Enter your Subject',
                                                hintStyle: TextStyle(
                                                  color: Theme.of(context)
                                                      .primaryColorLight,
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                                // labelText: 'Contact Number',
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    : Container(),
                                showAll
                                    ? Container(
                                        margin: EdgeInsets.only(bottom: 15.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Last Institution Studied',
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                color: HexColor("#494949"),
                                                fontWeight: FontWeight.w500,
                                                fontSize: 16.0,
                                              ),
                                            ),
                                            TextField(
                                              keyboardType: TextInputType.text,
                                              controller:
                                                  lastInstitutionTextCntlr,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1,
                                              decoration: InputDecoration(
                                                hintText:
                                                    'Enter your Last Institution',
                                                hintStyle: TextStyle(
                                                  color: Theme.of(context)
                                                      .primaryColorLight,
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                                // labelText: 'Contact Number',
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    : Container(),
                                showAll
                                    ? Container(
                                        margin: EdgeInsets.only(bottom: 15.0),
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Course',
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                color: HexColor("#494949"),
                                                fontWeight: FontWeight.w500,
                                                fontSize: 16.0,
                                              ),
                                            ),
                                            DropdownButton<String>(
                                              isExpanded: true,
                                              value: _selectedCourse,
                                              items: _courses.map((String val) {
                                                return new DropdownMenuItem<
                                                    String>(
                                                  value: val,
                                                  child: new Text(
                                                    val,
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText1,
                                                  ),
                                                );
                                              }).toList(),
                                              hint: Text(
                                                "Select Your LLB Course",
                                                style: TextStyle(
                                                  color: Theme.of(context)
                                                      .primaryColorLight,
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                              ),
                                              onChanged: (newVal) {
                                                setState(() {
                                                  _selectedCourse = newVal;
                                                });
                                                print(
                                                    "Course: $_selectedCourse");
                                              },
                                            ),
                                          ],
                                        ),
                                      )
                                    : Container(),
                                showAll
                                    ? Container(
                                        margin: EdgeInsets.only(bottom: 15.0),
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Entrance Type',
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                color: HexColor("#494949"),
                                                fontWeight: FontWeight.w500,
                                                fontSize: 16.0,
                                              ),
                                            ),
                                            DropdownButton<String>(
                                              isExpanded: true,
                                              value: _selectedEntranceType,
                                              items: _entranceTypes
                                                  .map((String val) {
                                                return new DropdownMenuItem<
                                                    String>(
                                                  value: val,
                                                  child: new Text(
                                                    val,
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText1,
                                                  ),
                                                );
                                              }).toList(),
                                              hint: Text(
                                                "Select Your LLB Course",
                                                style: TextStyle(
                                                  color: Theme.of(context)
                                                      .primaryColorLight,
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                              ),
                                              onChanged: (newVal) {
                                                setState(() {
                                                  _selectedEntranceType =
                                                      newVal;
                                                });
                                                print(
                                                    "Entrance Type: $_selectedEntranceType");
                                              },
                                            ),
                                          ],
                                        ),
                                      )
                                    : Container(),
                                widget.editScreenType == "Settings"
                                    ? Container(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Password',
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                color: HexColor("#494949"),
                                                fontWeight: FontWeight.w500,
                                                fontSize: 16.0,
                                              ),
                                            ),
                                            TextField(
                                              controller: passwordTextCntlr,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1,
                                              decoration: InputDecoration(
                                                hintText: 'Enter your password',
                                                hintStyle: TextStyle(
                                                  color: Theme.of(context)
                                                      .primaryColorLight,
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                                // labelText: 'Password',
                                              ),
                                              obscureText: true,
                                              obscuringCharacter: "*",
                                            ),
                                          ],
                                        ),
                                      )
                                    : Container(),
                                SizedBox(
                                  height: 20.0,
                                ),
                                ElevatedButton(
                                  onPressed: () {
                                    updateUserProfile();
                                    // Navigator.push(
                                    //   // Navigator.pushReplacement(
                                    //   context,
                                    //   MaterialPageRoute(
                                    //     builder: (context) => LoginScreen(),
                                    //   ),
                                    // );
                                  },
                                  child: Text(
                                    "Update",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          // Container(
                          //   padding: EdgeInsets.all(10.0),
                          //   height: 50.0,
                          //   // child: Container(
                          //   //   child: Image.asset(
                          //   //     "assets/logo/white png.png",
                          //   //     fit: BoxFit.fill,
                          //   //     height: 50.0,
                          //   //   ),
                          //   // ),
                          // ),
                        ],
                      ),
                    ),
                  ),
                ),
    );
  }
}

class DesktopEditProfileScreen extends StatefulWidget {
  @override
  _DesktopEditProfileScreenState createState() =>
      _DesktopEditProfileScreenState();
}

class _DesktopEditProfileScreenState extends State<DesktopEditProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
