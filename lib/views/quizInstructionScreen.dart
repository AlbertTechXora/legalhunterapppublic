import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:another_flushbar/flushbar.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import '../helpers/appFunctions.dart';
import '../services/connections.dart' as Constants;

import '../widgets/myAppBar.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myNavBar.dart';
import '../widgets/noInternetWidget.dart';

import './examOverviewScreen.dart';
import './quizExamScreen.dart';

class QuizInstructionSreen extends StatefulWidget {
  final int examId;
  final int userSelectedParentCatId;
  final List userSelectedSubCatList;
  QuizInstructionSreen({
    @required this.examId,
    this.userSelectedParentCatId,
    this.userSelectedSubCatList,
  });
  @override
  _QuizInstructionSreenState createState() => _QuizInstructionSreenState();
}

class _QuizInstructionSreenState extends State<QuizInstructionSreen> {
  var apiRequestStatus;
  var apiErrMessage;
  var connectionChk;
  bool isPageLoading = false;
  var examDetail;
  var questionList;
  var timeDuration;
  var durationOfExam;
  var firstExamDetailFromHistoryMap;
  bool isDarkMode = false;

  getExamDetails() async {
    setState(() {
      isPageLoading = true;
    });
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      //multipart/form-data POST Method
      Map<String, String> requestBody = <String, String>{
        "ExamID": widget.examId.toString(),
      };
      // Map<String, String> headers = <String, String>{
      //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
      // };
      var uri = Uri.parse(Constants.getExamDetails);
      var request = http.MultipartRequest('POST', uri)
        // ..headers.addAll(
        //     headers) //if u have headers, basic auth, token bearer... Else remove line
        ..fields.addAll(requestBody);
      var response = await request.send();
      final respStr = await response.stream.bytesToString();
      //

      print("response.request:${response.request}");
      if (response.statusCode == 200) {
        var data = json.decode(respStr);
        // print("data: $data");
        if (data != null) {
          apiRequestStatus = data['Status'];
          print("apiRequestStatus: $apiRequestStatus");
          if (apiRequestStatus == "Success") {
            // print("Yes");
            examDetail = data['ExamDetail'];
            questionList = data['QuestionList'];
            durationOfExam = examDetail['ExamDuration'];
            var examDuration = examDetail['ExamDuration'].split(":");
            timeDuration = "${examDuration[0]}h ${examDuration[1]}m";

            var examHistoryDetails = data['ExamHistoryDetails'];
            if (examHistoryDetails != null) {
              for (var i = 0; i < 1; i++) {
                firstExamDetailFromHistoryMap = {
                  "Errors": examHistoryDetails[i]['Errors'],
                  "ProviderKey": examHistoryDetails[i]['ProviderKey'],
                  "IsProviderKeyValid": examHistoryDetails[i]
                      ['IsProviderKeyValid'],
                  "ChildCategoryID": examHistoryDetails[i]['ChildCategoryID'],
                  "ParentCategoryID": examHistoryDetails[i]['ParentCategoryID'],
                  "ApplicationUserExamHistoryID": examHistoryDetails[i]
                      ['ApplicationUserExamHistoryID'],
                  "ExamID": examHistoryDetails[i]['ExamID'],
                  "ExamName": examHistoryDetails[i]['ExamName'],
                  "ExamNo": examHistoryDetails[i]['ExamNo'],
                  "NegativePointsForWrongAnswer": examHistoryDetails[i]
                      ['NegativePointsForWrongAnswer'],
                  "PositivePointsForCorrectAnswer": examHistoryDetails[i]
                      ['PositivePointsForCorrectAnswer'],
                  "TotalQuestions": examHistoryDetails[i]['TotalQuestions'],
                  "TotalQuestionsAttempted": examHistoryDetails[i]
                      ['TotalQuestionsAttempted'],
                  "TotalCorrectAnswers": examHistoryDetails[i]
                      ['TotalCorrectAnswers'],
                  "TotalWrongAnswers": examHistoryDetails[i]
                      ['TotalWrongAnswers'],
                  "TotalNegativeScore": examHistoryDetails[i]
                      ['TotalNegativeScore'],
                  "TotalUnAttemptedNegativeScore": examHistoryDetails[i]
                      ['TotalUnAttemptedNegativeScore'],
                  "TotalPositiveScore": examHistoryDetails[i]
                      ['TotalPositiveScore'],
                  "TotalScore": examHistoryDetails[i]['TotalScore'],
                  "TotalAdminGivenExamScore": examHistoryDetails[i]
                      ['TotalAdminGivenExamScore'],
                  "RandomExamNo": examHistoryDetails[i]['RandomExamNo'],
                  "ExamRank": examHistoryDetails[i]['ExamRank'],
                  "Time": examHistoryDetails[i]['Time'],
                  "Status": examHistoryDetails[i]['Status'],
                  "ExamAttemptedOnString": examHistoryDetails[i]
                      ['ExamAttemptedOnString'],
                  "ExamAttemptedBy": examHistoryDetails[i]['ExamAttemptedBy'],
                  "FullName": examHistoryDetails[i]['FullName'],
                  "QuestionList": examHistoryDetails[i]['QuestionList'],
                  "ExamRankList": examHistoryDetails[i]['ExamRankList'],
                  "LastAttemptsForCurrentExam": examHistoryDetails[i]
                      ['LastAttemptsForCurrentExam'],
                  "TotalTimeTaken": examHistoryDetails[i]['TotalTimeTaken'],
                  "TotalTimeTakenFormatted": examHistoryDetails[i]
                      ['TotalTimeTakenFormatted'],
                };
              }
            }
            // print("examDetail: $examDetail");
            // print("questionList: $questionList");
          } else {
            apiErrMessage = data['Errors'][0];
            Flushbar(
              message: apiErrMessage,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          apiRequestStatus = false;
          apiErrMessage = "No data found.";
          Flushbar(
            message: apiErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        apiRequestStatus = false;
        apiErrMessage = "Error: ${response.statusCode}.";
        Flushbar(
          message: apiErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      apiRequestStatus = false;
      apiErrMessage = connectionChk;
      Flushbar(
        message: apiErrMessage,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
  }

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<bool> isDark =
        preferences.getBool('isDark', defaultValue: false);
    isDark.listen((value) {
      setState(() {
        isDarkMode = value;
      });
    });
    // print("isDarkMode: $isDarkMode");
  }

  @override
  void initState() {
    super.initState();
    getSharedDataPref();
    getExamDetails();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(),
      bottomNavigationBar: MyBottomNavBar(),
      drawer: MyNavDrawer(),
      body: isPageLoading
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : connectionChk != null
              ? Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      NoInternetWidget(),
                      ElevatedButton(
                        onPressed: () {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    super.widget),
                          );
                        },
                        child: Text("Retry"),
                      ),
                    ],
                  ),
                )
              : Container(
                  child: ListView(
                    children: [
                      SizedBox(
                        height: 15.0,
                      ),
                      Container(
                        child: Column(
                          children: [
                            Card(
                              margin: EdgeInsets.only(bottom: 15.0),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(25),
                              ),
                              elevation: 5.0,
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.90,
                                height: 50.0,
                                child: Center(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(left: 10.0),
                                        child: Text(
                                          "Total Questions",
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline1,
                                        ),
                                      ),
                                      Container(
                                        width: 130.0,
                                        height: 50.0,
                                        decoration: BoxDecoration(
                                          color: isDarkMode
                                              ? Colors.blueGrey[900]
                                              : Theme.of(context).primaryColor,
                                          borderRadius:
                                              BorderRadius.circular(25),
                                        ),
                                        child: Center(
                                          child: Text(
                                            questionList.length.toString(),
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle1,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Card(
                              margin: EdgeInsets.only(bottom: 15.0),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(25),
                              ),
                              elevation: 5.0,
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.90,
                                height: 50.0,
                                child: Center(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(left: 10.0),
                                        child: Text(
                                          "Time Duration",
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline1,
                                        ),
                                      ),
                                      Container(
                                        width: 130.0,
                                        height: 50.0,
                                        decoration: BoxDecoration(
                                          color: isDarkMode
                                              ? Colors.blueGrey[900]
                                              : Theme.of(context).primaryColor,
                                          borderRadius:
                                              BorderRadius.circular(25),
                                        ),
                                        child: Center(
                                          child: Text(
                                            timeDuration,
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle1,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Card(
                              margin: EdgeInsets.only(bottom: 15.0),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(25),
                              ),
                              elevation: 5.0,
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.90,
                                height: 50.0,
                                child: Center(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(left: 10.0),
                                        child: Text(
                                          "Positive Marks",
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline1,
                                        ),
                                      ),
                                      Container(
                                        width: 130.0,
                                        height: 50.0,
                                        decoration: BoxDecoration(
                                          color: isDarkMode
                                              ? Colors.blueGrey[900]
                                              : Theme.of(context).primaryColor,
                                          borderRadius:
                                              BorderRadius.circular(25),
                                        ),
                                        child: Center(
                                          child: Text(
                                            examDetail[
                                                    'PositivePointsForCorrectAnswer']
                                                .toString(),
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle1,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Card(
                              margin: EdgeInsets.only(bottom: 15.0),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(25),
                              ),
                              elevation: 5.0,
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.90,
                                height: 50.0,
                                child: Center(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(left: 10.0),
                                        child: Text(
                                          "Negative Marks",
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline1,
                                        ),
                                      ),
                                      Container(
                                        width: 130.0,
                                        height: 50.0,
                                        decoration: BoxDecoration(
                                          color: isDarkMode
                                              ? Colors.blueGrey[900]
                                              : Theme.of(context).primaryColor,
                                          borderRadius:
                                              BorderRadius.circular(25),
                                        ),
                                        child: Center(
                                          child: Text(
                                            examDetail[
                                                    'NegativePointsForWrongAnswer']
                                                .toString(),
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle1,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(20.0),
                        child: Column(
                          children: [
                            Text(
                              "Instructions",
                              style: Theme.of(context).textTheme.headline6,
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(
                              width: 150.0,
                              child: Divider(
                                thickness: 1,
                              ),
                            ),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "\n${examDetail['ExamDesc']}",
                                style: Theme.of(context).textTheme.bodyText1,
                                overflow: TextOverflow.visible,
                              ),
                            ),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "\n${examDetail['ExamInstructions']}",
                                style: Theme.of(context).textTheme.bodyText1,
                                overflow: TextOverflow.visible,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(
                              width: 10.0,
                            ),
                            ElevatedButton(
                              onPressed: () {
                                Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(
                                    builder: (context) => QuizExamScreen(
                                      questionAndAnsList: questionList,
                                      examId: widget.examId,
                                      durationOfExam: durationOfExam,
                                      userSelectedParentCatId:
                                          widget.userSelectedParentCatId,
                                      userSelectedSubCatList:
                                          widget.userSelectedSubCatList,
                                    ),
                                  ),
                                );
                              },
                              child: Text(
                                "Start Exam",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                  Theme.of(context).primaryColorLight,
                                ),
                              ),
                              // color: Theme.of(context).primaryColorLight,
                            ),
                            ElevatedButton(
                              onPressed: () {
                                print("firstExamDetailFromHistoryMap");
                                firstExamDetailFromHistoryMap != null
                                    ? Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              ExamOverviewScreen(
                                            overviewType: "Previous",
                                            userSelectedParentCatId:
                                                widget.userSelectedParentCatId,
                                            userSelectedSubCatList:
                                                widget.userSelectedSubCatList,
                                            currExamDetailData:
                                                firstExamDetailFromHistoryMap,
                                          ),
                                        ),
                                      )
                                    : Flushbar(
                                        message:
                                            "Sorry.!! Option not available right now.",
                                        duration: Duration(seconds: 2),
                                        backgroundColor: Colors.red,
                                      ).show(context);
                              },
                              child: Text(
                                "Previous Results",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                    ],
                  ),
                ),
    );
  }
}
