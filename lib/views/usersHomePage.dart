import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';
// import 'package:hexcolor/hexcolor.dart';
// import 'package:flutter_color/flutter_color.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:another_flushbar/flushbar.dart';

import '../helpers/appFunctions.dart';
import '../services/connections.dart' as Constants;

import '../widgets/myAppBar.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myNavBar.dart';
import '../widgets/top_carousel.dart';
import '../widgets/loadingContainers.dart';
import '../widgets/subscriptionLockWidget.dart';
import '../widgets/showCustomDialog.dart';
import '../widgets/noInternetWidget.dart';

import './tryAnotherExamScreen.dart';
import './feedsOrNotificationsScreen.dart';
import './trainingVideosScreen.dart';
import './subscriptionScreen.dart';
import './quizInstructionScreen.dart';
import './feedsDetailsScreen.dart';

class UsersHomePage extends StatefulWidget {
  @override
  _UsersHomePageState createState() => _UsersHomePageState();
}

class _UsersHomePageState extends State<UsersHomePage> {
  bool isDarkMode = false;
  var apiRequestStatus;
  var apiErrMessage;
  var connectionChk;
  bool isPageLoading = false;
  List examCategoryList = [];
  List examSubCategories = [];
  List scheduledCategoriesList = [];
  List latestFeeds = [];
  List centreBannerList = [];
  var providerKey;

  List<Map<String, String>> activities = [
    {
      "tileText": "Training Videos",
      "image": "assets/images/newDashImages10.png",
      "page": "Training Videos",
    },
    {
      "tileText": "All Feeds",
      "image": "assets/images/newDashImages9.png",
      "page": "Feeds",
    },
    {
      "tileText": "Subscription Plans",
      "image": "assets/images/A4.png",
      "page": "Subscription",
    },
  ];

  getExamCategories() async {
    setState(() {
      isPageLoading = true;
    });
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      //multipart/form-data POST Method
      Map<String, String> requestBody = <String, String>{
        "ProviderKey": providerKey,
      };
      // Map<String, String> headers = <String, String>{
      //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
      // };
      var uri = Uri.parse(Constants.getCategoriesList);
      var request = http.MultipartRequest('POST', uri)
        // ..headers.addAll(
        //     headers) //if u have headers, basic auth, token bearer... Else remove line
        ..fields.addAll(requestBody);
      var response = await request.send();
      final respStr = await response.stream.bytesToString();
      //

      print("response.request:${response.request}");
      if (response.statusCode == 200) {
        var data = json.decode(respStr);
        // print("data: $data");
        if (data != null) {
          apiRequestStatus = data['Status'];
          print("apiRequestStatus: $apiRequestStatus");
          if (apiRequestStatus == "Success") {
            // print("Yes");
            var parentCategoryList = data['ParentCategoryList'];
            var scheduledCategoryTimeList = data['ScheduledCategoryTimeList'];
            if (parentCategoryList.length > 0 ||
                scheduledCategoryTimeList.length > 0) {
              for (var i = 0; i < parentCategoryList.length; i++) {
                var parentCatId = parentCategoryList[i]['CategoryID'];
                var parentCategoryName = parentCategoryList[i]['CategoryName'];
                var parentCategoryImageUrl =
                    parentCategoryList[i]['CategoryImageUrl'];
                var isAllowedInSubscription =
                    parentCategoryList[i]['IsAllowedInSubscription'];
                var isScheduledCategory =
                    parentCategoryList[i]['IsScheduledCategory'];
                if (scheduledCategoryTimeList.length > 0 &&
                    isScheduledCategory == true) {
                  for (var j = 0; j < scheduledCategoryTimeList.length; j++) {
                    var scheduledCatsId =
                        scheduledCategoryTimeList[j]['CategoryID'];
                    var scheduledExamId =
                        scheduledCategoryTimeList[j]['ExamID'];
                    var scheduledExamStartWithin =
                        scheduledCategoryTimeList[j]['ExamStartWithin'];
                    var scheduledExamStartWithinSplit =
                        scheduledExamStartWithin != "Exam Going On" &&
                                scheduledExamStartWithin != null
                            ? scheduledExamStartWithin.split(":")
                            : null;
                    var examTimeMins = scheduledExamStartWithinSplit != null
                        ? (int.parse(scheduledExamStartWithinSplit[0]) * 60) +
                            int.parse(scheduledExamStartWithinSplit[1])
                        : 0;
                    print("examTimeMins: $examTimeMins");
                    // print(
                    //     "scheduledCatsId: $scheduledCatsId == parentCatId: $parentCatId");
                    if (scheduledCatsId == parentCatId) {
                      var tempParentCatMap = {
                        "CategoryID": parentCatId,
                        "CategoryName": parentCategoryName,
                        "CategoryImageUrl": parentCategoryImageUrl,
                        "ExamID": scheduledExamId,
                        "ExamStartWithin": scheduledExamStartWithin,
                        "ExamTimeMins": examTimeMins,
                        "IsAllowedInSubscription": isAllowedInSubscription,
                      };
                      examCategoryList.add(tempParentCatMap);
                      break;
                    }
                    // else if (j == (scheduledCategoryTimeList.length - 1)) {
                    //   var tempParentCatMap = {
                    //     "CategoryID": parentCatId,
                    //     "CategoryName": parentCategoryName,
                    //     "CategoryImageUrl": parentCategoryImageUrl,
                    //     "ExamID": null,
                    //     "ExamStartWithin": null,
                    //     "ExamTimeMins": null,
                    //     "IsAllowedInSubscription": isAllowedInSubscription,
                    //   };
                    //   examCategoryList.add(tempParentCatMap);
                    // }
                  }
                } else {
                  print("Scheduled Categories Empty.");
                  var tempParentCatMap = {
                    "CategoryID": parentCatId,
                    "CategoryName": parentCategoryName,
                    "CategoryImageUrl": parentCategoryImageUrl,
                    "ExamID": null,
                    "ExamStartWithin": null,
                    "ExamTimeMins": null,
                    "IsAllowedInSubscription": isAllowedInSubscription,
                  };
                  examCategoryList.add(tempParentCatMap);
                }
              }
              if (scheduledCategoryTimeList.length > 0) {
                print(
                    "scheduledCategoryTimeList.length: ${scheduledCategoryTimeList.length}");
                for (var k = 0; k < scheduledCategoryTimeList.length; k++) {
                  var examStartWithInBanner = scheduledCategoryTimeList[k]
                                  ['ExamStartWithin'] !=
                              "Exam Going On" &&
                          scheduledCategoryTimeList[k]['ExamStartWithin'] !=
                              null
                      ? scheduledCategoryTimeList[k]['ExamStartWithin']
                          .split(":")
                      : scheduledCategoryTimeList[k]['ExamStartWithin'];
                  var tempScheduledCatMap = {
                    "bannerExamId": examStartWithInBanner == "Exam Going On"
                        ? scheduledCategoryTimeList[k]['ExamID']
                        : null,
                    "leadingImage":
                        scheduledCategoryTimeList[k]['IsAdvertisement'] != null
                            ? scheduledCategoryTimeList[k]
                                ['AdvertisementImageUrl']
                            : scheduledCategoryTimeList[k]['CategoryImageUrl'],
                    "titleText":
                        scheduledCategoryTimeList[k]['IsAdvertisement'] != null
                            ? scheduledCategoryTimeList[k]['AdvertisementTitle']
                            : scheduledCategoryTimeList[k]['CategoryName'],
                    "time":
                        scheduledCategoryTimeList[k]['IsAdvertisement'] != null
                            ? null
                            : examStartWithInBanner,
                    "trailerImage": "assets/images/newDashImages11.png",
                    "AdvertisementLink": scheduledCategoryTimeList[k]
                        ['AdvertisementLink'],
                    "IsAdvertisement": scheduledCategoryTimeList[k]
                        ['IsAdvertisement'],
                  };
                  centreBannerList.add(tempScheduledCatMap);
                }
              }
            }
            setState(() {
              // examCategoryList = data['ParentCategoryList'];
              // scheduledCategoriesList = scheduledCategoryTimeList;
              examSubCategories = data['ChildCategoryList'];
            });
            if (examCategoryList.isEmpty) {
              apiErrMessage = "No Exam Categories Found.";
              Flushbar(
                message: apiErrMessage,
                duration: Duration(seconds: 2),
                backgroundColor: Colors.red,
              ).show(context);
            }
            if (examSubCategories.isEmpty) {
              apiErrMessage = "No Exam SubCategories Found.";
              Flushbar(
                message: apiErrMessage,
                duration: Duration(seconds: 2),
                backgroundColor: Colors.red,
              ).show(context);
            }
          } else {
            apiErrMessage = data['Errors'][0];
            Flushbar(
              message: apiErrMessage,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          apiRequestStatus = false;
          apiErrMessage = "No data found.";
          Flushbar(
            message: apiErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        apiRequestStatus = false;
        apiErrMessage = "Error: ${response.statusCode}.";
        Flushbar(
          message: apiErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      apiRequestStatus = false;
      apiErrMessage = connectionChk;
      Flushbar(
        message: apiErrMessage,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
    print("examCategoryList: $examCategoryList");
    // print("examSubCategories: $examSubCategories");
    // print("centreBannerList: $centreBannerList");
  }

  getLatestFeeds() async {
    setState(() {
      isPageLoading = true;
    });
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      //multipart/form-data POST Method
      // Map<String, String> requestBody = <String, String>{
      //   "Username": username,
      //   "Password": password,
      // };
      // Map<String, String> headers = <String, String>{
      //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
      // };
      var uri = Uri.parse(Constants.getLatestFeeds);
      var request = http.MultipartRequest('POST', uri);
      // ..headers.addAll(
      //     headers) //if u have headers, basic auth, token bearer... Else remove line
      // ..fields.addAll(requestBody);
      var response = await request.send();
      final respStr = await response.stream.bytesToString();
      //

      print("response.request:${response.request}");
      if (response.statusCode == 200) {
        var data = json.decode(respStr);
        // print("data: $data");
        if (data != null) {
          apiRequestStatus = data['Status'];
          print("apiRequestStatus: $apiRequestStatus");
          if (apiRequestStatus == "Success") {
            // print("Yes");
            if (data['FeedList'].isEmpty) {
              apiErrMessage = "No New Feeds.";
              Flushbar(
                message: apiErrMessage,
                duration: Duration(seconds: 2),
                backgroundColor: Colors.red,
              ).show(context);
            } else {
              for (var i = 0; i < data['FeedList'].length; i++) {
                var tempFeedImageList = {
                  "FeedID": data['FeedList'][i]['FeedID'],
                  "FeedImageUrl": data['FeedList'][i]['FeedImageUrl'],
                  "FeedTitle": data['FeedList'][i]['FeedTitle'],
                  "FeedDesc": data['FeedList'][i]['FeedDesc'],
                  "FeedDateFormatted": data['FeedList'][i]['FeedDateFormatted'],
                };
                latestFeeds.add(tempFeedImageList);
              }
            }
          } else {
            apiErrMessage = data['Errors'][0];
            Flushbar(
              message: apiErrMessage,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          apiRequestStatus = false;
          apiErrMessage = "No data found.";
          Flushbar(
            message: apiErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        apiRequestStatus = false;
        apiErrMessage = "Error: ${response.statusCode}.";
        Flushbar(
          message: apiErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      apiRequestStatus = false;
      apiErrMessage = connectionChk;
      Flushbar(
        message: apiErrMessage,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
  }

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<bool> isDark =
        preferences.getBool('isDark', defaultValue: false);
    isDark.listen((value) {
      setState(() {
        isDarkMode = value;
      });
    });
    Preference<String> proKey =
        preferences.getString('ProviderKey', defaultValue: "");
    proKey.listen((value) {
      setState(() {
        providerKey = value;
      });
    });
    // print("isDarkMode: $isDarkMode");
  }

  Future<bool> _onWillPop() async {
    String titleText = "Confirm to Exit Legalhunter.?";
    String bottomBtnText = "Yes";
    return (await showDialog(
          context: context,
          builder: (BuildContext context) => ShowCustomDialog(
            title: titleText,
            buttonText: bottomBtnText,
          ),
        )) ??
        false;
  }

  @override
  void initState() {
    super.initState();
    getSharedDataPref();
    getLatestFeeds();
    getExamCategories();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        // backgroundColor: Theme.of(context).primaryColor,
        // backgroundColor: HexColor("#ECECEC"),
        appBar: MyAppBar(),
        bottomNavigationBar: MyBottomNavBar(),
        drawer: MyNavDrawer(),
        body: isPageLoading
            ? Container(
                color: Colors.white,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              )
            : connectionChk != null
                ? Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        NoInternetWidget(),
                        ElevatedButton(
                          onPressed: () {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      super.widget),
                            );
                          },
                          child: Text("Retry"),
                        ),
                      ],
                    ),
                  )
                : ListView(
                    children: [
                      Stack(
                        alignment: Alignment.topCenter,
                        children: [
                          Container(
                            color: Theme.of(context).primaryColor,
                            height: 180.0,
                            child: Column(
                              children: [
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 80.0,
                                  margin: EdgeInsets.only(left: 10.0),
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: latestFeeds.length,
                                    itemBuilder: (BuildContext context, index) {
                                      return GestureDetector(
                                        onTap: () {
                                          Navigator.of(context).push(
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  FeedsDetailsScreen(
                                                feedsImage: latestFeeds[index]
                                                    ['FeedImageUrl'],
                                                feedsTitle: latestFeeds[index]
                                                    ['FeedTitle'],
                                                feedsDesc: latestFeeds[index]
                                                    ['FeedDesc'],
                                                feedsTimeStamp:
                                                    latestFeeds[index]
                                                        ['FeedDateFormatted'],
                                              ),
                                            ),
                                          );
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(right: 15.0),
                                          padding: EdgeInsets.all(2.0),
                                          decoration: BoxDecoration(
                                            gradient: LinearGradient(
                                              colors: [
                                                Colors.white,
                                                Colors.white,
                                                Colors.amber,
                                                Colors.amberAccent,
                                              ],
                                              begin: Alignment.topCenter,
                                              end: Alignment.bottomCenter,
                                            ),
                                            shape: BoxShape.circle,
                                          ),
                                          child: Container(
                                            height: 65.0,
                                            width: 65.0,
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              shape: BoxShape.circle,
                                              image: DecorationImage(
                                                // image: AssetImage(
                                                //     latestFeeds[index]['image']),
                                                image: NetworkImage(
                                                    latestFeeds[index]
                                                        ['FeedImageUrl']),
                                                fit: BoxFit.contain,
                                              ),
                                              border: Border.all(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                width: 2,
                                              ),
                                            ),
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 85.0),
                            // color: HexColor("#ECECEC"),
                            child: Column(
                              children: [
                                examCategoryList.isEmpty
                                    ? LoadingContainers(loaderType: "Grid")
                                    : Container(
                                        padding: EdgeInsets.all(5.0),
                                        child: GridView.count(
                                          shrinkWrap: true,
                                          physics:
                                              NeverScrollableScrollPhysics(),
                                          scrollDirection: Axis.vertical,
                                          crossAxisCount: 2,
                                          childAspectRatio: 0.85,
                                          padding: const EdgeInsets.all(5.0),
                                          mainAxisSpacing: 10.0,
                                          crossAxisSpacing: 10.0,
                                          children: List.generate(
                                              examCategoryList.length, (index) {
                                            return Stack(
                                              children: [
                                                Positioned.fill(
                                                  bottom: 0.0,
                                                  child: Card(
                                                    shape:
                                                        RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              15),
                                                    ),
                                                    elevation: 5.0,
                                                    child: Container(
                                                      padding:
                                                          EdgeInsets.all(10.0),
                                                      decoration: BoxDecoration(
                                                        // color: examCategoryList[index]
                                                        //             [
                                                        //             'IsAllowedInSubscription'] ==
                                                        //         false
                                                        //     ? Colors.grey[400]
                                                        //     : null,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(15),
                                                      ),
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceAround,
                                                        children: [
                                                          Container(
                                                            child:
                                                                Image.network(
                                                              examCategoryList[
                                                                      index][
                                                                  'CategoryImageUrl'],
                                                              height: 80.0,
                                                              width: 80.0,
                                                            ),
                                                          ),
                                                          Container(
                                                            // margin: EdgeInsets.only(
                                                            //   top: 5.0,
                                                            //   bottom: 5.0,
                                                            // ),
                                                            child: Text(
                                                              examCategoryList[
                                                                      index][
                                                                  'CategoryName'],
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .headline3,
                                                              textAlign:
                                                                  TextAlign
                                                                      .center,
                                                            ),
                                                          ),
                                                          examCategoryList[
                                                                          index]
                                                                      [
                                                                      'ExamID'] !=
                                                                  null
                                                              ? examCategoryList[
                                                                              index]
                                                                          [
                                                                          'ExamStartWithin'] !=
                                                                      "Exam Going On"
                                                                  ? CountdownTimer(
                                                                      endTime: DateTime.now()
                                                                              .millisecondsSinceEpoch +
                                                                          1000 *
                                                                              (60 * examCategoryList[index]['ExamTimeMins']),
                                                                      widgetBuilder: (_,
                                                                          CurrentRemainingTime
                                                                              time) {
                                                                        if (time ==
                                                                            null) {
                                                                          return Text(
                                                                            'Time Out',
                                                                            style:
                                                                                TextStyle(
                                                                              color: Colors.grey,
                                                                              fontWeight: FontWeight.bold,
                                                                            ),
                                                                          );
                                                                        }
                                                                        return Row(
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.center,
                                                                          children: [
                                                                            Card(
                                                                              elevation: 5.0,
                                                                              shape: RoundedRectangleBorder(
                                                                                borderRadius: BorderRadius.circular(8.0),
                                                                              ),
                                                                              child: Container(
                                                                                height: 30.0,
                                                                                width: 30.0,
                                                                                child: Center(
                                                                                  child: Text(
                                                                                    time.hours == null ? "00" : '${time.hours}',
                                                                                    style: Theme.of(context).textTheme.bodyText1,
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                            ),
                                                                            Text(
                                                                              ":",
                                                                              style: Theme.of(context).textTheme.bodyText1,
                                                                            ),
                                                                            Card(
                                                                              elevation: 5.0,
                                                                              shape: RoundedRectangleBorder(
                                                                                borderRadius: BorderRadius.circular(8.0),
                                                                              ),
                                                                              child: Container(
                                                                                height: 30.0,
                                                                                width: 30.0,
                                                                                child: Center(
                                                                                  child: Text(
                                                                                    time.min == null ? "00" : "${time.min}",
                                                                                    style: Theme.of(context).textTheme.bodyText1,
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                            ),
                                                                            Text(
                                                                              ":",
                                                                              style: Theme.of(context).textTheme.bodyText1,
                                                                            ),
                                                                            Card(
                                                                              elevation: 5.0,
                                                                              shape: RoundedRectangleBorder(
                                                                                borderRadius: BorderRadius.circular(8.0),
                                                                              ),
                                                                              child: Container(
                                                                                height: 30.0,
                                                                                width: 30.0,
                                                                                child: Center(
                                                                                  child: Text(
                                                                                    "${time.sec}",
                                                                                    style: Theme.of(context).textTheme.bodyText1,
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        );
                                                                      },
                                                                    )
                                                                  : Container(
                                                                      child:
                                                                          Text(
                                                                        examCategoryList[index]
                                                                            [
                                                                            'ExamStartWithin'],
                                                                        style: Theme.of(context)
                                                                            .textTheme
                                                                            .bodyText1,
                                                                      ),
                                                                    )
                                                              : Container(
                                                                  height: 20.0,
                                                                  // child: Text(
                                                                  //     "Total Exams: 50",
                                                                  //     style: Theme.of(
                                                                  //             context)
                                                                  //         .textTheme
                                                                  //         .bodyText1),
                                                                  // child:
                                                                  //     CountdownTimer(
                                                                  //   endTime: DateTime
                                                                  //               .now()
                                                                  //           .millisecondsSinceEpoch +
                                                                  //       1000 *
                                                                  //           (60 * 30),
                                                                  //   widgetBuilder: (_,
                                                                  //       CurrentRemainingTime
                                                                  //           time) {
                                                                  //     if (time ==
                                                                  //         null) {
                                                                  //       return Text(
                                                                  //         'Time Out',
                                                                  //         style:
                                                                  //             TextStyle(
                                                                  //           color: Colors
                                                                  //               .grey,
                                                                  //           fontWeight:
                                                                  //               FontWeight
                                                                  //                   .bold,
                                                                  //         ),
                                                                  //       );
                                                                  //     }
                                                                  //     return Row(
                                                                  //       mainAxisAlignment:
                                                                  //           MainAxisAlignment
                                                                  //               .center,
                                                                  //       children: [
                                                                  //         Card(
                                                                  //           elevation:
                                                                  //               5.0,
                                                                  //           shape:
                                                                  //               RoundedRectangleBorder(
                                                                  //             borderRadius:
                                                                  //                 BorderRadius.circular(8.0),
                                                                  //           ),
                                                                  //           child:
                                                                  //               Container(
                                                                  //             height:
                                                                  //                 30.0,
                                                                  //             width:
                                                                  //                 30.0,
                                                                  //             child:
                                                                  //                 Center(
                                                                  //               child:
                                                                  //                   Text(
                                                                  //                 time.hours == null
                                                                  //                     ? "00"
                                                                  //                     : '${time.hours}',
                                                                  //                 style:
                                                                  //                     Theme.of(context).textTheme.bodyText1,
                                                                  //               ),
                                                                  //             ),
                                                                  //           ),
                                                                  //         ),
                                                                  //         Text(
                                                                  //           ":",
                                                                  //           style: Theme.of(
                                                                  //                   context)
                                                                  //               .textTheme
                                                                  //               .bodyText1,
                                                                  //         ),
                                                                  //         Card(
                                                                  //           elevation:
                                                                  //               5.0,
                                                                  //           shape:
                                                                  //               RoundedRectangleBorder(
                                                                  //             borderRadius:
                                                                  //                 BorderRadius.circular(8.0),
                                                                  //           ),
                                                                  //           child:
                                                                  //               Container(
                                                                  //             height:
                                                                  //                 30.0,
                                                                  //             width:
                                                                  //                 30.0,
                                                                  //             child:
                                                                  //                 Center(
                                                                  //               child:
                                                                  //                   Text(
                                                                  //                 time.min == null
                                                                  //                     ? "00"
                                                                  //                     : "${time.min}",
                                                                  //                 style:
                                                                  //                     Theme.of(context).textTheme.bodyText1,
                                                                  //               ),
                                                                  //             ),
                                                                  //           ),
                                                                  //         ),
                                                                  //         Text(
                                                                  //           ":",
                                                                  //           style: Theme.of(
                                                                  //                   context)
                                                                  //               .textTheme
                                                                  //               .bodyText1,
                                                                  //         ),
                                                                  //         Card(
                                                                  //           elevation:
                                                                  //               5.0,
                                                                  //           shape:
                                                                  //               RoundedRectangleBorder(
                                                                  //             borderRadius:
                                                                  //                 BorderRadius.circular(8.0),
                                                                  //           ),
                                                                  //           child:
                                                                  //               Container(
                                                                  //             height:
                                                                  //                 30.0,
                                                                  //             width:
                                                                  //                 30.0,
                                                                  //             child:
                                                                  //                 Center(
                                                                  //               child:
                                                                  //                   Text(
                                                                  //                 "${time.sec}",
                                                                  //                 style:
                                                                  //                     Theme.of(context).textTheme.bodyText1,
                                                                  //               ),
                                                                  //             ),
                                                                  //           ),
                                                                  //         ),
                                                                  //       ],
                                                                  //     );
                                                                  //   },
                                                                  // ),
                                                                ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Positioned.fill(
                                                  child: Material(
                                                    color: Colors.transparent,
                                                    child: InkWell(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              15),
                                                      highlightColor:
                                                          Colors.transparent,
                                                      splashColor:
                                                          Theme.of(context)
                                                              .highlightColor,
                                                      onTap: examCategoryList[
                                                                      index][
                                                                  'IsAllowedInSubscription'] ==
                                                              false
                                                          ? () {
                                                              Flushbar(
                                                                message:
                                                                    "Please Subscribe to avail this Option.!!",
                                                                duration:
                                                                    Duration(
                                                                        seconds:
                                                                            2),
                                                                backgroundColor:
                                                                    Colors.red,
                                                              ).show(context);
                                                            }
                                                          : () {
                                                              if (examCategoryList[index]['ExamID'] == null &&
                                                                  examCategoryList[
                                                                              index]
                                                                          [
                                                                          'ExamStartWithin'] ==
                                                                      null &&
                                                                  examCategoryList[
                                                                              index]
                                                                          [
                                                                          'ExamTimeMins'] ==
                                                                      null) {
                                                                Navigator.of(
                                                                        context)
                                                                    .push(
                                                                  MaterialPageRoute(
                                                                    builder:
                                                                        (context) =>
                                                                            TryAnotherExamScreen(
                                                                      examCatId:
                                                                          examCategoryList[index]
                                                                              [
                                                                              'CategoryID'],
                                                                      subCategoryList:
                                                                          examSubCategories,
                                                                    ),
                                                                  ),
                                                                );
                                                              } else {
                                                                if (examCategoryList[
                                                                            index]
                                                                        [
                                                                        'ExamStartWithin'] ==
                                                                    "Exam Going On") {
                                                                  Navigator.of(
                                                                          context)
                                                                      .push(
                                                                    MaterialPageRoute(
                                                                      builder:
                                                                          (context) =>
                                                                              QuizInstructionSreen(
                                                                        examId: examCategoryList[index]
                                                                            [
                                                                            'ExamID'],
                                                                      ),
                                                                    ),
                                                                  );
                                                                } else {
                                                                  Flushbar(
                                                                    message:
                                                                        "Sorry exam not yet started.!!",
                                                                    duration: Duration(
                                                                        seconds:
                                                                            2),
                                                                    backgroundColor:
                                                                        Colors
                                                                            .red,
                                                                  ).show(
                                                                      context);
                                                                }
                                                              }
                                                            },
                                                    ),
                                                  ),
                                                ),
                                                examCategoryList[index][
                                                            'IsAllowedInSubscription'] ==
                                                        false
                                                    ? Positioned.fill(
                                                        child: Align(
                                                          alignment: Alignment
                                                              .bottomRight,
                                                          child:
                                                              SubscriptionLockWidget(),
                                                        ),
                                                      )
                                                    : Container(),
                                              ],
                                            );
                                          }),
                                        ),
                                      ),
                                Container(
                                  margin: EdgeInsets.fromLTRB(
                                      10.0, 10.0, 10.0, 10.0),
                                  child: TopCarousel(
                                    items: centreBannerList,
                                    isDarkMode: isDarkMode,
                                  ),
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 200.0,
                                  margin:
                                      EdgeInsets.only(left: 10.0, top: 10.0),
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: activities.length,
                                    itemBuilder: (BuildContext context, index) {
                                      return InkWell(
                                        borderRadius: BorderRadius.circular(15),
                                        highlightColor: Colors.transparent,
                                        splashColor:
                                            Theme.of(context).highlightColor,
                                        onTap: activities[index]['page'] ==
                                                "Training Videos"
                                            ? () {
                                                Navigator.of(context).push(
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        TrainingVideosScreen(),
                                                  ),
                                                );
                                              }
                                            : activities[index]['page'] ==
                                                    "Subscription"
                                                ? () {
                                                    Navigator.of(context).push(
                                                      MaterialPageRoute(
                                                        builder: (context) =>
                                                            SubscriptionScreen(),
                                                      ),
                                                    );
                                                  }
                                                : activities[index]['page'] ==
                                                        "Feeds"
                                                    ? () {
                                                        Navigator.of(context)
                                                            .push(
                                                          MaterialPageRoute(
                                                            builder: (context) =>
                                                                FeedsOrNotificationsScreen(
                                                              pageTitle:
                                                                  "Feeds",
                                                            ),
                                                          ),
                                                        );
                                                      }
                                                    : () {},
                                        child: Container(
                                          margin: EdgeInsets.only(right: 10.0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Card(
                                                elevation: 5.0,
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          15.0),
                                                ),
                                                child: Container(
                                                  height: 135.0,
                                                  width: 200.5,
                                                  margin: EdgeInsets.all(5.0),
                                                  padding: EdgeInsets.all(15.0),
                                                  decoration: BoxDecoration(
                                                    // color: Colors.white,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15.0),
                                                    // image: DecorationImage(
                                                    //   image: AssetImage(
                                                    //       activities[index]['image']),
                                                    //   fit: BoxFit.fill,
                                                    // ),
                                                  ),
                                                  child: Image.asset(
                                                      activities[index]
                                                          ['image']),
                                                ),
                                              ),
                                              Container(
                                                width: 200.5,
                                                child: Center(
                                                  child: Text(
                                                    activities[index]
                                                        ['tileText'],
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText1,
                                                    textAlign: TextAlign.start,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
      ),
    );
  }
}
