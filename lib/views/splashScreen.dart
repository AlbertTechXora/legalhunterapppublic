import 'dart:async';

import 'package:flutter/material.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import './welcomeScreen.dart';
import './usersHomePage.dart';

import '../widgets/myLogoWidget.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => new _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  startTime() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, checkWhetherLoginRequired);
  }

  // void navigationPage() {
  //   Navigator.pushReplacement(
  //     context,
  //     MaterialPageRoute(
  //       builder: (context) => WelcomeScreen(),
  //     ),
  //   );
  // }

  var providerKey;
  checkWhetherLoginRequired() async {
    WidgetsFlutterBinding.ensureInitialized();
    final preferences = await StreamingSharedPreferences.instance;
    preferences.setInt('BottomAppbarItemIndex', 0);
    Preference<String> proKey =
        preferences.getString('ProviderKey', defaultValue: "");
    providerKey = proKey.getValue();
    print("providerKey: $providerKey");
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => providerKey == null || providerKey == ""
            ? WelcomeScreen()
            : UsersHomePage(),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: MyLogoWidget(),
        ),
      ),
    );
  }
}
