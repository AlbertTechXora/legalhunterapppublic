import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/countdown_timer_controller.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:another_flushbar/flushbar.dart';

import '../widgets/myAppBar.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myNavBar.dart';
import '../widgets/showCustomDialog.dart';

import './examOverviewScreen.dart';

import '../helpers/appFunctions.dart';
import '../services/connections.dart' as Constants;

class QuizExamScreen extends StatefulWidget {
  final int examId;
  final List questionAndAnsList;
  final String durationOfExam;
  final int userSelectedParentCatId;
  final List userSelectedSubCatList;
  QuizExamScreen({
    @required this.questionAndAnsList,
    @required this.examId,
    @required this.durationOfExam,
    this.userSelectedParentCatId,
    this.userSelectedSubCatList,
  });
  @override
  _QuizExamScreenState createState() => _QuizExamScreenState();
}

class _QuizExamScreenState extends State<QuizExamScreen> {
  var counterr = 0;
  var qstnCount = 0;
  var question;
  var questionId;
  List answerList = [];
  List usersAnsPostList = [];
  var selected;
  bool isDarkMode = false;
  var providerKey;
  var apiRequestStatus;
  var apiErrMessage;
  var connectionChk;
  bool isPageLoading = false;
  num examTimeMins = 0;
  int endTime;
  CountdownTimerController examCountdownTimerController;
  bool showNext = false;
  bool showSkip = true;
  var alreadySelected;
  var totalTimeTaken;
  bool canAttendExam = true;

  // int endTime = DateTime.now().millisecondsSinceEpoch + 1000 * (60 * 30);

  setQuestionAndAnswer(prevOrNext) {
    // print("questionAndAnsList:${widget.questionAndAnsList}");
    print("prevOrNext:$prevOrNext");
    print("counterr:$counterr");
    print(
        "questionAndAnsList.length: ${widget.questionAndAnsList.length} & counterr: $counterr");
    if (widget.questionAndAnsList.length >= counterr) {
      setState(() {
        if (prevOrNext == "Prev") {
          qstnCount -= 1;
        } else {
          qstnCount += 1;
        }
        if (widget.questionAndAnsList.length == counterr) {
          qstnCount -= 1;
          counterr -= 1;
        }
        if (prevOrNext == "Prev") {
          for (var i = 0; i < usersAnsPostList.length; i++) {
            if (usersAnsPostList[i]['QuestionID'] ==
                widget.questionAndAnsList[counterr]['QuestionID']) {
              var userAnsPrev = usersAnsPostList[i]['Answer'].split("Option");
              var userAnsIsSkipped = usersAnsPostList[i]['IsSkipped'];
              var userAnsPrevOption =
                  userAnsIsSkipped == true ? "Skipped" : userAnsPrev[1];
              if (userAnsPrevOption == "A") {
                selected = 0;
                alreadySelected = 0;
              } else if (userAnsPrevOption == "B") {
                selected = 1;
                alreadySelected = 1;
              } else if (userAnsPrevOption == "C") {
                selected = 2;
                alreadySelected = 2;
              } else if (userAnsPrevOption == "D") {
                selected = 3;
                alreadySelected = 3;
              } else {
                selected = null;
                alreadySelected = null;
              }
              break;
            }
          }
          showNext = true;
        } else if (prevOrNext == "SkipNext") {
          for (var i = 0; i < usersAnsPostList.length; i++) {
            if (usersAnsPostList[i]['QuestionID'] ==
                widget.questionAndAnsList[counterr]['QuestionID']) {
              var userAnsPrev = usersAnsPostList[i]['Answer'].split("Option");
              var userAnsIsSkipped = usersAnsPostList[i]['IsSkipped'];
              var userAnsPrevOption =
                  userAnsIsSkipped == true ? "Skipped" : userAnsPrev[1];
              if (userAnsPrevOption == "A") {
                selected = 0;
                alreadySelected = 0;
                showNext = true;
              } else if (userAnsPrevOption == "B") {
                selected = 1;
                alreadySelected = 1;
                showNext = true;
              } else if (userAnsPrevOption == "C") {
                selected = 2;
                alreadySelected = 2;
                showNext = true;
              } else if (userAnsPrevOption == "D") {
                selected = 3;
                alreadySelected = 3;
                showNext = true;
              } else {
                selected = null;
                alreadySelected = null;
                showNext = false;
              }
              break;
            } else {
              selected = null;
              alreadySelected = null;
              showNext = false;
            }
          }
        } else if (prevOrNext == "Skip") {
          selected = null;
          alreadySelected = null;
        } else if (prevOrNext == "Next") {
          for (var i = 0; i < usersAnsPostList.length; i++) {
            if (usersAnsPostList[i]['QuestionID'] ==
                widget.questionAndAnsList[counterr]['QuestionID']) {
              var userAnsPrev = usersAnsPostList[i]['Answer'].split("Option");
              var userAnsIsSkipped = usersAnsPostList[i]['IsSkipped'];
              var userAnsPrevOption =
                  userAnsIsSkipped == true ? "Skipped" : userAnsPrev[1];
              if (userAnsPrevOption == "A") {
                selected = 0;
                alreadySelected = 0;
                showNext = true;
              } else if (userAnsPrevOption == "B") {
                selected = 1;
                alreadySelected = 1;
                showNext = true;
              } else if (userAnsPrevOption == "C") {
                selected = 2;
                alreadySelected = 2;
                showNext = true;
              } else if (userAnsPrevOption == "D") {
                selected = 3;
                alreadySelected = 3;
                showNext = true;
              } else {
                selected = null;
                alreadySelected = null;
              }
              break;
            } else {
              selected = null;
              alreadySelected = null;
              showNext = false;
            }
          }
        }
        question = widget.questionAndAnsList[counterr]['QuestionTitle'];
        questionId = widget.questionAndAnsList[counterr]['QuestionID'];
        answerList = [
          {"answer": "A. ${widget.questionAndAnsList[counterr]['OptionA']}"},
          {"answer": "B. ${widget.questionAndAnsList[counterr]['OptionB']}"},
          {"answer": "C. ${widget.questionAndAnsList[counterr]['OptionC']}"},
          {"answer": "D. ${widget.questionAndAnsList[counterr]['OptionD']}"},
        ];
      });
      // print("questionId:$questionId");
      print("question:$question");
      print("selected:$selected");
      print("showNext:$showNext");
    }
  }

  var tempUserResList = [];
  postExamAns(question, answer, isSkiped) {
    var answerOption = answer.split(".");

    Map userAnswered = {
      "QuestionID": question,
      "Answer": "Option${answerOption[0]}",
      "IsSkipped": isSkiped
    };
    for (var i = 0; i < tempUserResList.length; i++) {
      if (question == tempUserResList[i]['QuestionID']) {
        tempUserResList.removeAt(i);
        break;
      }
    }
    tempUserResList.add(userAnswered);
    setState(() {
      usersAnsPostList = tempUserResList;
    });
    print("usersAnsPostList:$usersAnsPostList");
  }

  postExamAnsonTimeOut() {
    List userAnsweredQstnIds = [];
    if (usersAnsPostList.length > 0) {
      for (var i = 0; i < usersAnsPostList.length; i++) {
        print("I: $i");
        var userPostedQstnId = usersAnsPostList[i]['QuestionID'];
        userAnsweredQstnIds.add(userPostedQstnId);
      }
    }
    if (widget.questionAndAnsList.length > 0) {
      for (var k = 0; k < widget.questionAndAnsList.length; k++) {
        print("K: $k");
        var widgetQstnId = widget.questionAndAnsList[k]['QuestionID'];
        if (userAnsweredQstnIds.isNotEmpty &&
            userAnsweredQstnIds.contains(widgetQstnId) == false) {
          postExamAns(widgetQstnId, "A. Skipped", true);
        } else if (userAnsweredQstnIds.isEmpty) {
          postExamAns(widgetQstnId, "A. Skipped", true);
        }
      }
    }
    postUserExam();
  }

  postUserExam() async {
    setState(() {
      isPageLoading = true;
    });
    _timer.cancel();
    print("totalTimeTakenOnPost: $totalTimeTaken");
    print("usersAnsPostList: $usersAnsPostList");
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (usersAnsPostList.length == widget.questionAndAnsList.length) {
      if (connectionChk == null) {
        Map _passData = {
          "ProviderKey": providerKey,
          "ExamID": widget.examId,
          "TotalTimeTaken": totalTimeTaken,
          "QuestionList": usersAnsPostList,
        };
        print("_passData: ${json.encode(_passData)}");
        var url = Uri.parse(Constants.postUserExamDetail);
        var response = await http.post(
          url,
          body: json.encode(_passData),
          headers: {"Content-Type": "application/json"},
        );

        print("response.request:${response.request}");
        if (response.statusCode == 200) {
          var data = json.decode(response.body);
          print("data: $data");
          if (data != null) {
            apiRequestStatus = data['Status'];
            print("apiRequestStatus: $apiRequestStatus");
            if (apiRequestStatus == "Success") {
              // print("data: $data");
              Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                  builder: (context) => ExamOverviewScreen(
                    overviewType: "Current",
                    currExamDetailData: data,
                    questionAndAnsList: widget.questionAndAnsList,
                    userSelectedParentCatId: widget.userSelectedParentCatId,
                    userSelectedSubCatList: widget.userSelectedSubCatList,
                  ),
                ),
              );
            } else {
              if (data['IsProviderKeyValid'] == false) {
                apiErrMessage = "Invalid Provider Key";
                print("apiErrMessage:$apiErrMessage");
                Flushbar(
                  message: apiErrMessage,
                  duration: Duration(seconds: 2),
                  backgroundColor: Colors.red,
                ).show(context);
              } else {
                apiErrMessage = data['Errors'][0];
                print("apiErrMessage:$apiErrMessage");
                Flushbar(
                  message: apiErrMessage,
                  duration: Duration(seconds: 2),
                  backgroundColor: Colors.red,
                ).show(context);
              }
            }
          } else {
            apiRequestStatus = false;
            apiErrMessage = "No data found.";
            Flushbar(
              message: apiErrMessage,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          apiRequestStatus = false;
          apiErrMessage = "Error: ${response.statusCode}.";
          Flushbar(
            message: apiErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        apiRequestStatus = false;
        apiErrMessage = connectionChk;
        Flushbar(
          message: apiErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      postExamAnsonTimeOut();
    }
    setState(() {
      isPageLoading = false;
    });
  }

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<String> proKey =
        preferences.getString('ProviderKey', defaultValue: "");
    Preference<bool> isDark =
        preferences.getBool('isDark', defaultValue: false);
    setState(() {
      proKey.listen((value) {
        providerKey = value;
      });
      isDark.listen((value) {
        isDarkMode = value;
      });
    });
    // print("isDarkMode: $isDarkMode");
  }

  calcAndSetExamTimer() {
    var examDuration = widget.durationOfExam.split(":");
    var examDurationHrs = int.parse(examDuration[0]);
    var examDurationMins = int.parse(examDuration[1]);
    // print(
    //     "examDurationHrs: $examDurationHrs => examDurationMins:$examDurationMins");
    examTimeMins = (examDurationHrs * 60) + examDurationMins;
    endTime =
        DateTime.now().millisecondsSinceEpoch + 1000 * (60 * examTimeMins);
    examCountdownTimerController = CountdownTimerController(
      endTime: endTime,
      onEnd: () {
        examCountdownTimerController.disposeTimer();
        setState(() {
          canAttendExam = false;
        });
      },
    );
    print("examTimeMins:$examTimeMins");
  }

  Timer _timer;
  int seconds = 0;
  int minutes = 0;
  int hours = 0;
  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (seconds < 0) {
            timer.cancel();
          } else {
            seconds = seconds + 1;
            if (seconds > 59) {
              minutes += 1;
              seconds = 0;
              if (minutes > 59) {
                hours += 1;
                minutes = 0;
              }
            }
          }
          totalTimeTaken = "$hours:$minutes:$seconds";
          // print("totalTimeTaken: $totalTimeTaken");
        },
      ),
    );
  }

  Future<bool> _onWillPop() async {
    String titleText = "Confirm to Quit Exam.?";
    String bottomBtnText = "Yes";
    return (await showDialog(
          context: context,
          builder: (BuildContext context) => ShowCustomDialog(
            title: titleText,
            buttonText: bottomBtnText,
            alertFrom: "ExamScreen",
          ),
        )) ??
        false;
  }

  confirmToPostUserExam() {
    print("gsdgh");
    String titleText = "Confirm to Finish Exam.?";
    String bottomBtnText = "Yes";
    return showDialog(
      context: context,
      builder: (BuildContext context) => Dialog(
        insetPadding: EdgeInsets.only(
          left: 12.0,
          right: 12.0,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        elevation: 5.0,
        backgroundColor:
            isDarkMode ? Theme.of(context).primaryColorLight : null,
        child: Container(
          height: 160.0,
          padding: EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                child: FaIcon(
                  FontAwesomeIcons.question,
                  size: 40.0,
                  color: isDarkMode ? Colors.white : Colors.black,
                ),
              ),
              Text(
                titleText,
                style: Theme.of(context).textTheme.headline6,
                textAlign: TextAlign.center,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                    child: Text(
                      bottomBtnText,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                      postUserExam();
                    },
                  ),
                  ElevatedButton(
                    child: Text(
                      "No",
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    // print("questionAndAnsList: ${widget.questionAndAnsList}");
    getSharedDataPref();
    calcAndSetExamTimer();
    setQuestionAndAnswer("Next");
    startTimer();
  }

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: MyAppBar(),
        bottomNavigationBar: MyBottomNavBar(),
        drawer: MyNavDrawer(),
        body: isPageLoading
            ? Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              )
            : Container(
                margin: EdgeInsets.all(10.0),
                child: ListView(
                  children: [
                    Container(
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Container(
                            child: Stack(
                              children: [
                                Container(
                                  child: Image.asset(
                                    "assets/images/6.png",
                                    height: 100.0,
                                    width: 125.0,
                                    fit: BoxFit.contain,
                                  ),
                                ),
                                Positioned(
                                  top: 40.0,
                                  right: 5.0,
                                  // child: Text(
                                  //   "35:20:10",
                                  //   style: TextStyle(
                                  //     color: Colors.grey,
                                  //     fontWeight: FontWeight.bold,
                                  //   ),
                                  // ),
                                  child: CountdownTimer(
                                    controller: examCountdownTimerController,
                                    widgetBuilder:
                                        (_, CurrentRemainingTime time) {
                                      if (time == null) {
                                        return Text(
                                          'Time Out',
                                          style: TextStyle(
                                            color: Colors.grey,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        );
                                      }
                                      if (time.hours == null &&
                                          time.min == null) {
                                        return Text(
                                          '00:00:${time.sec}',
                                          style: TextStyle(
                                            color: Colors.grey,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        );
                                      }
                                      if (time.hours == null) {
                                        return Text(
                                          '00:${time.min}:${time.sec}',
                                          style: TextStyle(
                                            color: Colors.grey,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        );
                                      }
                                      return Text(
                                        '${time.hours}:${time.min}:${time.sec}',
                                        style: TextStyle(
                                          color: Colors.grey,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              confirmToPostUserExam();
                            },
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Container(
                                height: 60.0,
                                width: 70.0,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  color: widget.questionAndAnsList.length ==
                                              qstnCount ||
                                          canAttendExam == false
                                      ? isDarkMode
                                          ? Colors.blueGrey[900]
                                          : Theme.of(context).primaryColor
                                      : Theme.of(context).primaryColorLight,
                                ),
                                child: Center(
                                  child: Text(
                                    "Finish\nExam",
                                    textAlign: TextAlign.center,
                                    style:
                                        TextStyle(fontWeight: FontWeight.w400),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Card(
                      margin: EdgeInsets.only(
                        top: 10.0,
                        bottom: 20.0,
                        right: 5.0,
                        left: 5.0,
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      elevation: 5.0,
                      child: Container(
                        padding: EdgeInsets.all(10.0),
                        child: GridView.count(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          scrollDirection: Axis.vertical,
                          crossAxisCount: 10,
                          childAspectRatio: 1,
                          crossAxisSpacing: 1,
                          children: List.generate(
                              widget.questionAndAnsList.length, (index) {
                            return GestureDetector(
                              onTap: () {
                                counterr = index;
                                qstnCount = index;
                                // print("counterr = index:: ${counterr = index}");
                                setQuestionAndAnswer("SkipNext");
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Theme.of(context).primaryColor,
                                  borderRadius: BorderRadius.circular(5.0),
                                ),
                                margin: EdgeInsets.all(5.0),
                                child: Center(
                                  child: Text(index < 9
                                      ? "0${index + 1}"
                                      : "${index + 1}"),
                                ),
                              ),
                            );
                          }),
                        ),
                      ),
                    ),
                    Container(
                      height: 220.0,
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Container(
                            padding: EdgeInsets.only(left: 15.0, right: 15.0),
                            height: 200.0,
                            decoration: BoxDecoration(
                              color: isDarkMode
                                  ? Theme.of(context).primaryColorLight
                                  : Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: Center(
                              child: Text(
                                question,
                                style: Theme.of(context).textTheme.headline4,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          Positioned.fill(
                            child: Align(
                              alignment: Alignment.topCenter,
                              child: Container(
                                height: 40.0,
                                width: 100.0,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(25.0),
                                    bottomRight: Radius.circular(25.0),
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    "$qstnCount/${widget.questionAndAnsList.length}",
                                    style: isDarkMode
                                        ? TextStyle(
                                            fontSize: 20.0,
                                            height: 1.4,
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                          )
                                        : Theme.of(context).textTheme.headline2,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned.fill(
                            child: Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                height: 50.0,
                                width: 50.0,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.white,
                                ),
                                child: Center(
                                  child: Text(
                                    "?",
                                    style: isDarkMode
                                        ? TextStyle(
                                            fontSize: 20.0,
                                            height: 1.4,
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                          )
                                        : Theme.of(context).textTheme.headline2,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Column(
                        children: [
                          for (var i = 0; i < answerList.length; i++)
                            GestureDetector(
                              onTap: canAttendExam == false
                                  ? () {
                                      Flushbar(
                                        message:
                                            "Time is Over. Please Post Your Exam.!!",
                                        duration: Duration(seconds: 2),
                                        backgroundColor: Colors.red,
                                      ).show(context);
                                    }
                                  : () {
                                      Timer(Duration(milliseconds: 250), () {
                                        if (widget.questionAndAnsList.length >=
                                            counterr) {
                                          // counterr += 1;
                                          print(
                                              "showNext: $showNext; selected: $selected; alreadySelected:$alreadySelected;");

                                          if (showNext == true &&
                                              selected == alreadySelected) {
                                            for (var a = 0;
                                                a < usersAnsPostList.length;
                                                a++) {
                                              if (questionId ==
                                                  usersAnsPostList[a]
                                                      ['QuestionID']) {
                                                var ansIsSkipped =
                                                    usersAnsPostList[a]
                                                        ['IsSkipped'];
                                                if (ansIsSkipped == true) {
                                                  counterr += 1;
                                                  postExamAns(
                                                      questionId,
                                                      answerList[i]['answer'],
                                                      false);
                                                  showNext
                                                      ? setQuestionAndAnswer(
                                                          "SkipNext")
                                                      : setQuestionAndAnswer(
                                                          "Next");
                                                } else {
                                                  setState(() {
                                                    selected = null;
                                                  });
                                                  postExamAns(questionId,
                                                      "A. Skipped", true);
                                                }
                                                break;
                                              }
                                            }
                                          } else if (showNext == true &&
                                              selected != alreadySelected) {
                                            counterr += 1;
                                            postExamAns(questionId,
                                                answerList[i]['answer'], false);
                                            showNext
                                                ? setQuestionAndAnswer(
                                                    "SkipNext")
                                                : setQuestionAndAnswer("Next");
                                          } else if (showNext == false &&
                                              selected != alreadySelected) {
                                            counterr += 1;
                                            postExamAns(questionId,
                                                answerList[i]['answer'], false);
                                            showNext
                                                ? setQuestionAndAnswer(
                                                    "SkipNext")
                                                : setQuestionAndAnswer("Next");
                                          }

                                          // if (showNext == true &&
                                          //     selected == alreadySelected) {
                                          //   postExamAns(questionId,
                                          //       answerList[i]['answer'], false);
                                          //   showNext
                                          //       ? setQuestionAndAnswer("SkipNext")
                                          //       : setQuestionAndAnswer("Next");
                                          // }
                                          // showNext
                                          //     ? setQuestionAndAnswer("SkipNext")
                                          //     : setQuestionAndAnswer("Next");
                                        }
                                      });
                                    },
                              onTapDown: canAttendExam == false
                                  ? (TapDownDetails details) {}
                                  : (TapDownDetails details) {
                                      if (widget.questionAndAnsList.length >=
                                          counterr)
                                        setState(() {
                                          selected = i;
                                        });
                                    },
                              child: Card(
                                elevation: 2,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                margin: EdgeInsets.only(top: 10.0),
                                child: Container(
                                  height: 70.0,
                                  width: MediaQuery.of(context).size.width,
                                  padding: EdgeInsets.only(left: 10.0),
                                  decoration: BoxDecoration(
                                    color: selected == i
                                        ? isDarkMode
                                            ? Theme.of(context)
                                                .primaryColorLight
                                            : Theme.of(context).primaryColor
                                        : Colors.white,
                                    // border: Border.all(
                                    //   color: Colors.grey,
                                    // ),
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      answerList[i]['answer'],
                                      style: selected == i
                                          ? Theme.of(context)
                                              .textTheme
                                              .bodyText2
                                          : Theme.of(context)
                                              .textTheme
                                              .bodyText1,
                                      textAlign: TextAlign.start,
                                      overflow: TextOverflow.visible,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(10.0, 20.0, 10.0, 0.0),
                      child: canAttendExam == false
                          ? Container()
                          : Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                ElevatedButton(
                                  onPressed: () {
                                    if (counterr != 0 &&
                                        widget.questionAndAnsList.length !=
                                            counterr) {
                                      counterr -= 1;
                                      setQuestionAndAnswer("Prev");
                                    }
                                    if (counterr != 0 &&
                                        widget.questionAndAnsList.length ==
                                            counterr) {
                                      counterr -= 2;
                                      setQuestionAndAnswer("Prev");
                                    }
                                  },
                                  child: Text(
                                    "Previous",
                                  ),
                                ),
                                widget.questionAndAnsList.length == qstnCount
                                    ? Container()
                                    : showNext
                                        ? ElevatedButton(
                                            onPressed: () {
                                              if (widget.questionAndAnsList
                                                      .length !=
                                                  counterr) {
                                                counterr += 1;
                                                setQuestionAndAnswer(
                                                    "SkipNext");
                                              }
                                            },
                                            child: Text(
                                              "Next",
                                            ),
                                          )
                                        : ElevatedButton(
                                            onPressed: () {
                                              if (widget.questionAndAnsList
                                                      .length >=
                                                  counterr) {
                                                counterr += 1;
                                                postExamAns(questionId,
                                                    "A. Skipped", true);
                                                setQuestionAndAnswer("Skip");
                                              }
                                            },
                                            child: Text(
                                              "Skip",
                                            ),
                                          ),
                              ],
                            ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
