import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:another_flushbar/flushbar.dart';

import '../helpers/appFunctions.dart';
import '../services/connections.dart' as Constants;

import '../widgets/myAppBar.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myNavBar.dart';
import '../widgets/noInternetWidget.dart';

import 'trainingVideoPlayerScreen.dart';

class CategoryWiseVideosScreen extends StatefulWidget {
  final int videoCategoryId;
  final String videoCategoryName;
  CategoryWiseVideosScreen({
    @required this.videoCategoryId,
    @required this.videoCategoryName,
  });
  @override
  _CategoryWiseVideosScreenState createState() =>
      _CategoryWiseVideosScreenState();
}

class _CategoryWiseVideosScreenState extends State<CategoryWiseVideosScreen> {
  var apiRequestStatus;
  var apiErrMessage;
  var connectionChk;
  bool isPageLoading = false;
  List trainingVideosList = [];
  List trainingVideoListFromAPI = [];

  getTrainingVideosList() async {
    setState(() {
      isPageLoading = true;
    });
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      //multipart/form-data POST Method
      // Map<String, String> requestBody = <String, String>{
      //   "Username": username,
      //   "Password": password,
      // };
      // Map<String, String> headers = <String, String>{
      //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
      // };
      var uri = Uri.parse(Constants.getTrainingVideoList);
      var request = http.MultipartRequest('POST', uri);
      // ..headers.addAll(
      //     headers) //if u have headers, basic auth, token bearer... Else remove line
      // ..fields.addAll(requestBody);
      var response = await request.send();
      final respStr = await response.stream.bytesToString();
      //

      print("response.request:${response.request}");
      if (response.statusCode == 200) {
        var data = json.decode(respStr);
        // print("data: $data");
        if (data != null) {
          apiRequestStatus = data['Status'];
          print("apiRequestStatus: $apiRequestStatus");
          if (apiRequestStatus == "Success") {
            // print("Yes");
            setState(() {
              trainingVideoListFromAPI = data['TrainingVideoList'];
            });
            if (trainingVideoListFromAPI.isEmpty) {
              apiErrMessage = "No Training Videos Found.";
              Flushbar(
                message: apiErrMessage,
                duration: Duration(seconds: 2),
                backgroundColor: Colors.red,
              ).show(context);
            } else {
              displayTrainingVideos(trainingVideoListFromAPI);
            }
          } else {
            apiErrMessage = data['Errors'][0];
            Flushbar(
              message: apiErrMessage,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          apiRequestStatus = false;
          apiErrMessage = "No data found.";
          Flushbar(
            message: apiErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        apiRequestStatus = false;
        apiErrMessage = "Error: ${response.statusCode}.";
        Flushbar(
          message: apiErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      apiRequestStatus = false;
      apiErrMessage = connectionChk;
      Flushbar(
        message: apiErrMessage,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
  }

  displayTrainingVideos(videoListFromAPI) {
    if (videoListFromAPI.length > 0) {
      for (var i = 0; i < videoListFromAPI.length; i++) {
        if (videoListFromAPI[i]['TrainingVideosCategoryDf'] ==
            widget.videoCategoryId) {
          var tempVideosMap = {
            "TrainingVideosID": videoListFromAPI[i]['TrainingVideosID'],
            "TrainingVideosCategoryDf": videoListFromAPI[i]
                ['TrainingVideosCategoryDf'],
            "TrainingVideosTitle": videoListFromAPI[i]['TrainingVideosTitle'],
            "ImageUrl": videoListFromAPI[i]['ImageUrl'],
            "VideoUrl": videoListFromAPI[i]['VideoUrl'],
            "TrainingVideosDesc": videoListFromAPI[i]['TrainingVideosDesc'],
          };
          trainingVideosList.add(tempVideosMap);
        }
      }
    }
  }

  @override
  void initState() {
    super.initState();
    getTrainingVideosList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(),
      bottomNavigationBar: MyBottomNavBar(),
      drawer: MyNavDrawer(),
      body: isPageLoading
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : connectionChk != null
              ? Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        NoInternetWidget(),
                        ElevatedButton(
                          onPressed: () {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      super.widget),
                            );
                          },
                          child: Text("Retry"),
                        ),
                      ],
                    ),
                  )
              : Container(
                  margin: EdgeInsets.all(10.0),
                  child: ListView(
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          left: 10.0,
                        ),
                        child: Text(
                          "${widget.videoCategoryName}",
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        // height: 50.0,
                        margin: EdgeInsets.only(top: 10.0),
                        child: ListView.builder(
                          scrollDirection: Axis.vertical,
                          itemCount: trainingVideosList.length,
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemBuilder: (BuildContext context, index) {
                            return InkWell(
                              borderRadius: BorderRadius.circular(15),
                              highlightColor: Colors.transparent,
                              splashColor: Theme.of(context).highlightColor,
                              onTap: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) =>
                                        TrainingVideoPlayerScreen(
                                      videoLink: trainingVideosList[index]
                                          ['VideoUrl'],
                                    ),
                                  ),
                                );
                              },
                              child: Container(
                                margin: EdgeInsets.all(10.0),
                                child: Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Stack(
                                        alignment: Alignment.center,
                                        children: [
                                          Container(
                                            height: 180.0,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(15.0),
                                              image: DecorationImage(
                                                // image: AssetImage(
                                                //   trainingVideosList[index]['videoImage'],
                                                // ),
                                                image: NetworkImage(
                                                  trainingVideosList[index]
                                                      ['ImageUrl'],
                                                ),
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                          // Positioned(
                                          //   child: Container(
                                          //     height: 60.0,
                                          //     width: 85.0,
                                          //     decoration: BoxDecoration(
                                          //       borderRadius:
                                          //           BorderRadius.circular(12.0),
                                          //       image: DecorationImage(
                                          //         image: AssetImage(
                                          //           "assets/images/youtube_PNG2.png",
                                          //         ),
                                          //         fit: BoxFit.cover,
                                          //       ),
                                          //     ),
                                          //   ),
                                          // ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 5.0,
                                      ),
                                      trainingVideosList[index]
                                                  ['TrainingVideosTitle'] !=
                                              null
                                          ? Text(
                                              trainingVideosList[index]
                                                  ['TrainingVideosTitle'],
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1,
                                            )
                                          : Container(),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
    );
  }
}
