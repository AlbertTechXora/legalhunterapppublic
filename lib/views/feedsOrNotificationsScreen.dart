import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:another_flushbar/flushbar.dart';

import '../helpers/appFunctions.dart';
import '../services/connections.dart' as Constants;

import '../widgets/myAppBar.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myNavBar.dart';
import '../widgets/filtersDialogBox.dart';
import '../widgets/loadingContainers.dart';
import '../widgets/noInternetWidget.dart';
import '../widgets/showCustomDialog.dart';

import './feedsDetailsScreen.dart';

class FeedsOrNotificationsScreen extends StatefulWidget {
  final String pageTitle;
  final int scrollDownToIndex;
  FeedsOrNotificationsScreen({
    @required this.pageTitle,
    this.scrollDownToIndex,
  });
  @override
  _FeedsOrNotificationsScreenState createState() =>
      _FeedsOrNotificationsScreenState();
}

class _FeedsOrNotificationsScreenState
    extends State<FeedsOrNotificationsScreen> {
  var apiRequestStatus;
  var apiErrMessage;
  var connectionChk;
  bool isPageLoading = false;
  List feeds = [];
  List notifications = [];
  ScrollController _feedsController = new ScrollController();

  void _goToElement(int index) {
    _feedsController.animateTo(
        (100.0 *
            index), // 100 is the height of container and index of 6th element is 5
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeOut);
  }

  getLatestFeeds() async {
    setState(() {
      isPageLoading = true;
    });
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      //multipart/form-data POST Method
      // Map<String, String> requestBody = <String, String>{
      //   "Username": username,
      //   "Password": password,
      // };
      // Map<String, String> headers = <String, String>{
      //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
      // };
      var uri = Uri.parse(Constants.getLatestFeeds);
      var request = http.MultipartRequest('POST', uri);
      // ..headers.addAll(
      //     headers) //if u have headers, basic auth, token bearer... Else remove line
      // ..fields.addAll(requestBody);
      var response = await request.send();
      final respStr = await response.stream.bytesToString();
      //

      print("response.request:${response.request}");
      if (response.statusCode == 200) {
        var data = json.decode(respStr);
        // print("data: $data");
        if (data != null) {
          apiRequestStatus = data['Status'];
          print("apiRequestStatus: $apiRequestStatus");
          if (apiRequestStatus == "Success") {
            // print("Yes");
            setState(() {
              feeds = data['FeedList'];
            });
            if (feeds.isEmpty) {
              apiErrMessage = "No New Feeds.";
              Flushbar(
                message: apiErrMessage,
                duration: Duration(seconds: 2),
                backgroundColor: Colors.red,
              ).show(context);
            }
          } else {
            apiErrMessage = data['Errors'][0];
            Flushbar(
              message: apiErrMessage,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          apiRequestStatus = false;
          apiErrMessage = "No data found.";
          Flushbar(
            message: apiErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        apiRequestStatus = false;
        apiErrMessage = "Error: ${response.statusCode}.";
        Flushbar(
          message: apiErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      apiRequestStatus = false;
      apiErrMessage = connectionChk;
      Flushbar(
        message: apiErrMessage,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
    print("widget.scrollDownToIndex: ${widget.scrollDownToIndex}");
    if (widget.scrollDownToIndex != null) {
      _goToElement(widget.scrollDownToIndex);
    }
  }

  getLatestNotification() async {
    setState(() {
      isPageLoading = true;
    });
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      //multipart/form-data POST Method
      // Map<String, String> requestBody = <String, String>{
      //   "Username": username,
      //   "Password": password,
      // };
      // Map<String, String> headers = <String, String>{
      //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
      // };
      var uri = Uri.parse(Constants.getNotification);
      var request = http.MultipartRequest('POST', uri);
      // ..headers.addAll(
      //     headers) //if u have headers, basic auth, token bearer... Else remove line
      // ..fields.addAll(requestBody);
      var response = await request.send();
      final respStr = await response.stream.bytesToString();
      //

      print("response.request:${response.request}");
      if (response.statusCode == 200) {
        var data = json.decode(respStr);
        // print("data: $data");
        if (data != null) {
          apiRequestStatus = data['Status'];
          print("apiRequestStatus: $apiRequestStatus");
          if (apiRequestStatus == "Success") {
            // print("Yes");
            setState(() {
              notifications = data['NotificationList'];
            });
            if (notifications.isEmpty) {
              apiErrMessage = "No New Notifications.";
              Flushbar(
                message: apiErrMessage,
                duration: Duration(seconds: 2),
                backgroundColor: Colors.red,
              ).show(context);
            }
          } else {
            apiErrMessage = data['Errors'][0];
            Flushbar(
              message: apiErrMessage,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          apiRequestStatus = false;
          apiErrMessage = "No data found.";
          Flushbar(
            message: apiErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        apiRequestStatus = false;
        apiErrMessage = "Error: ${response.statusCode}.";
        Flushbar(
          message: apiErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      apiRequestStatus = false;
      apiErrMessage = connectionChk;
      Flushbar(
        message: apiErrMessage,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
  }

  Future<bool> _onWillPop() async {
    String titleText = "Confirm to Exit Legalhunter.?";
    String bottomBtnText = "Yes";
    return (await showDialog(
          context: context,
          builder: (BuildContext context) => ShowCustomDialog(
            title: titleText,
            buttonText: bottomBtnText,
          ),
        )) ??
        false;
  }

  @override
  void initState() {
    super.initState();
    widget.pageTitle == "Feeds" ? getLatestFeeds() : getLatestNotification();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: MyAppBar(),
        bottomNavigationBar: MyBottomNavBar(),
        drawer: MyNavDrawer(),
        body: isPageLoading
            ? Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              )
            : connectionChk != null
                ? Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        NoInternetWidget(),
                        ElevatedButton(
                          onPressed: () {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      super.widget),
                            );
                          },
                          child: Text("Retry"),
                        ),
                      ],
                    ),
                  )
                : Container(
                    child: ListView(
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                widget.pageTitle,
                                style: Theme.of(context).textTheme.headline6,
                              ),
                              widget.pageTitle == "Feeds"
                                  ? GestureDetector(
                                      onTap: () {
                                        showDialog(
                                          context: context,
                                          builder: (BuildContext context) =>
                                              FiltersDialogBox(),
                                        );
                                      },
                                      child: Container(
                                        child: Row(
                                          children: [
                                            Text(
                                              "Filters",
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1,
                                            ),
                                            Icon(
                                              Icons.filter_list,
                                              color: Colors.grey,
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  : Container(),
                            ],
                          ),
                        ),
                        feeds.isEmpty && widget.pageTitle == "Feeds"
                            ? LoadingContainers(loaderType: "List")
                            : notifications.isEmpty &&
                                    widget.pageTitle == "Notification"
                                ? LoadingContainers(loaderType: "List")
                                : Container(
                                    width: MediaQuery.of(context).size.width,
                                    // height: 50.0,
                                    margin: EdgeInsets.only(top: 10.0),
                                    child: ListView.builder(
                                      controller: _feedsController,
                                      scrollDirection: Axis.vertical,
                                      itemCount: widget.pageTitle == "Feeds"
                                          ? feeds.length
                                          : notifications.length,
                                      physics: NeverScrollableScrollPhysics(),
                                      shrinkWrap: true,
                                      itemBuilder:
                                          (BuildContext context, index) {
                                        return GestureDetector(
                                          onTap: widget.pageTitle == "Feeds"
                                              ? () {
                                                  Navigator.of(context).push(
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          FeedsDetailsScreen(
                                                        feedsImage: feeds[index]
                                                            ['FeedImageUrl'],
                                                        feedsTitle: feeds[index]
                                                            ['FeedTitle'],
                                                        feedsDesc: feeds[index]
                                                            ['FeedDesc'],
                                                        feedsTimeStamp: feeds[
                                                                index][
                                                            'FeedDateFormatted'],
                                                      ),
                                                    ),
                                                  );
                                                }
                                              : () {},
                                          child: Container(
                                            margin: EdgeInsets.all(10.0),
                                            child: Container(
                                              child: widget.pageTitle == "Feeds"
                                                  ? Column(
                                                      children: [
                                                        feeds[index][
                                                                    'FeedType'] !=
                                                                "Text"
                                                            ? Container(
                                                                height: 180.0,
                                                                margin: EdgeInsets
                                                                    .fromLTRB(
                                                                        5.0,
                                                                        10.0,
                                                                        5.0,
                                                                        10.0),
                                                                decoration:
                                                                    BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              15.0),
                                                                  image:
                                                                      DecorationImage(
                                                                    // image: AssetImage(
                                                                    //   feeds[index]['image'],
                                                                    // ),
                                                                    image: NetworkImage(
                                                                        feeds[index]
                                                                            [
                                                                            'FeedImageUrl']),
                                                                    fit: BoxFit
                                                                        .cover,
                                                                  ),
                                                                ),
                                                              )
                                                            : Container(),
                                                        Card(
                                                          elevation: 5.0,
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          15.0)),
                                                          child: Container(
                                                            padding:
                                                                EdgeInsets.all(
                                                                    15.0),
                                                            child: Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: [
                                                                Row(
                                                                  children: [
                                                                    Text(
                                                                        feeds[index]
                                                                            [
                                                                            'FeedTitle'],
                                                                        style: Theme.of(context)
                                                                            .textTheme
                                                                            .subtitle2),
                                                                  ],
                                                                ),
                                                                SizedBox(
                                                                  height: 10.0,
                                                                ),
                                                                feeds[index][
                                                                            'FeedDesc'] !=
                                                                        null
                                                                    ? Text(
                                                                        feeds[index]
                                                                            [
                                                                            'FeedDesc'],
                                                                        style: Theme.of(context)
                                                                            .textTheme
                                                                            .bodyText1,
                                                                      )
                                                                    : Container(),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                  : Column(
                                                      children: [
                                                        notifications[index][
                                                                    'NotificationImageUrl'] !=
                                                                null
                                                            ? Container(
                                                                margin: EdgeInsets
                                                                    .fromLTRB(
                                                                        5.0,
                                                                        10.0,
                                                                        5.0,
                                                                        10.0),
                                                                child: Column(
                                                                  children: [
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .end,
                                                                      children: [
                                                                        Text(
                                                                          notifications[index]
                                                                              [
                                                                              'CreatedOn'],
                                                                          style:
                                                                              TextStyle(
                                                                            color:
                                                                                Colors.grey,
                                                                            fontWeight:
                                                                                FontWeight.normal,
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                    Container(
                                                                      height:
                                                                          180.0,
                                                                      margin: EdgeInsets
                                                                          .only(
                                                                              top: 5.0),
                                                                      decoration:
                                                                          BoxDecoration(
                                                                        borderRadius:
                                                                            BorderRadius.circular(15.0),
                                                                        image:
                                                                            DecorationImage(
                                                                          // image: AssetImage(
                                                                          //   notifications[index]
                                                                          //       ['image'],
                                                                          // ),
                                                                          image:
                                                                              NetworkImage(notifications[index]['NotificationImageUrl']),
                                                                          fit: BoxFit
                                                                              .cover,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              )
                                                            : Container(),
                                                        Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .end,
                                                          children: [
                                                            Text(
                                                              notifications[
                                                                      index]
                                                                  ['CreatedOn'],
                                                              style: TextStyle(
                                                                color:
                                                                    Colors.grey,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .normal,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        Card(
                                                          elevation: 5.0,
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          15.0)),
                                                          child: Container(
                                                            padding:
                                                                EdgeInsets.all(
                                                                    15.0),
                                                            child: Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: [
                                                                Row(
                                                                  children: [
                                                                    Text(
                                                                      notifications[
                                                                              index]
                                                                          [
                                                                          'NotificationTitle'],
                                                                      style: Theme.of(
                                                                              context)
                                                                          .textTheme
                                                                          .subtitle2,
                                                                    ),
                                                                  ],
                                                                ),
                                                                SizedBox(
                                                                  height: 10.0,
                                                                ),
                                                                Text(
                                                                  notifications[
                                                                          index]
                                                                      [
                                                                      'NotificationDesc'],
                                                                  style: Theme.of(
                                                                          context)
                                                                      .textTheme
                                                                      .bodyText1,
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                      ],
                    ),
                  ),
      ),
    );
  }
}
