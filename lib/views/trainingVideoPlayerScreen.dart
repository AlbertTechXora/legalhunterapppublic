import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

// import '../widgets/myAppBar.dart';
// import '../widgets/myBottomNavBar.dart';
import '../widgets/myNavBar.dart';

class TrainingVideoPlayerScreen extends StatefulWidget {
  final String videoLink;
  TrainingVideoPlayerScreen({@required this.videoLink});
  @override
  _TrainingVideoPlayerScreenState createState() =>
      _TrainingVideoPlayerScreenState();
}

class _TrainingVideoPlayerScreenState extends State<TrainingVideoPlayerScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: MyAppBar(),
      // bottomNavigationBar: MyBottomNavBar(),
      drawer: MyNavDrawer(),
      body: Container(
        child: Center(
          child: YoutubePlayer(
            controller: YoutubePlayerController(
              initialVideoId: YoutubePlayer.convertUrlToId(widget.videoLink),
              flags: YoutubePlayerFlags(
                autoPlay: true,
                mute: false,
              ),
            ),
            showVideoProgressIndicator: true,
          ),
        ),
      ),
    );
  }
}
