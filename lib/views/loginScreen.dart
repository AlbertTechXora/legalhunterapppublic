import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:hexcolor/hexcolor.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:another_flushbar/flushbar.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import '../widgets/socialLoginButtons.dart';
import '../widgets/myLogoWidget.dart';

import '../helpers/appFunctions.dart';
import '../services/connections.dart' as Constants;

import './usersHomePage.dart';
import './forgotPassword.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var loginRequestStatus;
  var loginErrMessage;
  var connectionChk;
  bool isPageLoading = false;
  bool isPswdObscured = true;

  TextEditingController usernameController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  makePswdHiddenOrVisible() {
    if (isPswdObscured == true) {
      setState(() {
        isPswdObscured = false;
      });
    } else {
      setState(() {
        isPswdObscured = true;
      });
    }
  }

  applicationLogin() async {
    setState(() {
      isPageLoading = true;
    });
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      var username = usernameController.text;
      var password = passwordController.text;
      if ((username != "") && password != "") {
        print("username:" + username + "=>" + "password:" + password);

        //multipart/form-data POST Method
        Map<String, String> requestBody = <String, String>{
          "Username": username,
          "Password": password,
        };
        // Map<String, String> headers = <String, String>{
        //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
        // };
        var uri = Uri.parse(Constants.applicationLogin);
        var request = http.MultipartRequest('POST', uri)
          // ..headers.addAll(
          //     headers) //if u have headers, basic auth, token bearer... Else remove line
          ..fields.addAll(requestBody);
        var response = await request.send();
        final respStr = await response.stream.bytesToString();
        //

        print("response.request:${response.request}");
        if (response.statusCode == 200) {
          // print("Yes");
          var data = json.decode(respStr);
          print("data: $data");
          if (data != null) {
            loginRequestStatus = data['LoginRequestStatus'];
            print("loginRequestStatus: $loginRequestStatus");
            if (loginRequestStatus == true) {
              // print("Yes");
              final preferences = await StreamingSharedPreferences.instance;
              preferences.setString('LoginType', "Normal");
              preferences.setString('ProviderKey', data['ProviderKey']);
              preferences.setString('UserName', data['UserName']);
              preferences.setString('ProfileImage', data['ProfileImage']);
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) => UsersHomePage(),
                ),
              );
            } else {
              loginErrMessage = data['LoginErrorMessage'][0];
              usernameController.text = "";
              passwordController.text = "";
              Flushbar(
                message: loginErrMessage,
                duration: Duration(seconds: 2),
                backgroundColor: Colors.red,
              ).show(context);
            }
          } else {
            loginRequestStatus = false;
            loginErrMessage = "No data found.";
            passwordController.text = "";
            Flushbar(
              message: loginErrMessage,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          loginRequestStatus = false;
          loginErrMessage = "Error: ${response.statusCode}.";
          Flushbar(
            message: loginErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        loginRequestStatus = false;
        loginErrMessage = "Please enter your credentials.";
        Flushbar(
          message: loginErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      loginRequestStatus = false;
      loginErrMessage = connectionChk;
      Flushbar(
        message: loginErrMessage,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: isPageLoading
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : Container(
              child: ListView(
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      top: 30.0,
                      bottom: 30.0,
                    ),
                    height: 230.0,
                    width: 230.0,
                    child: Image.asset("assets/images/3.png"),
                  ),
                  Container(
                    child: Column(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.90,
                          child: Column(
                            children: [
                              Container(
                                margin:
                                    EdgeInsets.only(left: 10.0, bottom: 5.0),
                                child: Row(
                                  children: [
                                    Text(
                                      "Username",
                                      style: TextStyle(
                                        color: HexColor("#494949"),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              // Container(
                              //   margin: EdgeInsets.only(top: 5.0),
                              //   decoration: BoxDecoration(
                              //     border: Border.all(width: 1.0),
                              //     borderRadius: BorderRadius.circular(18),
                              //   ),
                              //   child: TextField(
                              //     controller: usernameController,
                              //     keyboardType: TextInputType.text,
                              //     style: TextStyle(
                              //       color: Colors.black,
                              //       fontWeight: FontWeight.normal,
                              //     ),
                              //     decoration: InputDecoration(
                              //       hintText: 'Email/ Contact Number',
                              //       hintStyle: TextStyle(
                              //         fontSize: 14.0,
                              //         color: Colors.grey,
                              //         fontWeight: FontWeight.normal,
                              //       ),
                              //       border: OutlineInputBorder(
                              //         borderRadius: BorderRadius.circular(15),
                              //         borderSide: BorderSide(
                              //           width: 0,
                              //           style: BorderStyle.none,
                              //         ),
                              //       ),
                              //       filled: true,
                              //       contentPadding: EdgeInsets.all(10),
                              //       fillColor: Colors.white70,
                              //     ),
                              //   ),
                              // ),
                              Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25),
                                ),
                                elevation: 5.0,
                                child: TextField(
                                  controller: usernameController,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.normal,
                                  ),
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    hintText: 'Email/ Contact Number',
                                    hintStyle: TextStyle(
                                      fontSize: 14.0,
                                      color: Colors.grey,
                                      fontWeight: FontWeight.normal,
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(25),
                                      borderSide: BorderSide(
                                        width: 0,
                                        style: BorderStyle.none,
                                      ),
                                    ),
                                    filled: true,
                                    contentPadding: EdgeInsets.all(16),
                                    fillColor: Colors.white70,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 20.0, left: 10.0),
                                child: Row(
                                  children: [
                                    Text(
                                      "Password",
                                      style: TextStyle(
                                        color: HexColor("#494949"),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              // Container(
                              //   margin: EdgeInsets.only(top: 5.0),
                              //   decoration: BoxDecoration(
                              //     border: Border.all(width: 1.0),
                              //     borderRadius: BorderRadius.circular(18),
                              //   ),
                              //   child: Row(
                              //     children: [
                              //       Flexible(
                              //         child: TextField(
                              //           controller: passwordController,
                              //           keyboardType:
                              //               TextInputType.visiblePassword,
                              //           obscureText: isPswdObscured,
                              //           obscuringCharacter: "*",
                              //           style: TextStyle(
                              //             color: Colors.black,
                              //             fontWeight: FontWeight.normal,
                              //           ),
                              //           decoration: InputDecoration(
                              //             hintText: 'Password',
                              //             hintStyle: TextStyle(
                              //               fontSize: 14.0,
                              //               color: Colors.grey,
                              //               fontWeight: FontWeight.normal,
                              //             ),
                              //             border: OutlineInputBorder(
                              //               borderRadius:
                              //                   BorderRadius.circular(15),
                              //               borderSide: BorderSide(
                              //                 width: 0,
                              //                 style: BorderStyle.none,
                              //               ),
                              //             ),
                              //             filled: true,
                              //             contentPadding: EdgeInsets.all(10),
                              //             fillColor: Colors.white70,
                              //           ),
                              //         ),
                              //       ),
                              //       IconButton(
                              //         onPressed: makePswdHiddenOrVisible,
                              //         icon: isPswdObscured
                              //             ? FaIcon(
                              //                 FontAwesomeIcons.solidEye,
                              //                 size: 18.0,
                              //               )
                              //             : FaIcon(
                              //                 FontAwesomeIcons.solidEyeSlash,
                              //                 size: 18.0,
                              //               ),
                              //         color: Colors.grey,
                              //       ),
                              //     ],
                              //   ),
                              // ),
                              Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25),
                                ),
                                elevation: 5.0,
                                child: Row(
                                  children: [
                                    Flexible(
                                      child: TextField(
                                        controller: passwordController,
                                        keyboardType:
                                            TextInputType.visiblePassword,
                                        obscureText: isPswdObscured,
                                        obscuringCharacter: "*",
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.normal,
                                        ),
                                        decoration: InputDecoration(
                                          hintText: 'Password',
                                          hintStyle: TextStyle(
                                            fontSize: 14.0,
                                            color: Colors.grey,
                                            fontWeight: FontWeight.normal,
                                          ),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(25),
                                            borderSide: BorderSide(
                                              width: 0,
                                              style: BorderStyle.none,
                                            ),
                                          ),
                                          filled: true,
                                          contentPadding: EdgeInsets.all(16),
                                          fillColor: Colors.white70,
                                        ),
                                      ),
                                    ),
                                    IconButton(
                                      onPressed: makePswdHiddenOrVisible,
                                      icon: isPswdObscured
                                          ? FaIcon(
                                              FontAwesomeIcons.solidEye,
                                              size: 18.0,
                                            )
                                          : FaIcon(
                                              FontAwesomeIcons.solidEyeSlash,
                                              size: 18.0,
                                            ),
                                      color: Colors.grey,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 15.0,
                              ),
                              ElevatedButton(
                                onPressed: () {
                                  applicationLogin();
                                  // Navigator.of(context).pushReplacement(
                                  //   MaterialPageRoute(
                                  //     builder: (context) => UsersHomePage(),
                                  //   ),
                                  // );
                                },
                                child: Text(
                                  "LOGIN",
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Container(
                                child: TextButton(
                                  onPressed: () {
                                    Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: (context) => ForgotPassword(),
                                      ),
                                    );
                                  },
                                  child: Text(
                                    "Forgot your Password?",
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  SocialLoginButtons(),
                  SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    child: Column(
                      children: [
                        MyLogoWidget(),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                ],
              ),
            ),
    );
  }
}
