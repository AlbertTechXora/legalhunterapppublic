import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:another_flushbar/flushbar.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import '../helpers/appFunctions.dart';
import '../services/connections.dart' as Constants;

import '../widgets/myAppBar.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myNavBar.dart';
import '../widgets/loadingContainers.dart';
import '../widgets/noInternetWidget.dart';
import '../widgets/showCustomDialog.dart';

import './quizInstructionScreen.dart';

class TryAnotherExamScreen extends StatefulWidget {
  final int examCatId;
  final List subCategoryList;
  TryAnotherExamScreen({
    @required this.examCatId,
    @required this.subCategoryList,
  });
  @override
  _TryAnotherExamScreenState createState() => _TryAnotherExamScreenState();
}

class _TryAnotherExamScreenState extends State<TryAnotherExamScreen> {
  var apiRequestStatus;
  var apiErrMessage;
  var connectionChk;
  bool isPageLoading = false;
  List examCategoryList = [];
  List examListByCategory = [];
  var selected;
  var providerKey;
  bool isDarkMode = false;

  getExamCategories() async {
    setState(() {
      isPageLoading = true;
    });
    if (widget.subCategoryList.isEmpty) {
      connectionChk = await AppFuntions.connectionCheck();
      print("connectionChk: $connectionChk");
      if (connectionChk == null) {
        //multipart/form-data POST Method
        Map<String, String> requestBody = <String, String>{
          "ProviderKey": providerKey,
        };
        // Map<String, String> headers = <String, String>{
        //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
        // };
        var uri = Uri.parse(Constants.getCategoriesList);
        var request = http.MultipartRequest('POST', uri)
          // ..headers.addAll(
          //     headers) //if u have headers, basic auth, token bearer... Else remove line
          ..fields.addAll(requestBody);
        var response = await request.send();
        final respStr = await response.stream.bytesToString();
        //

        print("response.request:${response.request}");
        if (response.statusCode == 200) {
          var data = json.decode(respStr);
          // print("data: $data");
          if (data != null) {
            apiRequestStatus = data['Status'];
            print("apiRequestStatus: $apiRequestStatus");
            if (apiRequestStatus == "Success") {
              // print("Yes");
              setState(() {
                examCategoryList = data['ChildCategoryList'];
              });
              for (var i = 0; i < 1; i++) {
                var catId = examCategoryList[i]['CategoryID'];
                setState(() {
                  selected = i;
                });
                print("selected: $selected");
                getExamListByCategories(catId);
              }
              if (examCategoryList.isEmpty) {
                apiErrMessage = "No Exam Categories Found.";
                Flushbar(
                  message: apiErrMessage,
                  duration: Duration(seconds: 2),
                  backgroundColor: Colors.red,
                ).show(context);
              }
            } else {
              apiErrMessage = data['Errors'][0];
              Flushbar(
                message: apiErrMessage,
                duration: Duration(seconds: 2),
                backgroundColor: Colors.red,
              ).show(context);
            }
          } else {
            apiRequestStatus = false;
            apiErrMessage = "No data found.";
            Flushbar(
              message: apiErrMessage,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          apiRequestStatus = false;
          apiErrMessage = "Error: ${response.statusCode}.";
          Flushbar(
            message: apiErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        apiRequestStatus = false;
        apiErrMessage = connectionChk;
        Flushbar(
          message: apiErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      print("Coming");
      // print("widget.subCategoryList: ${widget.subCategoryList}");
      var subCatId;
      if (widget.subCategoryList.length > 0) {
        print("Loop Started");
        for (var i = 0; i < widget.subCategoryList.length; i++) {
          var parentCatId = widget.subCategoryList[i]['ParentCategoryID'];
          print(
              "parentCatId: $parentCatId => widget.examCatId: ${widget.examCatId}");
          if (parentCatId == widget.examCatId) {
            setState(() {
              subCatId = widget.subCategoryList[i]['CategoryID'];
            });
            print("Yes");
            break;
          } else {
            print("No");
          }
        }
        print("subCatId:$subCatId");
        for (var i = 0; i < widget.subCategoryList.length; i++) {
          var parentCatId = widget.subCategoryList[i]['ParentCategoryID'];
          if (parentCatId == widget.examCatId) {
            var childCatId = widget.subCategoryList[i]['CategoryID'];
            var childCatName = widget.subCategoryList[i]['CategoryName'];
            var isAllowedInSubscription =
                widget.subCategoryList[i]['IsAllowedInSubscription'];
            var tempChildCatMap = {
              "CategoryID": childCatId,
              "ParentCategoryID": parentCatId,
              "CategoryName": childCatName,
              "IsAllowedInSubscription": isAllowedInSubscription,
            };
            examCategoryList.add(tempChildCatMap);
          }
        }
        // setState(() {
        //   examCategoryList = widget.subCategoryList;
        // });
        for (var i = 0; i < examCategoryList.length; i++) {
          var catId = examCategoryList[i]['CategoryID'];
          if (catId == subCatId) {
            setState(() {
              selected = i;
            });
            break;
          }
        }
        print("selected: $selected");
        getExamListByCategories(subCatId);
      } else {
        apiErrMessage = "No SubCategories Found.";
        Flushbar(
          message: apiErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    }
    setState(() {
      isPageLoading = false;
    });
  }

  getExamListByCategories(categoryId) async {
    setState(() {
      isPageLoading = true;
    });
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      //multipart/form-data POST Method
      Map<String, String> requestBody = <String, String>{
        "CategoryID": categoryId.toString(),
        "ParentCategoryId": widget.examCatId.toString(),
        "ChildCategoryId": categoryId.toString(),
      };
      // Map<String, String> headers = <String, String>{
      //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
      // };
      print("requestBody:$requestBody");
      var uri = Uri.parse(Constants.getExamListByCategory);
      var request = http.MultipartRequest('POST', uri)
        // ..headers.addAll(
        //     headers) //if u have headers, basic auth, token bearer... Else remove line
        ..fields.addAll(requestBody);
      var response = await request.send();
      final respStr = await response.stream.bytesToString();
      //

      print("response.request:${response.request}");
      if (response.statusCode == 200) {
        var data = json.decode(respStr);
        // print("data: $data");
        if (data != null) {
          apiRequestStatus = data['Status'];
          print("apiRequestStatus: $apiRequestStatus");
          if (apiRequestStatus == "Success") {
            // print("Yes");
            setState(() {
              examListByCategory = data['ExamList'];
            });
            if (examListByCategory.isEmpty) {
              apiErrMessage =
                  "No Exams Found. Please use above category filters if any.";
              Flushbar(
                message: apiErrMessage,
                duration: Duration(seconds: 2),
                backgroundColor: Colors.red,
              ).show(context);
            }
          } else {
            apiErrMessage = data['Errors'][0];
            Flushbar(
              message: apiErrMessage,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          apiRequestStatus = false;
          apiErrMessage = "No data found.";
          Flushbar(
            message: apiErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        apiRequestStatus = false;
        apiErrMessage = "Error: ${response.statusCode}.";
        Flushbar(
          message: apiErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      apiRequestStatus = false;
      apiErrMessage = connectionChk;
      Flushbar(
        message: apiErrMessage,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
  }

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<String> proKey =
        preferences.getString('ProviderKey', defaultValue: "");
    Preference<bool> isDark =
        preferences.getBool('isDark', defaultValue: false);
    proKey.listen((value) {
      setState(() {
        providerKey = value;
      });
    });
    isDark.listen((value) {
      isDarkMode = value;
    });
  }

  Future<bool> _onWillPop() async {
    String titleText = "Confirm to Exit Legalhunter.?";
    String bottomBtnText = "Yes";
    return (await showDialog(
          context: context,
          builder: (BuildContext context) => ShowCustomDialog(
            title: titleText,
            buttonText: bottomBtnText,
          ),
        )) ??
        false;
  }

  @override
  void initState() {
    super.initState();
    getSharedDataPref();
    getExamCategories();
    // getExamListByCategories(widget.examCatId);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: MyAppBar(),
        bottomNavigationBar: MyBottomNavBar(),
        drawer: MyNavDrawer(),
        body: isPageLoading
            ? Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              )
            : connectionChk != null
                ? Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        NoInternetWidget(),
                        ElevatedButton(
                          onPressed: () {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      super.widget),
                            );
                          },
                          child: Text("Retry"),
                        ),
                      ],
                    ),
                  )
                : Container(
                    child: ListView(
                      children: [
                        examCategoryList.isEmpty
                            ? Container()
                            : Container(
                                width: MediaQuery.of(context).size.width,
                                height: 50.0,
                                margin: EdgeInsets.only(top: 10.0, left: 10.0),
                                child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  itemCount: examCategoryList.length,
                                  itemBuilder: (BuildContext context, index) {
                                    return GestureDetector(
                                      onTap: examCategoryList[index]
                                                  ['IsAllowedInSubscription'] ==
                                              false
                                          ? () {
                                              Flushbar(
                                                message:
                                                    "Please Subscribe to avail this Option.!!",
                                                duration: Duration(seconds: 2),
                                                backgroundColor: Colors.red,
                                              ).show(context);
                                            }
                                          : () {
                                              getExamListByCategories(
                                                  examCategoryList[index]
                                                      ['CategoryID']);
                                            },
                                      onTapDown: (TapDownDetails details) {
                                        setState(() {
                                          selected = index;
                                        });
                                      },
                                      child: Card(
                                        elevation: 5.0,
                                        margin: EdgeInsets.all(5.0),
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(25.0),
                                        ),
                                        color: selected == index
                                            ? Theme.of(context)
                                                .primaryColorLight
                                            : isDarkMode
                                                ? null
                                                : Theme.of(context)
                                                    .primaryColor,
                                        child: Container(
                                          padding: EdgeInsets.all(10.0),
                                          constraints: BoxConstraints(
                                            minWidth: 165.0,
                                          ),
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(25.0),
                                          ),
                                          child: Center(
                                            child: Text(
                                              examCategoryList[index]
                                                  ['CategoryName'],
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline4,
                                            ),
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                        examListByCategory.isEmpty
                            ? LoadingContainers(
                                loaderType: "List",
                              )
                            : Container(
                                width: MediaQuery.of(context).size.width,
                                // height: 50.0,
                                margin: EdgeInsets.only(top: 10.0),
                                child: ListView.builder(
                                  scrollDirection: Axis.vertical,
                                  itemCount: examListByCategory.length,
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemBuilder: (BuildContext context, index) {
                                    return InkWell(
                                      borderRadius: BorderRadius.circular(15.0),
                                      splashColor:
                                          Theme.of(context).highlightColor,
                                      onTap: () {
                                        Navigator.of(context).push(
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                QuizInstructionSreen(
                                              examId: examListByCategory[index]
                                                  ['ExamID'],
                                              userSelectedParentCatId:
                                                  widget.examCatId,
                                              userSelectedSubCatList:
                                                  widget.subCategoryList,
                                            ),
                                          ),
                                        );
                                      },
                                      child: Container(
                                        margin: EdgeInsets.all(10.0),
                                        child: Card(
                                          elevation: 5.0,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(15.0)),
                                          child: Container(
                                            padding: EdgeInsets.all(15.0),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  examListByCategory[index]
                                                      ['ExamName'],
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle2,
                                                ),
                                                SizedBox(
                                                  height: 10.0,
                                                ),
                                                examListByCategory[index]
                                                            ['ExamDesc'] !=
                                                        null
                                                    ? Text(
                                                        examListByCategory[
                                                            index]['ExamDesc'],
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .bodyText1,
                                                        textAlign:
                                                            TextAlign.justify,
                                                      )
                                                    : Container(),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                      ],
                    ),
                  ),
      ),
    );
  }
}
