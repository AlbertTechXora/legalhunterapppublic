import 'package:flutter/material.dart';
// import 'package:hexcolor/hexcolor.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:another_flushbar/flushbar.dart';

import '../widgets/myLogoWidget.dart';
import '../widgets/socialLoginButtons.dart';

import '../helpers/appFunctions.dart';
import '../services/connections.dart' as Constants;

import '../views/loginScreen.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth > 1200) {
          return DesktopSignUpScreen();
        } else if (constraints.maxWidth > 800 && constraints.maxWidth < 1200) {
          return MobileSignUpScreen();
        } else {
          return MobileSignUpScreen();
        }
      },
    );
  }
}

class MobileSignUpScreen extends StatefulWidget {
  @override
  _MobileSignUpScreenState createState() => _MobileSignUpScreenState();
}

class _MobileSignUpScreenState extends State<MobileSignUpScreen> {
  bool iAgreeTerms = false;
  var registrationStatus;
  var registrationErrMessage;
  var connectionChk;
  bool isPageLoading = false;

  TextEditingController dobTextCntlr = new TextEditingController();
  TextEditingController firstNameTextCntlr = new TextEditingController();
  TextEditingController lastNameTextCntlr = new TextEditingController();
  TextEditingController emailTextCntlr = new TextEditingController();
  TextEditingController contactNumTextCntlr = new TextEditingController();
  TextEditingController usernameTextCntlr = new TextEditingController();
  TextEditingController passwordTextCntlr = new TextEditingController();

  DateTime selectedDate = DateTime.now();
  var formatter = new DateFormat('dd-MM-yyyy');
  var inputFormatter = new DateFormat('yyyy-MM-dd');

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime.now().subtract(Duration(days: 365 * 80)),
      lastDate: DateTime(4020),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: Theme.of(context).primaryColor, //Head background
            accentColor: Theme.of(context).accentColor, //selection color
            colorScheme:
                ColorScheme.light(primary: Theme.of(context).primaryColor),
            buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
          ),
          child: child,
        );
      },
    );
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
        dobTextCntlr.text = formatter.format(selectedDate).toString();
      });
    }
  }

  void _onIAgreeTermsChanged(bool newValue) => setState(() {
        iAgreeTerms = newValue;
        print("iAgreeTerms: $iAgreeTerms");
      });

  applicationSignUp() async {
    setState(() {
      isPageLoading = true;
    });
    connectionChk = await AppFuntions.connectionCheck();
    print("connectionChk: $connectionChk");
    if (connectionChk == null) {
      var username = usernameTextCntlr.text;
      var password = passwordTextCntlr.text;
      var contactNum = contactNumTextCntlr.text;
      var email = emailTextCntlr.text;
      var lastName = lastNameTextCntlr.text;
      var firstName = firstNameTextCntlr.text;
      var fullName = firstName + " " + lastName;
      var dob = formatter.format(selectedDate).toString();
      if (fullName != "" && username != "" && password != "") {
        print("username:" + username + "=>" + "password:" + password);
        if (iAgreeTerms == true) {
          //multipart/form-data POST Method
          Map<String, String> requestBody = <String, String>{
            "FirstName": firstName,
            "LastName": lastName,
            "Email": email,
            "PhoneNumber": contactNum,
            "DateOfBirth": dob,
            "UserName": username,
            "Password": password,
            "AddressLine1": "",
            "AddressLine2": "",
          };
          // Map<String, String> headers = <String, String>{
          //   'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
          // };
          var uri = Uri.parse(Constants.applicationSignUp);
          var request = http.MultipartRequest('POST', uri)
            // ..headers.addAll(
            //     headers) //if u have headers, basic auth, token bearer... Else remove line
            ..fields.addAll(requestBody);
          var response = await request.send();
          final respStr = await response.stream.bytesToString();
          //

          print("response.request:${response.request}");
          if (response.statusCode == 200) {
            var data = json.decode(respStr);
            print("data: $data");
            if (data != null) {
              registrationStatus = data['Status'];
              registrationErrMessage = data['Errors'];
              print("registrationStatus: $registrationStatus");
              if (registrationStatus == "Success") {
                Flushbar(
                  message: "Registration Successful",
                  duration: Duration(seconds: 2),
                  backgroundColor: Colors.green,
                ).show(context);
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => LoginScreen(),
                  ),
                );
              } else {
                Flushbar(
                  message: registrationErrMessage[0],
                  duration: Duration(seconds: 2),
                  backgroundColor: Colors.red,
                ).show(context);
              }
            } else {
              registrationStatus = false;
              registrationErrMessage = "No data found.";
              Flushbar(
                message: registrationErrMessage,
                duration: Duration(seconds: 2),
                backgroundColor: Colors.red,
              ).show(context);
            }
          } else {
            registrationStatus = false;
            registrationErrMessage = "Error: ${response.statusCode}.";
            Flushbar(
              message: registrationErrMessage,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
            ).show(context);
          }
        } else {
          registrationStatus = false;
          registrationErrMessage = "Please agree to our terms & conditions.";
          Flushbar(
            message: registrationErrMessage,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
          ).show(context);
        }
      } else {
        registrationStatus = false;
        registrationErrMessage = "Please fill up the missing details.";
        Flushbar(
          message: registrationErrMessage,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
        ).show(context);
      }
    } else {
      registrationStatus = false;
      registrationErrMessage = connectionChk;
      Flushbar(
        message: registrationErrMessage,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ).show(context);
    }
    setState(() {
      isPageLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isPageLoading
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : SingleChildScrollView(
              child: SafeArea(
                child: Container(
                  color: Theme.of(context).primaryColor,
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(18.0),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(30.0),
                            bottomRight: Radius.circular(30.0),
                          ),
                        ),
                        child: Column(
                          children: <Widget>[
                            MyLogoWidget(),
                            SizedBox(
                              height: 15.0,
                            ),
                            Row(
                              children: [
                                Text(
                                  "SIGNUP",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20.0,
                                    color: Theme.of(context).primaryColor,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  "Register your account!",
                                  style: TextStyle(
                                    // fontWeight: FontWeight.bold,
                                    fontSize: 15.0,
                                    color: Theme.of(context).primaryColorLight,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 30.0,
                            ),
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'First Name',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: HexColor("#494949"),
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                  TextField(
                                    controller: firstNameTextCntlr,
                                    // autofocus: true,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.normal,
                                    ),
                                    decoration: InputDecoration(
                                      hintText: 'Enter your first name',
                                      hintStyle: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorLight,
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.normal,
                                      ),
                                      // labelText: 'First Name',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Last Name',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: HexColor("#494949"),
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                  TextField(
                                    controller: lastNameTextCntlr,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.normal,
                                    ),
                                    decoration: InputDecoration(
                                      hintText: 'Enter your last name',
                                      hintStyle: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorLight,
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.normal,
                                      ),
                                      // labelText: 'Last Name',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Date of Birth',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: HexColor("#494949"),
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                  TextField(
                                    keyboardType: TextInputType.datetime,
                                    controller: dobTextCntlr,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.normal,
                                    ),
                                    decoration: InputDecoration(
                                      hintText: 'Enter your date of birth',
                                      hintStyle: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorLight,
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.normal,
                                      ),
                                      // labelText: 'Date of Birth',
                                    ),
                                    onTap: () {
                                      _selectDate(context);
                                    },
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Email',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: HexColor("#494949"),
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                  TextField(
                                    keyboardType: TextInputType.emailAddress,
                                    controller: emailTextCntlr,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.normal,
                                    ),
                                    decoration: InputDecoration(
                                      hintText: 'Enter your email',
                                      hintStyle: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorLight,
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.normal,
                                      ),
                                      // labelText: 'Email',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Contact Number',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: HexColor("#494949"),
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                  TextField(
                                    keyboardType: TextInputType.phone,
                                    controller: contactNumTextCntlr,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.normal,
                                    ),
                                    decoration: InputDecoration(
                                      hintText: 'Enter your contact number',
                                      hintStyle: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorLight,
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.normal,
                                      ),
                                      // labelText: 'Contact Number',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Username',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: HexColor("#494949"),
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                  TextField(
                                    controller: usernameTextCntlr,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.normal,
                                    ),
                                    decoration: InputDecoration(
                                      hintText: 'Enter your username',
                                      hintStyle: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorLight,
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.normal,
                                      ),
                                      // labelText: 'Username',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Password',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: HexColor("#494949"),
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                  TextField(
                                    controller: passwordTextCntlr,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.normal,
                                    ),
                                    decoration: InputDecoration(
                                      hintText: 'Enter your password',
                                      hintStyle: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorLight,
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.normal,
                                      ),
                                      // labelText: 'Password',
                                    ),
                                    obscureText: true,
                                    obscuringCharacter: "*",
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              children: [
                                Theme(
                                  data: ThemeData(
                                    unselectedWidgetColor:
                                        Theme.of(context).primaryColor,
                                  ),
                                  child: Checkbox(
                                    value: iAgreeTerms,
                                    onChanged: _onIAgreeTermsChanged,
                                    activeColor: Theme.of(context).primaryColor,
                                  ),
                                ),
                                Text(
                                  "I agree to the terms of ",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12.0,
                                    color: Theme.of(context).primaryColorLight,
                                  ),
                                ),
                                Text(
                                  "Use & Privacy Policy",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12.0,
                                    color: Theme.of(context).primaryColor,
                                  ),
                                ),
                              ],
                            ),
                            ElevatedButton(
                              onPressed: () {
                                applicationSignUp();
                                // Navigator.push(
                                //   // Navigator.pushReplacement(
                                //   context,
                                //   MaterialPageRoute(
                                //     builder: (context) => LoginScreen(),
                                //   ),
                                // );
                              },
                              child: Text(
                                "Sign Up",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Text(
                              "Let's Move",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 12.0,
                                color: Theme.of(context).primaryColorLight,
                              ),
                            ),
                            SocialLoginButtons(),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            TextButton(
                              child: Row(
                                children: [
                                  Text(
                                    "Already have an account? ",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Text(
                                    "Login",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color:
                                          Theme.of(context).primaryColorLight,
                                    ),
                                  ),
                                ],
                              ),
                              onPressed: () {
                                Navigator.push(
                                  // Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => LoginScreen(),
                                  ),
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}

class DesktopSignUpScreen extends StatefulWidget {
  @override
  _DesktopSignUpScreenState createState() => _DesktopSignUpScreenState();
}

class _DesktopSignUpScreenState extends State<DesktopSignUpScreen> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
