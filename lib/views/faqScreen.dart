import 'package:flutter/material.dart';

import '../widgets/myAppBar.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myNavBar.dart';

class FaqScreen extends StatefulWidget {
  @override
  _FaqScreenState createState() => _FaqScreenState();
}

class _FaqScreenState extends State<FaqScreen> {
  List faqScreenList = [
    {
      "Question": "Do I need to subscribe?",
      "Answer":
          "Yes. Subscription costs just £1.00 per month and for that you'll get access to all the quizzes on our India, UK and USA sites - that's over 5,000 quizzes in all!",
    },
    {
      "Question": "Are the quizzes copyrighted?",
      "Answer":
          "All of our quizzes are subject to copyright restriction however we allow our subscribers to print the quizzes. All that we ask is that they are reproduced in the exact form that our software generates (every sheet must show the Education Quizzes details at the bottom of the quiz). Use of any of the materials without showing the Education Quizzes details is a breach of our copyright. Legal action will be taken against any school that uses the quizzes without our express permission. After all, we have to make a living!",
    },
    {
      "Question": "How do I select a quiz to play?",
      "Answer":
          "When you click on a quiz subject, it will take you to the quiz listing page which shows the quiz titles - click any that you want to play.",
    },
    {
      "Question": "Can I play a quiz more than once?",
      "Answer":
          "Yes, you can repeat quizzes as many times as you like. Only your most recent score will be recorded so that you can see your current progress.",
    },
    {
      "Question": "How do I find quizzes on a particular subject?",
      "Answer":
          "Navigating the site is easily done by clicking the buttons on the top of most pages. If for example you wanted to find a quiz on Upper Primary Punctuation you would click Upper Primary > Upper Primary English > Punctuation",
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(),
      bottomNavigationBar: MyBottomNavBar(),
      drawer: MyNavDrawer(),
      body: Container(
        margin: EdgeInsets.all(15.0),
        child: ListView(
          children: [
            Text(
              "FAQ",
              style: Theme.of(context).textTheme.headline5,
              textAlign: TextAlign.center,
            ),
            for (var i = 0; i < faqScreenList.length; i++)
              Container(
                margin: EdgeInsets.only(top: 12.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      faqScreenList[i]['Question'],
                      style: Theme.of(context).textTheme.headline3,
                      textAlign: TextAlign.justify,
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Text(
                      faqScreenList[i]['Answer'],
                      style: Theme.of(context).textTheme.bodyText1,
                      textAlign: TextAlign.justify,
                    ),
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }
}
